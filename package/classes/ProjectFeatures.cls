public class ProjectFeatures
{
	public static final String STATUS_PENDING = 'Pending';
    public static final String STATUS_PLANNING = 'Planning';
    public static final String STATUS_IN_PROGRESS = 'In Progress';
    public static final String STATUS_VOID = 'Void';
    public static final String STATUS_COMPLETE = 'Complete';
    
    private static ProjectContext.Validator validatorImplementation;
    
    public static ProjectContext.Validator newValidator()
    {
        if(validatorImplementation == null)
        {
            return new Validator();
        }
        else
        {
            return validatorImplementation;
        }
    }
    
    public static void setValidatorImplementation(ProjectContext.Validator impl)
    {
		validatorImplementation = impl;
    }
    
    public class Validator implements ProjectContext.Validator
    {
        public Boolean ValidateStatus {get; set;}
        public Boolean ValidateCantReparent {get; set;}
        public Boolean ValidateCantModifyDeprecated {get; set;}
        public Boolean ValidateCantDeleteNonVoid {get; set;}
        public Boolean ValidateCantAddToOldVersion {get; set;}
        public Boolean ValidateCantAddToCompleteOrDepricatedProject {get; set;}
        
        private List<ProjectFeature__c> newFeatures;
        private List<ProjectFeature__c> oldFeatures;
        
        private Map<Id, Project__c> ownerProjects;
        private Map<Id, ProjectVersion__c> assignedVersion;
        
        public Validator()
        {
            ValidateStatus = true;
            ValidateCantReparent = true;
            ValidateCantModifyDeprecated = true;
            ValidateCantDeleteNonVoid = true;
            ValidateCantAddToOldVersion = true;
            ValidateCantAddToCompleteOrDepricatedProject = true;
        }
        
        public Validator withOldRecords(List<SObject> oldRecords)
        {
            oldFeatures = (List<ProjectFeature__c>)oldRecords;
            return this;
        }
        
        public Validator withNewRecords(List<SObject> newRecords)
        {
            newFeatures = (List<ProjectFeature__c>)newRecords;
            return this;
        }
        
        public Validator exceptStatus()
        {
            ValidateStatus = false;
            return this;
        }
        
        public Validator exceptCantModifyDeprecated()
        {
            ValidateCantModifyDeprecated = false;
            return this;
        }
        
        public Validator exceptCantDeleteNonVoid()
        {
            ValidateCantDeleteNonVoid = false;
            return this;
        }
        
        public Validator exceptCantAddToOldVersion()
        {
            ValidateCantAddToOldVersion = false;
            return this;
        }
        
        public Validator exceptCantAddToCompleteOrDepricatedProject()
        {
            ValidateCantAddToCompleteOrDepricatedProject = false;
            return this;
        }
        
        public void validate()
        {
            for(ProjectFeature__c feature : getFeatures())
            {
                if(ProjectContext.isRecordPreValidated( (SObject)feature) )
                    continue;
               	
                ValidationHandler handler = new ValidationHandler(feature);
                
                if(isInsert())
                {
                    validateInsert(handler);
                    continue;
                }
                
                if(isDelete())
                {
                    validateDelete(handler);
                    continue;
                }
                
                //Update validation, if the handler doesn't have an ID then something went wrong!
            	if(handler.getId() == null)
                	handler.registerError('Update trigger but record has no ID');
                
                if(ValidateStatus)
                    doValidateStatus(handler);
                
                if(ValidateCantReparent)
                    doValidateCantReparent(handler);
                
                if(ValidateCantModifyDeprecated)
                    doValidateCantModifyDeprecated(handler);
                
                if(!ProjectContext.isInTrigger())
                    ProjectContext.registerPreValidated(handler);
            }
        }
        
        private void validateInsert(ValidationHandler handler)
        {
            if(ValidateCantAddToOldVersion)
                    doValidateCantAddToOldVersion(handler);
            
            if(ValidateCantAddToCompleteOrDepricatedProject)
                doValidateCantAddToCompleteOrDepricatedProject(handler);
        }
        
        private void validateDelete(ValidationHandler handler)
        {
			if(ValidateCantDeleteNonVoid)
                doValidateCantDeleteNonVoid(handler);
        }
        
        private void doValidateStatus(ValidationHandler handler)
        {
            String oldStatus = getOldRecord(handler.getId()).Status__c;
            String newStatus = ((ProjectFeature__c)handler.getRecord()).Status__c;
            
            if(oldStatus != newStatus)
            {
                if(newStatus == ProjectFeatures.STATUS_COMPLETE && ((ProjectFeature__c)handler.getRecord()).Complete_Date__c == null)
                    handler.registerError('Status__c', 'A completed feature must have a completion date.');
            }
        }
        
        private void doValidateCantReparent(ValidationHandler handler)
        {
            if(getOldRecord(handler.getId()).Project__c != ((ProjectFeature__c)handler.getRecord()).Project__c)
                handler.registerError('Project__c', 'Cannot reparent a project feature');
        }
        
        private void doValidateCantModifyDeprecated(ValidationHandler handler)
        {
			Project__c owner = getProject( ((ProjectFeature__c)handler.getRecord()).Project__c );
            if(owner.Status__c == Projects.STATUS_DEPRECATED || owner.Status__c == Projects.STATUS_COMPLETE)
                handler.registerError('Cannot change the features of a Complete or Deprecated project');
        }
        
        private void doValidateCantDeleteNonVoid(ValidationHandler handler)
        {
			Project__c owner = getProject( ((ProjectFeature__c)handler.getRecord()).Project__c );
            if(owner.Status__c == Projects.STATUS_DEPRECATED)
            {
                if(((ProjectFeature__c)handler.getRecord()).Status__c != ProjectFeatures.STATUS_VOID)
	                handler.registerError('Cannot delete a non-void feature.');
            }
            else
            {
                handler.registerError('Cannot delete a feature from an active project.');
            }
        }
        
        private void doValidateCantAddToOldVersion(ValidationHandler handler)
        {
            ProjectVersion__c release = getVersion( ((ProjectFeature__c)handler.getRecord()).Release__c );
            if(release.Status__c == ProjectVersions.STATUS_RELEASED || release.Status__c == ProjectVersions.STATUS_DEPRECATED)
                handler.registerError('Release__c', 'Cannot add a feature to an older version of the project.');
        }
        
        private void doValidateCantAddToCompleteOrDepricatedProject(ValidationHandler handler)
        {
            Project__c owner = getProject( ((ProjectFeature__c)handler.getRecord()).Project__c );
            if(owner.Status__c == Projects.STATUS_COMPLETE || owner.Status__c == Projects.STATUS_DEPRECATED)
                handler.registerError('Cannot add features to a Complete or Deprecated project');
        }
        
        private List<ProjectFeature__c> getFeatures()
        {
            if(newFeatures != null)
                return newFeatures;
            
            if(oldFeatures != null)
                return oldFeatures;
            
            return new List<ProjectFeature__c>();
        }
        
        private ProjectFeature__c getOldRecord(Id recordId)
        {
            if(oldFeatures == null)
            {
                oldFeatures = ProjectSelector.newInstance().selectFeaturesById(new Map<Id, ProjectFeature__c>(newFeatures).keySet());
            }
            
            return new Map<Id, ProjectFeature__c>(oldFeatures).get(recordId);
        }
        
        private Project__c getProject(Id projectId)
        {
            if(ownerProjects == null)
                ownerProjects = new Map<Id, Project__c>(ProjectSelector.newInstance().selectProjectsById( getProjectIds() ));
            
            return ownerProjects.get(projectId);
        }
        
        private ProjectVersion__c getVersion(Id versionId)
        {
           	if(assignedVersion == null)
            	assignedVersion = new Map<Id, ProjectVersion__c>(ProjectSelector.newInstance().selectVersionsById( getVersionIds() ));
            
            return assignedVersion.get(versionId);
        }
        
        private Set<Id> getProjectIds()
        {
            Set<Id> ids = new Set<Id>();
            for(ProjectFeature__c feature : getFeatures())
            {
                ids.add(feature.Project__c);
            }
            return ids;
        }
        
        private Set<Id> getVersionIds()
        {
            Set<Id> ids = new Set<Id>();
            for(ProjectFeature__c feature : getFeatures())
            {
                ids.add(feature.Release__c);
            }
            return ids;
        }
        
        private Boolean isInsert()
        {
            return ProjectContext.isInTrigger() && oldFeatures == null && newFeatures != null;
        }
        
        private Boolean isDelete()
        {
            return ProjectContext.isInTrigger() && oldFeatures != null && newFeatures == null;
        }
    }
    
    public class ValidationHandler implements ProjectContext.ValidationHandler
    {
        private ProjectFeature__c record;
        
        public ValidationHandler(ProjectFeature__c sObj)
        {
            record = sObj;
        }
        
        public Id getId()
        {
            return record.Id;
        }
        
        public SObject getRecord()
        {
            return record;
        }
        
        public Boolean isEqual(SObject otherRecord)
        {
            return false;
        }
        
        public void registerError(String errorMessage)
        {
            if(ProjectContext.isInTrigger())
            {
                record.addError(errorMessage);
            }
            else
            {
                throw new ProjectException(errorMessage);
            }
        }
        
        public void registerError(String fieldname, String errorMessage)
        {
            if(!ProjectContext.isInTrigger())
                throw new ProjectException(errorMessage);
            
            if(fieldName == 'Status__c')
            {
                record.Status__c.addError(errorMessage);
            }
            else if(fieldName == 'Project__c')
            {
                record.Project__c.addError(errorMessage);
            }
            else if(fieldName == 'Release__c')
            {
                record.Release__c.addError(errorMessage);
            }
        }
    }
}