@isTest(SeeAllData=false)
public class ProjectTriggerActionsTest
{
	private static TestMethod void sendNotificationToProjectDevelopersCallsNotificationService()
    {
        SendNotificationScenario scenario = new SendNotificationScenario();
        scenario.withProject('Test Project')
            	.withHumanAsset(true, 'Developer')
            	.withBug();
        
        Map<Id, List<ProjectAsset__c>> assetsByProjectId = new Map<Id, List<ProjectAsset__c>>();
        assetsByProjectId.put( scenario.get('project').Id, new List<ProjectAsset__c>{ (ProjectAsset__c)scenario.get('asset') } );
        
        ProjectMocks.ProjectSelector mockSelector = new ProjectMocks.ProjectSelector();
        mockSelector.whenSelectAssetsMappedByProjectId().thenReturn(assetsByProjectId);
        mockSelector.whenSelectProjectsById().thenReturn( new List<Project__c>{ (Project__c)scenario.get('project') } );
        ProjectSelector.setImplementation(mockSelector);
        
        ProjectMocks.NotificationService mockService = new ProjectMocks.NotificationService();
        mockService.whenSendNotifications().thenAssertCalled(1);
        NotificationService.setImplementation(mockService);
        
        sim_util.EventBus.Event event = new sim_util.EventBus.EventBuilder().withName('insert')
            								  			  .withArgument('newRecords', new List<ProjectBug__c>{(ProjectBug__c)scenario.get('bug')})
            								  			  .fire();
        
        ProjectTriggerActions.SendNotificationToProjectDevelopers action = new ProjectTriggerActions.SendNotificationToProjectDevelopers();
        action.call(event);
        
        mockService.assertCalls();
        
        List<Object> callArgs = mockService.whenSendNotifications().wasCalled().getArguments(0);
        NotificationService.Notification arg = ((List<NotificationService.Notification>)callArgs.get(0)).get(0);
        Boolean chatterArg = (Boolean)callArgs.get(1);
        Boolean emailArg = (Boolean)callArgs.get(2);
        
        System.assertEquals(scenario.get('user').Id, arg.User, 'Should have called the notification service with the assigned developer ID');
        System.assertEquals('New Test Project bug reported', arg.Subject, 'Should have called the notification service withthe correct subject');
        System.assertEquals('A new bug has been reported on Test Project.', arg.Message, 'Should have called the notification service with the correct message');
        System.assertEquals(true, chatterArg, 'Should have called the notification service to send a chatter post');
        System.assertEquals(true, emailArg, 'Should have called the notification service to send an email');
    }
    
    private static TestMethod void sendNotificationToDevelopersDoesNotCallsNotificationServiceWhenNoneAssigned()
    {
        SendNotificationScenario scenario = new SendNotificationScenario();
        scenario.withProject('Test Project')
            	.withHumanAsset(false, 'Developer')
            	.withBug();
        
        Map<Id, List<ProjectAsset__c>> assetsByProjectId = new Map<Id, List<ProjectAsset__c>>();
        assetsByProjectId.put( scenario.get('project').Id, new List<ProjectAsset__c>{ (ProjectAsset__c)scenario.get('asset') } );
        
        ProjectMocks.ProjectSelector mockSelector = new ProjectMocks.ProjectSelector();
        mockSelector.whenSelectAssetsMappedByProjectId().thenReturn(assetsByProjectId);
        mockSelector.whenSelectProjectsById().thenReturn( new List<Project__c>{ (Project__c)scenario.get('project') } );
        ProjectSelector.setImplementation(mockSelector);
        
        ProjectMocks.NotificationService mockService = new ProjectMocks.NotificationService();
        mockService.whenSendNotifications().thenAssertCalled(0);
        NotificationService.setImplementation(mockService);
        
        sim_util.EventBus.Event event = new sim_util.EventBus.EventBuilder().withName('insert')
            											  .withArgument('newRecords', new List<ProjectBug__c>{(ProjectBug__c)scenario.get('bug')})
            								  			  .fire();
        
        ProjectTriggerActions.SendNotificationToProjectDevelopers action = new ProjectTriggerActions.SendNotificationToProjectDevelopers();
        action.call(event);
        
        mockService.assertCalls();
    }
    
    private class SendNotificationScenario
    {
        private Map<String, SObject> scenario;
        
        public SendNotificationScenario()
        {
            scenario = new Map<String, SObject>();
            scenario.put('user', [SELECT Id FROM User LIMIT 1]);
        }
        
        public SendNotificationScenario withProject(String name)
        {
            Project__c project = new Project__c();
        	project.Name = name;
        
        	insert project;
            scenario.put('project', project);
            
            return this;
        }
        
        public SendNotificationScenario withHumanAsset(Boolean assigned, String purpose)
        {
            ProjectAsset__c asset = new ProjectAsset__c();
            asset.Project__c = scenario.get('project').Id;
            asset.Person__c = scenario.get('user').Id;
            asset.Purpose__c = purpose;
            asset.Assigned__c = assigned;
            
            insert asset;
            scenario.put('asset', asset);
            
            return this;
        }
        
        public SendNotificationScenario withBug()
        {
            ProjectVersion__c version = new ProjectVersion__c();
            version.Project__c = scenario.get('project').Id;
            insert version;
            
            ProjectBug__c bug = new ProjectBug__c();
            bug.Project__c = scenario.get('project').Id;
            bug.Found_Date__c = Date.today();
            bug.Found_Version__c = version.Id;
            bug.Status__c = 'Pending';
            
            insert bug;
            scenario.put('bug', bug);
            
            return this;
        }
        
        public SObject get(String scenarioKey)
        {
            return scenario.get(scenarioKey);
        }
    }
}