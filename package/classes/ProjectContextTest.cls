@isTest(SeeAllData=true)
private class ProjectContextTest
{

    private static TestMethod void shouldReportPlatformRequest_beforeRegisterServiceTransactionCalled()
    {
		System.assert(ProjectContext.isPlatformRequest(), 'Should be a platform request');
    }
    
    private static TestMethod void shouldReportNonPlatformRequest_afterRegisterServiceTrasactionCalled()
    {
        ProjectContext.registerServiceTransaction();
        System.assert(!ProjectContext.isPlatformRequest(), 'Should not be a platform request');
    }
    
    private static TestMethod void shouldReportIsNotInTrigger_beforeRegisterInTrigger()
    {
		System.assert(!ProjectContext.isInTrigger(), 'Should not be in a trigger');
    }
    
    private static TestMethod void shouldReportIsInTrigger_afterRegisterInTrigger()
    {
        ProjectContext.registerInTrigger(true);
        System.assert(ProjectContext.isInTrigger(), 'Should be in a trigger');
    }
    
    private static TestMethod void shouldCountRegisterInTriggerCalls()
    {
        ProjectContext.registerInTrigger(true);
        ProjectContext.registerInTrigger(true);
        ProjectContext.registerInTrigger(true);
        
        System.assert(ProjectContext.isInTrigger(), 'Should be in third trigger');
        
        ProjectContext.registerInTrigger(false);
        System.assert(ProjectContext.isInTrigger(), 'Should be in second trigger');
        
        ProjectContext.registerInTrigger(false);
        System.assert(ProjectContext.isInTrigger(), 'Should be in first trigger');
        
        ProjectContext.registerInTrigger(false);
        System.assert(!ProjectContext.isInTrigger(), 'Should not be in a trigger anymore');
    }
    
    private static TestMethod void isServiceMode_shouldReturnCustomSetting()
    {
        System.assert(!ProjectContext.isServiceMode(), 'Should not be in service mode by default');
        
        ProjectManagementSettings__c settings = ProjectManagementSettings__c.getInstance();
        settings.ServiceMode__c = true;
        upsert settings;
        
        System.assert(ProjectContext.isServiceMode(), 'Should be in service mode');
    }
    
    private static TestMethod void isRecordPrevlidated_returnFalse_whenRecordNeverValidated()
    {
        Account acc = new Account(Name = 'Steve');
        insert acc;
        
        System.assert(!ProjectContext.isRecordPreValidated(acc), 'Record should not have been validated');
    }
    
    private static TestMethod void isRecordPrevalidated_returnTrue_whenRecordValidated_notChanged()
    {
        Account acc = new Account(Name = 'Steve');
        insert acc;
        
        ProjectMocks.ValidationHandler handler = new ProjectMocks.ValidationHandler();
        handler.whenGetId().thenReturn(acc.Id);
        handler.whenIsEqual().thenAssertCalled(1)
            				 .thenReturn(true);
        
        ProjectContext.registerPreValidated(handler);
        
        System.assert(ProjectContext.isRecordPreValidated(acc), 'Record should be pre-validated');
        
        handler.assertCalls();
    }
    
    private static TestMethod void isRecordPrevalidated_returnFalse_whenRecordValidated_changed()
    {
        Account acc = new Account(Name = 'Steve');
        insert acc;
        
        ProjectMocks.ValidationHandler handler = new ProjectMocks.ValidationHandler();
        handler.whenGetId().thenReturn(acc.Id);
        handler.whenIsEqual().thenAssertCalled(1)
            				 .thenReturn(false);
        
        ProjectContext.registerPreValidated(handler);
        
        System.assert(!ProjectContext.isRecordPreValidated(acc), 'Record should be pre-validated');
        
        handler.assertCalls();
    }
    
}