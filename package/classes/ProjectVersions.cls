public class ProjectVersions
{
    public static final String STATUS_PENDING = 'Pending';
	public static final String STATUS_IN_DEVELOPMENT = 'In Development';
    public static final String STATUS_RELEASED = 'Released';
    public static final String STATUS_DEPRECATED = 'Deprecated';
    
    public class Validator implements ProjectContext.Validator
    {
        private List<ProjectVersion__c> newVersions;
        private List<ProjectVersion__c> oldVersions;
        
        public Validator withOldRecords(List<SObject> oldRecords)
        {
            oldVersions = (List<ProjectVersion__c>)oldRecords;
            return this;
        }
        
        public Validator withNewRecords(List<SObject> newRecords)
        {
            newVersions = (List<ProjectVersion__c>)newRecords;
            return this;
        }
        
        public void validate()
        {
            for(ProjectVersion__c version : getVersions())
            {
                if(ProjectContext.isRecordPreValidated( (SObject)version) )
                    continue;
               	
                ValidationHandler handler = new ValidationHandler(version);
                
                if(isInsert())
                {
                    validateInsert(handler);
                    continue;
                }
                
                if(isDelete())
                {
                    validateDelete(handler);
                    continue;
                }
                
                //Update validation, if the handler doesn't have an ID then something went wrong!
            	if(handler.getId() == null)
                	handler.registerError('Update trigger but record has no ID');
                
                // Update Validation here
                
                if(!ProjectContext.isInTrigger())
                    ProjectContext.registerPreValidated(handler);
            }
        }
        
        private void validateInsert(ValidationHandler handler)
        {
            
        }
        
        private void validateDelete(ValidationHandler handler)
        {
            
        }
        
        private List<ProjectVersion__c> getVersions()
        {
            if(newVersions != null)
                return newVersions;
            
            if(oldVersions != null)
                return oldVersions;
            
            return new List<ProjectVersion__c>();
        }
        
        private Boolean isInsert()
        {
            return ProjectContext.isInTrigger() && oldVersions == null && newVersions != null;
        }
        
        private Boolean isDelete()
        {
            return ProjectContext.isInTrigger() && oldVersions != null && newVersions == null;
        }
    }
    
    public class ValidationHandler implements ProjectContext.ValidationHandler
    {
        private ProjectVersion__c record;
        
        public ValidationHandler(ProjectVersion__c sObj)
        {
            record = sObj;
        }
        
        public Id getId()
        {
            return record.Id;
        }
        
        public SObject getRecord()
        {
            return record;
        }
        
        public Boolean isEqual(SObject otherRecord)
        {
            return false;
        }
        
        public void registerError(String errorMessage)
        {
            if(ProjectContext.isInTrigger())
            {
                record.addError(errorMessage);
            }
            else
            {
                throw new ProjectException(errorMessage);
            }
        }
        
        public void registerError(String fieldname, String errorMessage)
        {
            if(!ProjectContext.isInTrigger())
                throw new ProjectException(errorMessage);
            
            if(fieldName == 'Status__c')
            {
                record.Status__c.addError(errorMessage);
            }
        }
    }
}