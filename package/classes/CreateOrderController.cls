public class CreateOrderController
{    
    private Contract ownerContract;
    public Order ContractOrder {get; set;}
    public Boolean IsValid {get; set;}
        
    public CreateOrderController(ApexPages.StandardController stdController)
    {
        ownerContract = (Contract)stdController.getRecord();
        ContractOrder = new Order();
        
        validate();
	}
    
    public PageReference createOrder()
    {
        PageReference ref;
        try
        {
        	Id orderId = ContractService.newInstance().createOrder(ownerContract, ContractOrder.EffectiveDate);
            ref = new PageReference('/' + orderId + '/e');
        }
        catch(Exception e)
        {
            ref = null;
            IsValid = false;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(msg);
        }
     	
        return ref;
    }
    
    private void validate()
    {
        String message;
        ApexPages.Severity severity;
        if(ownerContract.Status == 'Activated')
        {
            message = 'Click the button below to create an Order from this Contract.';
            severity = ApexPages.Severity.INFO;
            IsValid = true;
        }
        else
        {
            message = 'You can only create Orders from active Contracts.';
            severity = ApexPages.Severity.ERROR;
            IsValid = false;
        }
        ApexPages.Message pageMessage = new ApexPages.Message(severity, message);
        ApexPages.addMessage(pageMessage);
    }
}