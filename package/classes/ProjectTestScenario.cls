@isTest(SeeAllData=false)
public class ProjectTestScenario
{
    public static ProjectBuilder createProject()
    {
        return new ProjectBuilder();
    }
    
    public static VersionBuilder createVersion()
    {
        return new VersionBuilder();
    }
    
    public static FeatureBuilder createFeature()
    {
        return new FeatureBuilder();
    }
    
    public static BugBuilder createBug()
    {
        return new BugBuilder();
    }
    
    public static AssetBuilder createAsset()
    {
        return new AssetBuilder();
    }
    
    public static DependencyBuilder createDependency()
    {
        return new DependencyBuilder();
    }
    
    public static Scenario createScenario()
    {
        return new Scenario();
    }
    
    public static Scenario createBasicScenario()
    {
        Scenario test = new Scenario();
        
        test.newProject('Test Project')
            .withStatus(Projects.STATUS_IN_PROGRESS)
            .withBudget(100000.00)
            .withStartDate(Date.today().addYears(-1))
            .withDescription('A project used for unit testing.')
            .withPlatform(Projects.PLATFORM_SALESFORCE)
            .withSourceControl(Projects.SOURCE_CONTROL_GIT)
            .withWebsite('http://test.com')
            .withType(Projects.TYPE_INTERNAL);

        Id versionId = test.addVersion()
            			   .withVersionNumber('1.00')
            			   .withCurrent(true)
            			   .withStatus(ProjectVersions.STATUS_IN_DEVELOPMENT)
            			   .withUrl('http://test.com')
            			   .withChangeLog('A test project version.')
            			   .save().Id;

        test.Projects.get(0).withCurrentVersion(versionId).save();
        
        test.addAsset()
            .withPurpose('Developer')
            .withAssigned(true)
            .save();
        
        test.addFeature()
            .withName('Implement')
            .withStatus(ProjectFeatures.STATUS_PENDING)
            .withDescription('Implement this feature.')
            .withEstimatedDevHours(3)
            .withRemainingDevHours(3)
            .withRelease(versionId)
            .save();
        
        test.addBug()
            .withFoundDate(Date.today())
            .withFoundVersion(versionId)
            .withDescription('A test bug.')
            .withSteps('1) Test step 1')
            .withEstimatedHours(3)
            .withRemainingHours(3)
            .withStatus(ProjectBugs.STATUS_PENDING)
            .save();
        
        test.addDependency()
            .withName('NodeJS')
            .withVersionNumber('6.9.2')
            .withVersion(versionId)
            .withInstallDate( Date.today() )
            .withWebsite('https://nodejs.org/')
            .withDescription('A Javascript engine that allows native development with Javascript.');
        
        return test;
    }
    
    public class Scenario
    {
        public List<ProjectBuilder> Projects {get; set;}
        public List<VersionBuilder> Versions {get; set;}
        public List<FeatureBuilder> Features {get; set;}
        public List<BugBuilder> Bugs {get; set;}
        public List<AssetBuilder> Assets {get; set;}
        public List<DependencyBuilder> Dependencies {get; set;}
        
        public Scenario()
        {
            Projects = new List<ProjectBuilder>();
            Versions = new List<VersionBuilder>();
            Features = new List<FeatureBuilder>();
            Bugs = new List<BugBuilder>();
            Assets = new List<AssetBuilder>();
            Dependencies = new List<DependencyBuilder>();
        }
        
        public ProjectBuilder newProject(String projectName)
        {
            ProjectBuilder project = new ProjectBuilder();
            project.withName(projectName).save();
            
            Projects.add(project);
            return project;
        }
        
        public FeatureBuilder addFeature()
        {
            return addFeature(0);
        }
        
        public FeatureBuilder addFeature(Integer projectIndex)
        {
            FeatureBuilder feature = new FeatureBuilder();
            feature.withProject( Projects.get(projectIndex).getRecord().Id );
            
            Features.add(feature);
            return feature;
        }
        
        public VersionBuilder addVersion()
        {
            return addVersion(0);
        }
        
        public VersionBuilder addVersion(Integer projectIndex)
        {
            VersionBuilder version = new VersionBuilder();
            version.withProject( Projects.get(projectIndex).getRecord().Id );
            
            Versions.add(version);
            return version;
        }
        
        public BugBuilder addBug()
        {
            return addBug(0);
        }
        
        public BugBuilder addBug(Integer projectIndex)
        {
            BugBuilder bug = new BugBuilder();
            bug.withProject( Projects.get(projectIndex).getRecord().Id );
            
            Bugs.add(bug);
            return bug;
        }
        
        public AssetBuilder addAsset()
        {
            return addAsset(0);
        }
        
        public AssetBuilder addAsset(Integer projectIndex)
        {
            AssetBuilder asset = new AssetBuilder();
            asset.withProject( Projects.get(projectIndex).getRecord().Id );
            
            Assets.add(asset);
            return asset;
        }
        
        public DependencyBuilder addDependency()
        {
            return addDependency(0);
        }
        
        public DependencyBuilder addDependency(Integer projectIndex)
        {
            DependencyBuilder dependency = new DependencyBuilder();
            dependency.withAssociatedProject( Projects.get(projectIndex).getRecord().Id );
            
            Dependencies.add(dependency);
            return dependency;
        }
        
        public void doUpdate()
        {
			updateProjects();
            updateFeatures();
            updateVersions();
            updateBugs();
            updateAssets();
            updateDependencies();
        }
        
        public void updateProjects()
        {
            for(ProjectBuilder project : Projects)
            {
                project.save();
            }
        }
        
        public void updateFeatures()
        {
            for(FeatureBuilder feature : Features)
            {
                feature.save();
            }
        }
        
        public void updateVersions()
        {
            for(VersionBuilder version : Versions)
            {
                version.save();
            }
        }

        public void updateBugs()
        {
            for(BugBuilder bug : Bugs)
            {
                bug.save();
            }
        }
        
        public void updateAssets()
        {
            for(AssetBuilder asset : Assets)
            {
                asset.save();
            }
        }
        
        public void updateDependencies()
        {
            for(DependencyBuilder dependency : Dependencies)
            {
                dependency.save();
            }
        }
    }
    
    public abstract class Builder
    {
        public abstract SObject getSObject();
        public abstract void resolveDependencies();
        
        public void assertFailInsert(String expectedMessage)
        {
            try
            {
                insert getSObject();
                System.assert(false, 'Expected to fail insert');
            }
            catch(DmlException e)
            {
                System.assert(e.getMessage().contains(expectedMessage), 'Expected message "' + expectedMessage + '" but got ' + e.getMessage());
            }
        }
        
        public void assertPassInsert()
        {
            try
            {
                insert getSObject();
            }
            catch(DmlException e)
            {
                System.assert(false, 'Expected to pass insert');
            }
        }
        
        protected Boolean isEmpty(String text)
        {
            return text == null || text == '';
        }
    }
    
    public class ProjectBuilder extends Builder
    {
        private Project__c record;
        
        public ProjectBuilder()
        {
            record = new Project__c();
        }
        
        public ProjectBuilder withName(String projectName)
        {
            record.Name = projectName;
            return this;
        }
        
        public ProjectBuilder withBudget(Decimal budget)
        {
            record.Budget__c = budget;
            return this;
        }
        
        public ProjectBuilder withCompleteDate(Date completeDate)
        {
            record.Complete_Date__c = completeDate;
            return this;
        }
        
        public ProjectBuilder withContract(Id contractId)
        {
            record.Contract__c = contractId;
            return this;
        }
        
        public ProjectBuilder withCurrentVersion(Id currentVersion)
        {
            record.Current_Version__c = currentVersion;
            return this;
        }
        
        public ProjectBuilder withCustomer(Id customerAccount)
        {
            record.Customer__c = customerAccount;
            return this;
        }
        
        public ProjectBuilder withDescription(String description)
        {
            record.Description__c = description;
            return this;
        }
        
        public ProjectBuilder withUserManager(Id manager)
        {
            record.Manager_User__c = manager;
            return this;
        }
        
        public ProjectBuilder withContactManager(Id manager)
        {
            record.Manager_Contact__c = manager;
            return this;
        }
        
        public ProjectBuilder withPlatform(String platform)
        {
            record.Platform__c = platform;
            return this;
        }
        
        public ProjectBuilder withProduct(Id productId)
        {
            record.Product__c = productId;
            return this;
        }
        
        public ProjectBuilder withRepository(String repo)
        {
            record.Repository__c = repo;
            return this;
        }
        
        public ProjectBuilder withSourceControl(String sourceControl)
        {
            record.Source_Control__c = sourceControl;
            return this;
        }
        
        public ProjectBuilder withStartDate(Date startDate)
        {
            record.Started_Date__c = startDate;
            return this;
        }
        
        public ProjectBuilder withStatus(String status)
        {
            record.Status__c = status;
            return this;
        }
        
        public ProjectBuilder withType(String projectType)
        {
            record.Type__c = projectType;
            return this;
        }
        
        public ProjectBuilder withWebsite(String website)
        {
            record.Website__c = website;
            return this;
        }
        
        public Project__c getRecord()
        {
            resolveDependencies();
            return record;
        }
        
        public Project__c save()
        {
            resolveDependencies();
            upsert record;
            return record;
        }
        
        public Override void resolveDependencies()
        {
            if ( isEmpty(record.Name) )
                record.Name = 'Test Project';
            
            if( isEmpty(record.Status__c) )
                record.Status__c = Projects.STATUS_IN_PROGRESS;
        }
        
        public Override SObject getSObject()
        {
            return record;
        }
    }
    
    public class VersionBuilder extends Builder
    {
        Private ProjectVersion__c record;
        
        public VersionBuilder()
        {
            record = new ProjectVersion__c();
        }
        
        public VersionBuilder withVersionNumber(String versionNumber)
        {
            record.Name = versionNumber;
            return this;
        }
        
        public VersionBuilder withChangeLog(String changeLog)
        {
            record.ChangeLog__c = changeLog;
            return this;
        }
        
        public VersionBuilder withCurrent(Boolean current)
        {
            record.Current__c = current;
            return this;
        }
        
        public VersionBuilder withProject(Id projectId)
        {
            record.Project__c = projectId;
            return this;
        }
        
        public VersionBuilder withReleaseDate(Date releaseDate)
        {
            record.Release_Date__c = releaseDate;
            return this;
        }
        
        public VersionBuilder withStatus(String status)
        {
            record.Status__c = status;
            return this;
        }
        
        public VersionBuilder withUrl(String url)
        {
            record.URL__c = url;
            return this;
        }
        
        public VersionBuilder withReleaseTag(String url)
        {
            record.Release_Tag__c = url;
            return this;
        }
        
        public ProjectVersion__c getRecord()
        {
            resolveDependencies();
            return record;
        }
        
        public ProjectVersion__c save()
        {
            resolveDependencies();
            upsert record;
            return record;
        }
            
        public Override void resolveDependencies()
        {
            if ( isEmpty(record.Name) )
                record.Name = '1.00';
            
            if( record.Project__c == null )
                record.Project__c = new ProjectBuilder().save().Id;
        }
        
        public Override SObject getSObject()
        {
            return record;
        }
    }
    
    public class FeatureBuilder extends Builder
    {
        private ProjectFeature__c record;
        
        public FeatureBuilder()
        {
            record = new ProjectFeature__c();
        }
        
        public FeatureBuilder withName(String featureName)
        {
            record.Name__c = featureName;
            return this;
        }
        
        public FeatureBuilder withUser(Id userId)
        {
            record.Assigned_User__c = userId;
            return this;
        }
        
        public FeatureBuilder withCompleteDate(Date completeDate)
        {
            record.Complete_Date__c = completeDate;
            return this;
        }
        
        public FeatureBuilder withDescription(String description)
        {
            record.Description__c = description;
            return this;
        }
        
        public FeatureBuilder withEstimatedDevHours(Integer devHours)
        {
            record.Estimated_Dev_Hours__c = devHours;
            return this;
        }
        
        public FeatureBuilder withProject(Id projectId)
        {
            record.Project__c = projectId;
            return this;
        }
        
        public FeatureBuilder withRelease(Id versionId)
        {
            record.Release__c = versionId;
            return this;
        }
        
        public FeatureBuilder withRemainingDevHours(Integer devHours)
        {
            record.Remaining_Dev_Hours__c = devHours;
            return this;
        }
        
        public FeatureBuilder withStartDate(Date startDate)
        {
            record.Start_Date__c = startDate;
            return this;
        }
        
        public FeatureBuilder withStatus(String status)
        {
            record.Status__c = status;
            return this;
        }
        
        public FeatureBuilder withTotalDevHours(Integer devHours)
        {
            record.Total_Dev_Hours__c = devHours;
            return this;
        }
        
        public ProjectFeature__c getRecord()
        {
            resolveDependencies();
            return record;
        }
        
        public ProjectFeature__c save()
        {
            resolveDependencies();
            upsert record;
            return record;
        }
        
        public Override void resolveDependencies()
        {
            if( record.Name__c == null )
                record.Name__c = 'Test Feature';
            
            if( record.Project__c == null )
                record.Project__c = new ProjectBuilder().save().Id;
            
            if( record.Release__c == null )
            {
                Project__c project = [SELECT Id, Current_Version__c FROM Project__c WHERE Id = :record.Project__c];
                if( project.Current_Version__c == null )
                {
                    ProjectVersion__c version = new VersionBuilder().withProject(project.Id).withCurrent(true).save();
                    project.Current_Version__c = version.Id;
                    record.Release__c = version.Id;
                }
                else
                {
                    record.Release__c = project.Current_Version__c;
                }
            }
            
            if ( record.Status__c == ProjectFeatures.STATUS_COMPLETE )
            {
                record.Complete_Date__c = Date.today();
            }
        }
        
        public Override SObject getSObject()
        {
            return record;
        }
    }
    
    public class BugBuilder extends Builder
    {
        private ProjectBug__c record;
        
        public BugBuilder()
        {
            record = new ProjectBug__c();
        }
        
        public BugBuilder withAssignedUser(Id userId)
        {
            record.Assigned_User__c = userId;
            return this;
        }
        
        public BugBuilder withDescription(String description)
        {
            record.Description__c = description;
            return this;
        }
        
        public BugBuilder withEstimatedHours(Integer devHours)
        {
            record.Estimated_Hours__c = devHours;
            return this;
        }
        
        public BugBuilder withFixedDate(Date fixedDate)
        {
            record.Fixed_Date__c = fixedDate;
            return this;
        }
        
        public BugBuilder withFixedVersion(Id versionId)
        {
            record.Fixed_In_Version__c = versionId;
            return this;
        }
        
        public BugBuilder withFoundDate(Date foundDate)
        {
            record.Found_Date__c = foundDate;
            return this;
        }
        
        public BugBuilder withFoundVersion(Id versionId)
        {
            record.Found_Version__c = versionId;
            return this;
        }
        
        public BugBuilder withProject(Id projectId)
        {
            record.Project__c = projectId;
            return this;
        }
        
        public BugBuilder withRemainingHours(Integer devHours)
        {
            record.Remaining_Dev_Hours__c = devHours;
            return this;
        }
        
        public BugBuilder withStatus(String status)
        {
            record.Status__c = status;
            return this;
        }
        
        public BugBuilder withSteps(String steps)
        {
            record.Steps_To_Recreate__c = steps;
            return this;
        }
        
        public BugBuilder withTotalDevHours(Integer devHours)
        {
            record.Total_Dev_Hours__c = devHours;
            return this;
        }
        
        public ProjectBug__c getRecord()
        {
            resolveDependencies();
            return record;
        }
        
        public ProjectBug__c save()
        {
            resolveDependencies();
            upsert record;
            return record;
        }
        
        public Override void resolveDependencies()
        {
            if( record.Found_Date__c == null )
                record.Found_Date__c = Date.today();
            
            if( record.Project__c == null )
                record.Project__c = new ProjectBuilder().save().Id;
            
            if ( record.Found_Version__c == null )
            {
                Project__c project = [SELECT Id, Current_Version__c FROM Project__c WHERE Id = :record.Project__c];
                if( project.Current_Version__c == null )
                {
                    ProjectVersion__c version = new VersionBuilder().withProject(project.Id).withCurrent(true).save();
                    project.Current_Version__c = version.Id;
                    update project;

                    record.Found_Version__c = version.Id;
                }
                else
                {
                    record.Found_Version__c = project.Current_Version__c;
                }
            }
            
            if ( record.Status__c == ProjectBugs.STATUS_FIXED )
            {
                record.Fixed_In_Version__c = record.Found_Version__c;
            }
        }
        
        public Override SObject getSObject()
        {
            return record;
        }
    }
    
    public class AssetBuilder extends Builder
    {
        private ProjectAsset__c record;
        
        public AssetBuilder()
        {
            record = new ProjectAsset__c();
        }
        
        public AssetBuilder withAssigned(Boolean assigned)
        {
            record.Assigned__c = assigned;
            return this;
        }
        
        public AssetBuilder withDescription(String description)
        {
            record.Description__c = description;
            return this;
        }
        
        public AssetBuilder withPassword(String pass)
        {
            record.Password__c = pass;
            return this;
        }

        public AssetBuilder withPerson(Id personId)
        {
            record.Person__c = personId;
            return this;
        }
        
        public AssetBuilder withProduct(Id productId)
        {
            record.Product__c = productId;
            return this;
        }
        
        public AssetBuilder withProject(Id projectId)
        {
            record.Project__c = projectId;
            return this;
        }
        
        public AssetBuilder withPurpose(String purpose)
        {
            record.Purpose__c = purpose;
            return this;
        }
        
        public AssetBuilder withQuantity(Decimal quantity)
        {
            record.Quantity__c = quantity;
            return this;
        }
        
        public AssetBuilder withSerialNumber(String serial)
        {
            record.Serial_Number__c = serial;
            return this;
        }
        
        public AssetBuilder withSupplier(Id supplierId)
        {
            record.Supplier__c = supplierId;
            return this;
        }
        
        public AssetBuilder withUnitPrice(Decimal unitPrice)
        {
            record.Unit_Price__c = unitPrice;
            return this;
        }
        
        public AssetBuilder withUsername(String username)
        {
            record.Username__c = username;
            return this;
        }
        
        public AssetBuilder withWebsite(String url)
        {
            record.Website__c = url;
            return this;
        }
        
        public ProjectAsset__c getRecord()
        {
            resolveDependencies();
            return record;
        }
        
        public ProjectAsset__c save()
        {
            resolveDependencies();
            upsert record;
            return record;
        }
        
        public Override void resolveDependencies()
        {
            if( record.Project__c == null )
                record.Project__c = new ProjectBuilder().save().Id;
        }
        
        public Override SObject getSObject()
        {
            return record;
        }
    }
    
    public class DependencyBuilder extends Builder
    {
        private ProjectDependency__c record;
        
        public DependencyBuilder()
        {
            record = new ProjectDependency__c();
        }
        
        public DependencyBuilder withProject(Id projectId)
        {
            record.HostProject__c = projectId;
            return this;
        }
        
        public DependencyBuilder withAssociatedProject(Id projectId)
        {
            record.AssociatedProject__c = projectId;
            return this;
        }
        
        public DependencyBuilder withDescription(String description)
        {
            record.Description__c = description;
            return this;
        }
        
        public DependencyBuilder withInstallDate(Date installDate)
        {
            record.InstallDate__c = installDate;
            return this;
        }
        
        public DependencyBuilder withProduct(Id productId)
        {
            record.Product__c = productId;
            return this;
        }
        
        public DependencyBuilder withName(String name)
        {
            record.SoftwareName__c = name;
            return this;
        }
        
        public DependencyBuilder withSupplier(Id supplierId)
        {
            record.Supplier__c = supplierId;
            return this;
        }
        
        public DependencyBuilder withUninstallDate(Date uninstallDate)
        {
            record.UninstallDate__c = uninstallDate;
            return this;
        }
        
        public DependencyBuilder withVersion(Id versionId)
        {
            record.Version__c = versionId;
            return this;
        }
        
        public DependencyBuilder withVersionNumber(String version)
        {
            record.VersionNumber__c = version;
            return this;
        }
        
        public DependencyBuilder withWebsite(String website)
        {
            record.Website__c = website;
            return this;
        }
        
        public ProjectDependency__c getRecord()
        {
            resolveDependencies();
            return record;
        }
        
        public ProjectDependency__c save()
        {
            resolveDependencies();
            upsert record;
            return record;
        }
        
        public Override void resolveDependencies()
        {
            if( isEmpty(record.SoftwareName__c) )
                record.SoftwareName__c = 'Test Dependency';

            if( record.HostProject__c == null )
                record.HostProject__c = new ProjectBuilder().save().Id;
        }
        
        public Override SObject getSObject()
        {
            return record;
        }
    }
    
}