public class GetBugController
{
    public Id Project {get; set;}
    public Id Bug {get; set;}
    
    public ProjectBug__c BugObject {get; set;}
    
    private Map<Id, Project__c> projectsById;
    private Map<Id, ProjectBug__c> bugsById;
    
    public GetBugController()
    {
        projectsById = new Map<Id, Project__c>( getMyProjects() );
    }
    
    public PageReference assign()
    {
        ProjectBug__c pBug = ProjectSelector.newInstance().selectBugById(Bug);
        pBug.Assigned_User__c = UserInfo.getUserId();
        update pBug;
        
        return new PageReference('/apex/myfeatures');
    }
    
    public PageReference cancel()
    {
        return new PageReference('/apex/myfeatures');
    }
    
    public PageReference refresh()
    {
        if(Bug != null)
        {
        	BugObject = ProjectSelector.newInstance().selectBugById(Bug);   
        }
        
        return null;
    }
    
    public List<SelectOption> getProjects()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add( new SelectOption('', ' -- ') );
        for(Project__c proj : projectsById.values())
        {
            options.add( new SelectOption(proj.Id, proj.Name) );
        }
        return options;
    }
    
    public List<SelectOption> getBugs()
    {
        if(projectsById.values().size() == 0 || Project == null)
 	       return null;
        
        bugsById = new Map<Id, ProjectBUg__c>( ProjectSelector.newInstance().selectBugsWhere( buildBugQuery() ) );
        
        List<SelectOption> options = new List<SelectOption>();
        options.add( new SelectOption('', ' -- ') );
        for(ProjectBug__c pBug : bugsById.values())
        {
            options.add( new SelectOption(pBug.Id, pBug.Name) );
        }
        return options;
    }
    
    private String buildBugQuery()
    {
        return 'Project__c = \'' + Project + '\' AND Status__c != \'Void\' AND Status__c != \'Fixed\' AND Assigned_User__c = null';
    }
    
    private List<Project__c> getMyProjects()
    {
        Id myUserId = System.UserInfo.getUserId();
        List<ProjectAsset__c> myProjectAssets = ProjectSelector.newInstance().selectAssetsWhere('Person__c = \'' + myUserId + '\'');
        
        Set<Id> myProjectIds = new Set<Id>();
        for(ProjectAsset__c asset : myProjectAssets)
        {
            if(asset.Assigned__c == true && asset.Purpose__c == 'Developer')
                myProjectIds.add(asset.Project__c);
        }
        
        if(myProjectIds.isEmpty())
            return new List<Project__c>();
        
        return ProjectSelector.newInstance().selectProjectsById(myProjectIds);
    }
}