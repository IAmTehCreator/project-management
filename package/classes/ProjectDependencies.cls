public class ProjectDependencies
{
	public class Validator implements ProjectContext.Validator
    {
        private List<ProjectDependency__c> newDependencies;
        private List<ProjectDependency__c> oldDependencies;

        public Validator(){}
        
        public Validator withOldRecords(List<SObject> oldRecords)
        {
            oldDependencies = (List<ProjectDependency__c>)oldRecords;
            return this;
        }
        
        public Validator withNewRecords(List<SObject> newRecords)
        {
            newDependencies = (List<ProjectDependency__c>)newRecords;
            return this;
        }
        
        public void validate()
        {
            for(ProjectDependency__c dependency : getDependencies())
            {
                if(ProjectContext.isRecordPreValidated( (SObject)dependency) )
                    continue;
                
                ValidationHandler handler = new ValidationHandler(dependency);
                
                if(isInsert())
                {
                    validateInsert(handler);
                    continue;
                }
                
                if(isDelete())
                {
                    validateDelete(handler);
                    continue;
                }
            }
        }
        
        private void validateInsert(ValidationHandler handler){}
        private void validateDelete(ValidationHandler handler){}
        
        private List<ProjectDependency__c> getDependencies()
        {
            if(newDependencies != null)
                return newDependencies;
            
            if(oldDependencies != null)
                return oldDependencies;
            
            return new List<ProjectDependency__c>();
        }
        
        private Boolean isInsert()
        {
            return ProjectContext.isInTrigger() && oldDependencies == null && newDependencies != null;
        }
        
        private Boolean isDelete()
        {
            return ProjectContext.isInTrigger() && oldDependencies != null && newDependencies == null;
        }
    }
    
    public class ValidationHandler implements ProjectContext.ValidationHandler
    {
        private ProjectDependency__c record;
        
        public ValidationHandler(ProjectDependency__c sObj)
        {
            record = sObj;
        }
        
        public Id getId()
        {
            return record.Id;
        }
        
        public SObject getRecord()
        {
            return record;
        }
        
        public Boolean isEqual(SObject otherRecord)
        {
            return false;
        }
        
        public void registerError(String errorMessage)
        {
            if(ProjectContext.isInTrigger())
            {
                record.addError(errorMessage);
            }
            else
            {
                throw new ProjectException(errorMessage);
            }
        }
        
        public void registerError(String fieldname, String errorMessage)
        {
            if(!ProjectContext.isInTrigger())
                throw new ProjectException(errorMessage);
        }
    }
}