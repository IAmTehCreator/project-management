@isTest(SeeAllData=false)
public class MyFeatureEditControllerTest
{
    public static final String ACTION_LOG = MyFeatureEditController.ACTION_LOG;
    public static final String ACTION_DROP = MyFeatureEditController.ACTION_DROP;
    public static final String ACTION_PLAN = MyFeatureEditController.ACTION_PLAN;
    public static final String ACTION_START = MyFeatureEditController.ACTION_START;
    public static final String ACTION_COMPLETE = MyFeatureEditController.ACTION_COMPLETE;
    public static final String ACTION_TEST = MyFeatureEditController.ACTION_TEST;
    
    public static final String TYPE_BUG = MyFeatureEditController.TYPE_BUG;
    public static final String TYPE_FEATURE = MyFeatureEditController.TYPE_FEATURE;

	private static TestMethod void controllerShouldSetPropertiesFromQueryString()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        ProjectFeature__c feature = ProjectTestScenario.createFeature().withProject(project.Id).save();
        
        ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectFeatureById().thenReturn(feature);
        selector.whenSelectProjectById().thenReturn(project);
        
        MyFeatureEditController controller = setupController(ACTION_LOG, TYPE_FEATURE, feature.Id);
        
        System.assertEquals(ACTION_LOG, controller.Mode, 'Should set the Mode property from the action query parameter');
        System.assertEquals(TYPE_FEATURE, controller.ItemType, 'Should set the ItemType property from the type query parameter');
        System.assertEquals(feature.Id, controller.RecordId, 'Should set the RecordId property from the recordid query parameter');
        System.assertEquals(1, ApexPages.getMessages().size(), 'Should add a page message');
    }
    
    private static TestMethod void controllerShouldQueryAndSetTheFeatureAndProject()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        ProjectFeature__c feature = ProjectTestScenario.createFeature().withProject(project.Id).save();
        
        ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectFeatureById().thenReturn(feature)
            							.thenAssertCalled(1)
            							.thenAssertCalledWith(feature.Id);
        selector.whenSelectProjectById().thenReturn(project)
            							.thenAssertCalled(1)
            							.thenAssertCalledWith(project.Id);
        
        MyFeatureEditController controller = setupController(ACTION_LOG, TYPE_FEATURE, feature.Id);
        
        selector.assertCalls();
    }
    
    private static TestMethod void controllerShouldQueryAndSetTheBugAndProject()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        ProjectBug__c bug = ProjectTestScenario.createBug().withProject(project.Id).save();
        
        ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectBugById().thenReturn(bug)
            							.thenAssertCalled(1)
            							.thenAssertCalledWith(bug.Id);
        selector.whenSelectProjectById().thenReturn(project)
            							.thenAssertCalled(1)
            							.thenAssertCalledWith(project.Id);
        
        MyFeatureEditController controller = setupController(ACTION_LOG, TYPE_BUG, bug.Id);
        
        selector.assertCalls();
    }
    
    private static TestMethod void controllerShouldSetTheCorrectTitle_Complete_Feature()
    {
        ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectFeatureById().thenReturn( ProjectTestScenario.createFeature().getRecord() );
        selector.whenSelectProjectById().thenReturn( ProjectTestScenario.createProject().getRecord() );
        
        MyFeatureEditController controller = setupController(ACTION_COMPLETE, TYPE_FEATURE, null);
        
        System.assertEquals('Complete feature', controller.PageTitle, 'Should set the correct page title');
        System.assertEquals('This will set remaining hours to zero and set this feature to complete, click \'Next\' to continue.', controller.Message, 'Should set the correct page message');
    }
    
    private static TestMethod void controllerShouldSetTheCorrectTitle_Complete_Bug()
    {
        ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectBugById().thenReturn( ProjectTestScenario.createBug().getRecord() );
        selector.whenSelectProjectById().thenReturn( ProjectTestScenario.createProject().getRecord() );
        
        MyFeatureEditController controller = setupController(ACTION_COMPLETE, TYPE_BUG, null);
        
        System.assertEquals('Complete bug', controller.PageTitle, 'Should set the correct page title');
        System.assertEquals('This will set remaining hours to zero and set this bug to complete, click \'Next\' to continue.', controller.Message, 'Should set the correct page message');
    }
    
    private static TestMethod void controllerShouldSetTheCorrectTitle_Log_Feature()
    {
    	ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectFeatureById().thenReturn( ProjectTestScenario.createFeature().getRecord() );
        selector.whenSelectProjectById().thenReturn( ProjectTestScenario.createProject().getRecord() );
        
        MyFeatureEditController controller = setupController(ACTION_LOG, TYPE_FEATURE, null);
        
        System.assertEquals('Log work on feature', controller.PageTitle, 'Should set the correct page title');
        System.assertEquals('This will add the set number of hours onto the total hours and deduct the set number of hours from the remaining hours. Click \'Next\' to continue.', controller.Message, 'Should set the correct page message');
    }
    
    private static TestMethod void controllerShouldSetTheCorrectTitle_Log_Bug()
    {
        ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectBugById().thenReturn( ProjectTestScenario.createBug().getRecord() );
        selector.whenSelectProjectById().thenReturn( ProjectTestScenario.createProject().getRecord() );
        
        MyFeatureEditController controller = setupController(ACTION_LOG, TYPE_BUG, null);
        
        System.assertEquals('Log work on bug', controller.PageTitle, 'Should set the correct page title');
        System.assertEquals('This will add the set number of hours onto the total hours and deduct the set number of hours from the remaining hours. Click \'Next\' to continue.', controller.Message, 'Should set the correct page message');
    }
    
    private static TestMethod void controllerShouldSetTheCorrectTitle_Drop_Feature()
    {
        ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectFeatureById().thenReturn( ProjectTestScenario.createFeature().getRecord() );
        selector.whenSelectProjectById().thenReturn( ProjectTestScenario.createProject().getRecord() );
        
        MyFeatureEditController controller = setupController(ACTION_DROP, TYPE_FEATURE, null);
        
        System.assertEquals('Drop feature', controller.PageTitle, 'Should set the correct page title');
        System.assertEquals('This will un-assign you from this feature. Click \'Next\' to continue.', controller.Message, 'Should set the correct page message');
    }
    
    private static TestMethod void controllerShouldSetTheCorrectTitle_Drop_Bug()
    {
        ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectBugById().thenReturn( ProjectTestScenario.createBug().getRecord() );
        selector.whenSelectProjectById().thenReturn( ProjectTestScenario.createProject().getRecord() );
        
        MyFeatureEditController controller = setupController(ACTION_DROP, TYPE_BUG, null);
        
        System.assertEquals('Drop bug', controller.PageTitle, 'Should set the correct page title');
        System.assertEquals('This will un-assign you from this bug. Click \'Next\' to continue.', controller.Message, 'Should set the correct page message');
    }
    
    private static TestMethod void controllerShouldSetTheCorrectTitle_Start_Feature()
    {
        ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectFeatureById().thenReturn( ProjectTestScenario.createFeature().getRecord() );
        selector.whenSelectProjectById().thenReturn( ProjectTestScenario.createProject().getRecord() );
        
        MyFeatureEditController controller = setupController(ACTION_START, TYPE_FEATURE, null);
        
        System.assertEquals('Start feature', controller.PageTitle, 'Should set the correct page title');
        System.assertEquals('This will mark the feature as started on todays date. Click \'Next\' to continue.', controller.Message, 'Should set the correct page message');
    }
    
    private static TestMethod void controllerShouldSetTheCorrectTitle_Start_Bug()
    {
        ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectBugById().thenReturn( ProjectTestScenario.createBug().getRecord() );
        selector.whenSelectProjectById().thenReturn( ProjectTestScenario.createProject().getRecord() );
        
        MyFeatureEditController controller = setupController(ACTION_START, TYPE_BUG, null);
        
        System.assertEquals('Start bug', controller.PageTitle, 'Should set the correct page title');
        System.assertEquals('This will mark the bug as started on todays date. Click \'Next\' to continue.', controller.Message, 'Should set the correct page message');
    }
    
    private static TestMethod void controllerShouldSetTheCorrectTitle_Test_Bug()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        ProjectBug__c bug = ProjectTestScenario.createBug().save();

        ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectBugById().thenReturn( bug );
        selector.whenSelectProjectById().thenReturn( project );
        
        MyFeatureEditController controller = setupController(ACTION_TEST, TYPE_BUG, null);
        
        System.assertEquals('Start testing ' + bug.Name, controller.PageTitle, 'Should set the correct page title');
        System.assertEquals('This will mark this bug as in the testing phase. Click \'Next\' to continue.', controller.Message, 'Should set the correct page message');
    }
    
    private static TestMethod void controllerShouldSetTheCorrectTitle_Plan_Feature()
    {
        ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectFeatureById().thenReturn( ProjectTestScenario.createFeature().getRecord() );
        selector.whenSelectProjectById().thenReturn( ProjectTestScenario.createProject().getRecord() );
        
        MyFeatureEditController controller = setupController(ACTION_PLAN, TYPE_FEATURE, null);
        
        System.assertEquals('Start planning feature', controller.PageTitle, 'Should set the correct page title');
        System.assertEquals('This will mark this feature as in the planning phase. Click \'Next\' to continue.', controller.Message, 'Should set the correct page message');
    }
    
    private static TestMethod void controllerShouldReturnToMyFeaturesWhenCancel()
    {
        ProjectMocks.ProjectSelector selector = mockSelector();
        selector.whenSelectFeatureById().thenReturn( ProjectTestScenario.createFeature().getRecord() );
        selector.whenSelectProjectById().thenReturn( ProjectTestScenario.createProject().getRecord() );
        
        MyFeatureEditController controller = setupController(ACTION_LOG, TYPE_FEATURE, null);
        
        System.assertEquals('/apex/sim_pm__myfeatures', controller.cancel().getUrl(), 'Should return to MyFeatures page on Cancel');
    }
    
    private static TestMethod void controllerShouldAddPageMessageWhenException()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature().save();
        MyFeatureEditController controller = setupController(ACTION_LOG, TYPE_FEATURE, feature.Id);
        
        //Mock the trigger to fail
        ProjectMocks.Validator validator = new ProjectMocks.Validator();
        FeatureTriggerListener callback = new FeatureTriggerListener(validator);
        validator.whenWithOldRecords().thenDo(callback);
        ProjectFeatures.setValidatorImplementation(validator);
        
        System.assertEquals(null, controller.next(), 'Should return null page reference');
        System.assertEquals(2, ApexPages.getMessages().size(), 'Should add a page message for the exception');
    }
    
    private static TestMethod void controllerShouldReturnToMyFeaturesWhenNext()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature().save();
        MyFeatureEditController controller = setupController(ACTION_LOG, TYPE_FEATURE, feature.Id);
        controller.Hours = 1;
        
        System.assertEquals('/apex/sim_pm__myfeatures', controller.next().getUrl(), 'Should return to MyFeatures page');
    }
    
    private static TestMethod void controllerShouldLogWork_Feature()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature()
            										   .withRemainingDevHours(3)
            										   .save();
        
        MyFeatureEditController controller = setupController(ACTION_LOG, TYPE_FEATURE, feature.Id);
        controller.Hours = 2;
        controller.next();
        
        feature = [SELECT Total_Dev_Hours__c, Remaining_Dev_Hours__c FROM ProjectFeature__c WHERE Id = :feature.Id];
        System.assertEquals(2, feature.Total_Dev_Hours__c, 'Should add the logged hours to the total hours');
        System.assertEquals(1, feature.Remaining_Dev_Hours__c, 'Should subtract the logged hours from the remaining');
    }
    
    private static TestMethod void controllerShouldLogWork_ShouldCapRemainingHoursAtZero_Feature()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature()
            										   .withRemainingDevHours(3)
            										   .save();
        
        MyFeatureEditController controller = setupController(ACTION_LOG, TYPE_FEATURE, feature.Id);
        controller.Hours = 4;
        controller.next();
        
        feature = [SELECT Total_Dev_Hours__c, Remaining_Dev_Hours__c FROM ProjectFeature__c WHERE Id = :feature.Id];
        System.assertEquals(4, feature.Total_Dev_Hours__c, 'Should add the logged hours to the total hours');
        System.assertEquals(0, feature.Remaining_Dev_Hours__c, 'Should cap the remaining hours at zero');
    }
    
    private static TestMethod void controllerShouldLogWork_Bug()
    {
        ProjectBug__c bug = ProjectTestScenario.createBug()
            								   .withRemainingHours(3)
            								   .save();
        
        MyFeatureEditController controller = setupController(ACTION_LOG, TYPE_BUG, bug.Id);
        controller.Hours = 2;
        controller.next();
        
        bug = [SELECT Total_Dev_Hours__c, Remaining_Dev_Hours__c FROM ProjectBug__c WHERE Id = :bug.Id];
        System.assertEquals(2, bug.Total_Dev_Hours__c, 'Should add the logged hours to the total hours');
        System.assertEquals(1, bug.Remaining_Dev_Hours__c, 'Should subtract the logged hours from the remaining');
    }

    private static TestMethod void controllerShouldLogWork_ShouldCapRemainingHoursAtZero_Bug()
    {
        ProjectBug__c bug = ProjectTestScenario.createBug()
            								   .withRemainingHours(3)
            								   .save();
        
        MyFeatureEditController controller = setupController(ACTION_LOG, TYPE_BUG, bug.Id);
        controller.Hours = 4;
        controller.next();
        
        bug = [SELECT Total_Dev_Hours__c, Remaining_Dev_Hours__c FROM ProjectBug__c WHERE Id = :bug.Id];
        System.assertEquals(4, bug.Total_Dev_Hours__c, 'Should add the logged hours to the total hours');
        System.assertEquals(0, bug.Remaining_Dev_Hours__c, 'Should subtract the logged hours from the remaining');
    }

    private static TestMethod void controllerShouldDrop_Feature()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature()
            .withUser( UserInfo.getUserId() )
            .save();
        
        MyFeatureEditController controller = setupController(ACTION_DROP, TYPE_FEATURE, feature.Id);
        controller.next();
        
        feature = [SELECT Assigned_User__c FROM ProjectFeature__c WHERE Id = :feature.Id];
        System.assertEquals(null, feature.Assigned_User__c, 'Should unassign the user');
    }

    private static TestMethod void controllerShouldDrop_Bug()
    {
        ProjectBug__c bug = ProjectTestScenario.createbug()
            .withAssignedUser( UserInfo.getUserId() )
            .save();
        
        MyFeatureEditController controller = setupController(ACTION_DROP, TYPE_BUG, bug.Id);
        controller.next();
        
        bug = [SELECT Assigned_User__c FROM ProjectBug__c WHERE Id = :bug.Id];
        System.assertEquals(null, bug.Assigned_User__c, 'Should unassign the user');
    }

    private static TestMethod void controllerShouldPlanFeature()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature()
            										   .withStatus(ProjectFeatures.STATUS_PENDING)
            										   .save();
        
        MyFeatureEditController controller = setupController(ACTION_PLAN, TYPE_FEATURE, feature.Id);
        controller.next();
        
        feature = [SELECT Status__c FROM ProjectFeature__c WHERE Id = :feature.Id];
        System.assertEquals(ProjectFeatures.STATUS_PLANNING, feature.Status__c, 'Should set status to "Planning"');
    }
    
    private static TestMethod void controllerShouldStart_Feature()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature()
            										   .withStatus(ProjectFeatures.STATUS_PLANNING)
            										   .save();
        
        MyFeatureEditController controller = setupController(ACTION_START, TYPE_FEATURE, feature.Id);
        controller.next();
        
        feature = [SELECT Status__c FROM ProjectFeature__c WHERE Id = :feature.Id];
        System.assertEquals(ProjectFeatures.STATUS_IN_PROGRESS, feature.Status__c, 'Should set status to "In Progress"');
    }

    private static TestMethod void controllerShouldStart_Bug()
    {
        ProjectBug__c bug = ProjectTestScenario.createBug()
            								   .withStatus(ProjectBugs.STATUS_PENDING)
            								   .save();
        
        MyFeatureEditController controller = setupController(ACTION_START, TYPE_BUG, bug.Id);
        controller.next();
        
        bug = [SELECT Status__c FROM ProjectBug__c WHERE Id = :bug.Id];
        System.assertEquals(ProjectBugs.STATUS_FIXING, bug.Status__c, 'Should set status to "Fixing"');
    }
    
    private static TestMethod void controllerShouldComplete_Feature()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature()
            										   .withStatus(ProjectFeatures.STATUS_IN_PROGRESS)
            										   .withRemainingDevHours(1)
            										   .save();
        
        MyFeatureEditController controller = setupController(ACTION_COMPLETE, TYPE_FEATURE, feature.Id);
        controller.next();
        
        feature = [SELECT Status__c, Remaining_Dev_Hours__c FROM ProjectFeature__c WHERE Id = :feature.Id];
        System.assertEquals(ProjectFeatures.STATUS_COMPLETE, feature.Status__c, 'Should set status to "In Progress"');
        System.assertEquals(0, feature.Remaining_Dev_Hours__c, 'Should set remaining dev hours to zero');
    }

    private static TestMethod void controllerShouldComplete_Bug()
    {
        ProjectBug__c bug = ProjectTestScenario.createBug()
            								   .withStatus(ProjectBugs.STATUS_TESTING)
            								   .save();
        
        MyFeatureEditController controller = setupController(ACTION_COMPLETE, TYPE_BUG, bug.Id);
        controller.next();
        
        bug = [SELECT Status__c FROM ProjectBug__c WHERE Id = :bug.Id];
        System.assertEquals(ProjectBugs.STATUS_FIXED, bug.Status__c, 'Should set status to "Fixed"');
    }
    
    private static TestMethod void controllerShouldTestBug()
    {
        ProjectBug__c bug = ProjectTestScenario.createBug()
            								   .withStatus(ProjectBugs.STATUS_FIXING)
            								   .save();
        
        MyFeatureEditController controller = setupController(ACTION_TEST, TYPE_BUG, bug.Id);
        controller.next();
        
        bug = [SELECT Status__c FROM ProjectBug__c WHERE Id = :bug.Id];
        System.assertEquals(ProjectBugs.STATUS_TESTING, bug.Status__c, 'Should set status to "Testing"');
    }
    
    private class FeatureTriggerListener implements sim_util.Mocks.MethodListener
    {
        private ProjectMocks.Validator validator;
        
        public FeatureTriggerListener(ProjectMocks.Validator validator)
        {
			this.validator = validator;
        }
        
        public Object execute(List<Object> args)
        {
            ProjectFeature__c feature = ((List<ProjectFeature__c>)validator.whenWithNewRecords().wasCalled().getArguments(0)[0])[0];
            feature.addError('TestError');
            return null;
        }
    }
    
    private static MyFeatureEditController setupController(String action, String itemType, String recordId)
    {
        Map<String, String> params = ApexPages.currentPage().getParameters();
        params.put('action', action);
        params.put('type', itemType);
        params.put('recordid', recordId);
        
        return new MyFeatureEditController();
    }
    
    private static ProjectMocks.ProjectSelector mockSelector()
    {
        ProjectMocks.ProjectSelector selector = new ProjectMocks.ProjectSelector();
        ProjectSelector.setImplementation(selector);
        return selector;
    }
}