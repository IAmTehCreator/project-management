public class MyFeaturesController
{
    private String userId;
    
    private List<TodoItem> featureItems;
    private List<TodoItem> bugItems;
    
    private Map<Id, String> projectNames;
    
    public MyFeaturesController()
    {
        userId = UserInfo.getUserId();
        
        List<ProjectFeature__c> features = ProjectSelector.newInstance().selectFeaturesWhere( buildQueryString() );
        List<ProjectBug__c> bugs = ProjectSelector.newInstance().selectBugsWhere( buildQueryString() );
        projectNames = new Map<Id, String>();
        buildProjectNameList(features, bugs);
        
        featureItems = createItemList(features);
        bugItems = createItemList(bugs);
        
        if(featureItems.size() == 0 && bugItems.size() == 0)
        {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO, 'You have no assigned features or bugs, to assign yourself a feature or bug use the \'Assign Feature\' or the \'Assign Bug\' buttons.') );
        }
    }
    
    public List<TodoItem> getFeatures()
    {
        return featureItems;
    }
    
    public List<TodoItem> getBugs()
    {
        return bugItems;
    }
    
    public Boolean getHasFeatures()
    {
        return featureItems.size() > 0;
    }
    
    public Boolean getHasBugs()
    {
        return bugItems.size() > 0;
    }
    
    public PageReference newFeature()
    {
        return new PageReference('/apex/getfeature');
    }
    
    public PageReference newBug()
    {
		return new PageReference('/apex/getbug');
    }
    
    public String getProjectName(Id projectId)
    {
        return projectNames.get(projectId);
    }
    
    public Decimal getFeatureHoursTotal()
    {
        Decimal total = 0;
        for(TodoItem feature : featureItems)
        {
            total += feature.Hours;
        }
        return total;
    }
    
    public Decimal getBugHoursTotal()
    {
        Decimal total = 0;
        for(TodoItem bug : bugItems)
        {
            total += bug.Hours;
        }
        return total;
    }
    
    private void buildProjectNameList(List<ProjectFeature__c> features, List<ProjectBug__c> bugs)
    {
        Set<Id> ids = new Set<Id>();
		for(ProjectFeature__c feature : features)
        {
            ids.add(feature.Project__c);
        }
        
        for(ProjectBug__c bug : bugs)
        {
            ids.add(bug.Project__c);
        }
        
        List<Project__c> projects = ProjectSelector.newInstance().selectProjectsById(ids);
        
        for(Project__c proj : projects)
        {
            projectNames.put(proj.Id, proj.Name);
        }
    }
    
    private String buildQueryString()
    {
        return 'Assigned_User__c = \'' + userId + '\' AND Status__c != \'Complete\' AND Status__c != \'Void\' AND Status__c != \'Fixed\' ';
    }
    
    private List<TodoItem> createItemList(List<ProjectFeature__c> features)
    {
		List<TodoItem> items = new List<TodoItem>();
        for(ProjectFeature__c feature : features)
        {
			items.add(new TodoItem(feature, getProjectName(feature.Project__c)));
        }
        return items;
    }
    
    private List<TodoItem> createItemList(List<ProjectBug__c> bugs)
    {
		List<TodoItem> items = new List<TodoItem>();
        for(ProjectBug__c bug : bugs)
        {
			items.add(new TodoItem(bug, getProjectName(bug.Project__c)));
        }
        return items;
    }
    
    public class TodoItem
    {
        public String ProjectName {get; set;}
        public String ProjectId {get; set;}
        public String RecordId {get; set;}
        public String Name {get; set;}
        public String Label {get; set;}
        public String Status {get; set;}
        public String Description {get; set;}
        public Decimal Hours {get; set;}
        
        public TodoItem(ProjectFeature__c feature, String projName)
        {
            ProjectName = projName;
            ProjectId = feature.Project__c;
            RecordId = feature.Id;
            Name = feature.Name;
            Label = feature.Name__c;
            Status = feature.Status__c;
            Description = feature.Description__c;
            Hours = feature.Remaining_Dev_Hours__c;
		}
        
        public TodoItem(ProjectBug__c bug, String projName)
        {
            ProjectName = projName;
            ProjectId = bug.Project__c;
            ProjectId = bug.Project__c;
            RecordId = bug.Id;
            Name = bug.Name;
            Status = bug.Status__c;
            Description = bug.Description__c;
            Hours = bug.Remaining_Dev_Hours__c;
        }
    }
    
}