public class CreateContractFromOppController
{
    
    public Contract NewContract {get; set;}
    public Order NewOrder {get; set;}
    public Boolean IsValid {get; set;}
    
    private Opportunity opp;
    
    public CreateContractFromOppController(ApexPages.StandardController stdController)
    {
        opp = (Opportunity)stdController.getRecord();
        IsValid = true;
        
        NewContract = new Contract();
        NewOrder = new Order();
    }
    
    public PageReference createContract()
    {
        ContractService.ContractSettings settings = new ContractService.ContractSettings();
        settings.Opp = opp;
        settings.StartDate = NewContract.StartDate;
        settings.Periods = NewContract.ContractTerm;
        
        PageReference ref;
        try
        {
            Id contractId = ContractService.newInstance().createFromOpportunity(settings);
            ref = new PageReference('/' + contractId);
        }
        catch(Exception e)
        {
            ref = null;
            setInvalidAndAddMessage(e.getMessage());
        }
        
    	return ref;   
    }
    
    private void validateOpportunity()
    {
        if(opp.AccountId == null)
            setInvalidAndAddMessage('The opportunity must be associated with an account.');
    }
    
    private void setInvalidAndAddMessage(String message)
    {
        IsValid = false;
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, message);
        ApexPages.addMessage(msg);
    }

}