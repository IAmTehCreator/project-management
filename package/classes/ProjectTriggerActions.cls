public class ProjectTriggerActions
{
    /**
     * Trigger action for project bug trigger, sends chatter posts and emails to all developers associated
     * with the projects that the new bugs were recorded against.
     * NOTE: Expects the new bug SObjects as the first parameter
     */
	public class SendNotificationToProjectDevelopers implements sim_util.EventBus.Callback
    {
        public void call(sim_util.EventBus.Event event)
        {
            List<ProjectBug__c> bugs = (List<ProjectBug__c>)event.Arguments.get('newRecords');
            Map<Project__c, List<ProjectAsset__c>> developers = filterAssets( ProjectSelector.newInstance().selectAssetsMappedByProjectId( getProjectIds(bugs) ) );
            
            List<NotificationService.Notification> notifications = createNotifications(developers);
            if(!notifications.isEmpty())
	            NotificationService.newInstance().sendNotifications(notifications, true, true);
        }
        
        private List<NotificationService.Notification> createNotifications(Map<Project__c, List<ProjectAsset__c>> developersByProject)
        {
            List<NotificationService.Notification> notifications = new List<NotificationService.Notification>();
            
            for(Project__c project : developersByProject.keySet())
            {
                for(ProjectAsset__c developer : developersByProject.get(project))
                {
                    NotificationService.Notification notification = new NotificationService.Notification();
                    notification.User = developer.Person__c;
                    notification.Subject = 'New ' + project.Name + ' bug reported';
                    notification.Message = 'A new bug has been reported on ' + project.Name + '.';
                    
                    notifications.add(notification);
                }
            }
            
            return notifications;
        }
        
        private Set<Id> getProjectIds(List<ProjectBug__c> bugs)
        {
            Set<Id> ids = new Set<Id>();
            for(ProjectBug__c bug : bugs)
            {
                ids.add(bug.Project__c);
            }
            return ids;
        }
        
        private Map<Project__c, List<ProjectAsset__c>> filterAssets(Map<Id, List<ProjectAsset__c>> assetsMappedByProject)
        {
            Map<Id, Project__c> projects = new Map<Id, Project__c>(ProjectSelector.newInstance().selectProjectsById( assetsMappedByProject.keySet() ));
            
            Map<Project__c, List<ProjectAsset__c>> peopleMappedByProject = new Map<Project__c, List<ProjectAsset__c>>();
            for(Id projectId : assetsMappedByProject.keySet())
            {
                List<ProjectAsset__c> assets = assetsMappedByProject.get(projectId);
                
                List<ProjectAsset__c> developers = new List<ProjectAsset__c>();
                for(ProjectAsset__c asset : assets)
                {
                    if(asset.Person__c == null)
                        continue;
                    
                    if(asset.Assigned__c == true && asset.Purpose__c == 'Developer')
                        developers.add(asset);
                }
                
                peopleMappedByProject.put(projects.get(projectId), developers);
            }
            
            return peopleMappedByProject;
        }
    }
}