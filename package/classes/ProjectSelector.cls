public class ProjectSelector
{
    public interface API
    {
        Project__c selectProjectById(Id projectId);
		List<Project__c> selectProjectsById(Set<Id> projectIds);
        List<Project__c> selectProjectsWhere(String whereClause);
        
        List<ProjectVersion__c> selectVersionsByProjectId(Id projectId);
        List<ProjectFeature__c> selectFeaturesByProjectId(Id projectId);
        List<ProjectBug__c> selectBugsByProjectId(Id projectId);
        List<ProjectAsset__c> selectAssetsByProjectId(Id projectId);
        
        ProjectVersion__c selectVersionById(Id versionId);
        List<ProjectVersion__c> selectVersionsById(Set<Id> versionIds);
        List<ProjectVersion__c> selectVersionsWhere(String whereClause);
        
        ProjectFeature__c selectFeatureById(Id featureId);
        List<ProjectFeature__c> selectFeaturesById(Set<Id> featureIds);
        List<ProjectFeature__c> selectFeaturesWhere(String whereClause);
        
        ProjectBug__c selectBugById(Id bugId);
        List<ProjectBug__c> selectBugsById(Set<Id> bugIds);
        List<ProjectBug__c> selectBugsWhere(String whereClause);

		ProjectAsset__c selectAssetById(Id assetId);        
        List<ProjectAsset__c> selectAssetsById(Set<Id> assetIds);
        List<ProjectAsset__c> selectAssetsWhere(String whereClause);
        Map<Id, List<ProjectAsset__c>> selectAssetsMappedByProjectID(Set<Id> projectIds);
        
        ProjectDependency__c selectDependencyById(Id dependencyId);
        List<ProjectDependency__c> selectDependenciesById(Set<Id> ids);
        List<ProjectDependency__c> selectDependenciesWhere(String whereClause);
    }
    
    private static ProjectSelector.API implementation;
    
    public static ProjectSelector.API newInstance()
    {
		if(implementation == null)
        {
			return new ProjectSelector.ServiceImplementation();
        }
        else
        {
            return implementation;
        }
    }
    
    public static void setImplementation(ProjectSelector.API impl)
    {
		implementation = impl;
    }
    
    public class ServiceImplementation extends SObjectSelector implements ProjectSelector.API
    {
        public Project__c selectProjectById(Id projectId)
        {
            if(projectId == null)
                return null;
            
            return Database.query( buildQuery( getProjectFieldList(), 'Project__c', 'Id =: projectId LIMIT 1' ) );
        }
        
		public List<Project__c> selectProjectsById(Set<Id> projectIds)
        {
            if(projectIds == null || projectIds.isEmpty())
                return new List<Project__c>();
            
            return Database.query( buildQuery( getProjectFieldList(), 'Project__c', 'Id IN :projectIds' ) );
        }
        
        public List<Project__c> selectProjectsWhere(String whereClause)
        {
            if(whereClause == null || whereClause == '')
                return new List<Project__c>();
           	
            return Database.query( buildQuery( getProjectFieldList(), 'Project__c', whereClause) );
        }
        
        public List<ProjectVersion__c> selectVersionsByProjectId(Id projectId)
        {
			if(projectId == null)
                return new List<ProjectVersion__c>();
            
            return Database.query( buildQuery( getVersionFieldList(), 'ProjectVersion__c', 'Project__c =: projectId' ) );
        }
        
        public List<ProjectFeature__c> selectFeaturesByProjectId(Id projectId)
        {
            if(projectId == null)
                return new List<ProjectFeature__c>();
            
            return Database.query( buildQuery( getFeatureFieldList(), 'ProjectFeature__c', 'Project__c =: projectId' ) );
        }
        
        public List<ProjectBug__c> selectBugsByProjectId(Id projectId)
        {
            if(projectId == null)
                return new List<ProjectBug__c>();
            
            return Database.query( buildQuery( getBugFieldList(), 'ProjectBug__c', 'Project__c =: projectId' ) );
        }
        
        public List<ProjectAsset__c> selectAssetsByProjectId(Id projectId)
        {
            if(projectId == null)
                return new List<ProjectAsset__c>();
            
            return Database.query( buildQuery( getAssetFieldList(), 'ProjectAsset__c', 'Project__c =: projectId' ) );
        }
        
        public ProjectVersion__c selectVersionById(Id versionId)
        {
            if(versionId == null)
                return null;
            
            return Database.query( buildQuery( getVersionFieldList(), 'ProjectVersion__c', 'Id =: versionId') );
        }
        
        public List<ProjectVersion__c> selectVersionsById(Set<Id> versionIds)
        {
            if(versionIds == null || versionIds.isEmpty())
                return new List<ProjectVersion__c>();
            
            return Database.query( buildQuery( getVersionFieldList(), 'ProjectVersion__c', 'Id IN :versionIds') );
        }
        
        public List<ProjectVersion__c> selectVersionsWhere(String whereClause)
        {
            if(whereClause == null || whereClause == '')
                return new List<ProjectVersion__c>();
           	
            return Database.query( buildQuery( getVersionFieldList(), 'ProjectVersion__c', whereClause) );
        }
        
        public ProjectFeature__c selectFeatureById(Id featureId)
        {
            if(featureId == null)
                return null;
            
            return Database.query( buildQuery( getFeatureFieldList(), 'ProjectFeature__c', 'Id =: featureId') );
        }
        
        public List<ProjectFeature__c> selectFeaturesById(Set<Id> featureIds)
        {
            if(featureIds == null || featureIds.isEmpty())
                return new List<ProjectFeature__c>();
           	
            return Database.query( buildQuery( getFeatureFieldList(), 'ProjectFeature__c', 'Id IN :featureIds') );
        }
        
        public List<ProjectFeature__c> selectFeaturesWhere(String whereClause)
        {
            if(whereClause == null || whereClause == '')
                return new List<ProjectFeature__c>();
           	
            return Database.query( buildQuery( getFeatureFieldList(), 'ProjectFeature__c', whereClause) );
        }
        
        public ProjectBug__c selectBugById(Id bugId)
        {
            if(bugId == null)
                return null;
            
            return Database.query( buildQuery( getBugFieldList(), 'ProjectBug__c', 'Id =: bugId') );
        }
        
        public List<ProjectBug__c> selectBugsById(Set<Id> bugIds)
        {
            if(bugIds == null || bugIds.isEmpty())
                return new List<ProjectBug__c>();
            
            return Database.query( buildQuery( getBugFieldList(), 'ProjectBug__c', 'Id IN :bugIds') );
        }
        
        public List<ProjectBug__c> selectBugsWhere(String whereClause)
        {
            if(whereClause == null || whereClause == '')
                return new List<ProjectBug__c>();
           	
            return Database.query( buildQuery( getBugFieldList(), 'ProjectBug__c', whereClause) );
        }
        
        public ProjectAsset__c selectAssetById(Id assetId)
        {
            if(assetId == null)
                return null;
            
            return Database.query( buildQuery( getAssetFieldList(), 'ProjectAsset__c', 'Id =: assetId') );
        }
        
        public List<ProjectAsset__c> selectAssetsById(Set<Id> assetIds)
        {
            if(assetIds == null || assetIds.isEmpty())
                return new List<ProjectAsset__c>();
            
            return Database.query( buildQuery( getAssetFieldList(), 'ProjectAsset__c', 'Id IN :assetIds') );
        }
        
        public List<ProjectAsset__c> selectAssetsWhere(String whereClause)
        {
            if(whereClause == null || whereClause == '')
                return new List<ProjectAsset__c>();
           	
            return Database.query( buildQuery( getAssetFieldList(), 'ProjectAsset__c', whereClause) );
        }
        
        public Map<Id, List<ProjectAsset__c>> selectAssetsMappedByProjectID(Set<Id> projectIds)
        {
            if(projectIds == null || projectIds.isEmpty())
                return new Map<Id, List<ProjectAsset__c>>();
            
            List<ProjectAsset__c> assets = Database.query( buildQuery( getAssetFieldList(), 'ProjectAsset__c', 'Project__c IN :projectIds' ) );
            
            return (Map<Id, List<ProjectAsset__c>>)mapByIdField(assets, 'Project__c');
        }
        
        public ProjectDependency__c selectDependencyById (Id dependencyId)
        {
            if(dependencyId == null)
                return null;
            
            return Database.query( buildQuery( getDependencyFieldList(), 'ProjectDependency__c', 'Id =: dependencyId') );
        }
        
        public List<ProjectDependency__c> selectDependenciesById(Set<Id> ids)
        {
            if(ids == null || ids.isEmpty())
                return new List<ProjectDependency__c>();
            
            return Database.query( buildQuery( getDependencyFieldList(), 'ProjectDependency__c', 'Id IN :ids') );
        }
        
        public List<ProjectDependency__c> selectDependenciesWhere(String whereClause)
        {
            if(whereClause == null || whereClause == '')
                return new List<ProjectDependency__c>();
           	
            return Database.query( buildQuery( getDependencyFieldList(), 'ProjectDependency__c', whereClause) );
        }
        
        private Map<Id, List<SObject>> mapByIdField(List<SObject> sObjects, String field)
        {
            Map<Id, List<SObject>> idMap = new Map<Id, List<SObject>>();
			for(SObject sObj : sObjects)
            {
                Id fieldValue = (Id)sObj.get(field);
                
                if(idMap.get(fieldValue) == null)
                    idMap.put(fieldValue, new List<SObject>());
                
                idMap.get(fieldValue).add(sObj);
            }
            return idMap;
        }
        
        private List<String> getProjectFieldList()
        {
        	List<String> fields = new List<String>();
            fields.add('Name');
            fields.add('Budget__c');
            fields.add('Complete_Date__c');
            fields.add('Completed_Features__c');
            fields.add('Contract__c');
            fields.add('Current_Bugs__c');
            fields.add('Current_Version__c');
            fields.add('Customer__c');
            fields.add('Description__c');
            fields.add('Features__c');
            fields.add('Manager_User__c');
            fields.add('Manager_Contact__c');
            fields.add('Platform__c');
            fields.add('Product__c');
            fields.add('Remaining_Bug_Hours__c');
            fields.add('Remaining_Feature_Hours__c');
            fields.add('Repository__c');
            fields.add('Source_Control__c');
            fields.add('Started_Date__c');
            fields.add('Status__c');
            fields.add('Total_Asset_Value__c');
            fields.add('Total_Staff__c');
            fields.add('Type__c');
            fields.add('Website__c');
            return fields;
        }
        
        private List<String> getVersionFieldList()
        {
        	List<String> fields = new List<String>();
            fields.add('Name');
            fields.add('ChangeLog__c');
            fields.add('Current__c');
            fields.add('Project__c');
            fields.add('Release_Date__c');
            fields.add('Status__c');
            fields.add('URL__c');
            fields.add('Release_Tag__c');
            return fields;
        }
        
        private List<String> getFeatureFieldList()
        {
        	List<String> fields = new List<String>();
            fields.add('Name');
            fields.add('Complete_Date__c');
            fields.add('Description__c');
            fields.add('Estimated_Dev_Hours__c');
            fields.add('Name__c');
            fields.add('Project__c');
            fields.add('Release__c');
            fields.add('Remaining_Dev_Hours__c');
            fields.add('Start_Date__c');
            fields.add('Status__c');
            fields.add('Total_Dev_Hours__c');
            fields.add('Assigned_User__c');
            return fields;
        }
        
        private List<String> getBugFieldList()
        {
        	List<String> fields = new List<String>();
            fields.add('Name');
            fields.add('Description__c');
            fields.add('Estimated_Hours__c');
            fields.add('Fixed_Date__c');
            fields.add('Fixed_In_Version__c');
            fields.add('Found_Date__c');
            fields.add('Found_Version__c');
            fields.add('Project__c');
            fields.add('Remaining_Dev_Hours__c');
            fields.add('Status__c');
            fields.add('Steps_to_Recreate__c');
            fields.add('Total_Dev_Hours__c');
            fields.add('Assigned_User__c');
            return fields;
        }
        
        private List<String> getAssetFieldList()
        {
            List<String> fields = new List<String>();
            fields.add('Assigned__c');
            fields.add('Description__c');
            fields.add('Password__c');
            fields.add('Person__c');
            fields.add('Product__c');
            fields.add('Project__c');
            fields.add('Purpose__c');
            fields.add('Quantity__c');
            fields.add('Serial_Number__c');
            fields.add('Supplier__c');
            fields.add('Total__c');
            fields.add('Unit_Price__c');
            fields.add('Username__c');
            fields.add('Website__c');
            return fields;
        }
        
        private List<String> getDependencyFieldList()
        {
            List<String> fields = new List<String>();
            fields.add('Name');
            fields.add('AssociatedProject__c');
            fields.add('Current__c');
            fields.add('Deprecated__c');
            fields.add('Description__c');
            fields.add('HostProject__c');
            fields.add('InstallDate__c');
            fields.add('Product__c');
            fields.add('SoftwareName__c');
            fields.add('Supplier__c');
            fields.add('UninstallDate__c');
            fields.add('Version__c');
            fields.add('VersionNumber__c');
            fields.add('Website__c');
            return fields;
        }
    }
    
}