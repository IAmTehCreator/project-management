public class GetFeatureController
{
    public Id Project {get; set;}
    public Id Feature {get; set;}
    
    public ProjectFeature__c FeatureObject {get; set;}
    
    private Map<Id, Project__c> projectsById;
    private Map<Id, ProjectFeature__c> featuresById;
    
    public GetFeatureController()
    {
        projectsById = new Map<Id, Project__c>( getMyProjects() );
    }
    
    public PageReference assign()
    {
        ProjectFeature__c feat = ProjectSelector.newInstance().selectFeatureById(Feature);
        feat.Assigned_User__c = UserInfo.getUserId();
        update feat;
        
        return new PageReference('/apex/myfeatures');
    }
    
    public PageReference cancel()
    {
        return new PageReference('/apex/myfeatures');
    }
    
    public PageReference refresh()
    {
        if(Feature != null)
        {
        	FeatureObject = ProjectSelector.newInstance().selectFeatureById(Feature);
        }
        
        return null;
    }
    
    public List<SelectOption> getProjects()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add( new SelectOption('', ' -- ') );
        for(Project__c proj : projectsById.values())
        {
            options.add( new SelectOption(proj.Id, proj.Name) );
        }
        return options;
    }
    
    public List<SelectOption> getFeatures()
    {
        if(projectsById.values().size() == 0 || Project == null)
 	       return null;
        
        featuresById = new Map<Id, ProjectFeature__c>( ProjectSelector.newInstance().selectFeaturesWhere( buildFeatureQuery() ) );
        
        List<SelectOption> options = new List<SelectOption>();
        options.add( new SelectOption('', ' -- ') );
        for(ProjectFeature__c feat : featuresById.values())
        {
            options.add( new SelectOption(feat.Id, feat.Name + ' - ' + feat.Name__c) );
        }
        return options;
    }
    
    private String buildFeatureQuery()
    {
        return 'Project__c = \'' + Project + '\' AND Release__c = \'' + projectsById.get(Project).Current_Version__c + '\' AND Status__c != \'Void\' AND Status__c != \'Complete\' AND Assigned_User__c = null';
    }
    
    private List<Project__c> getMyProjects()
    {
        Id myUserId = System.UserInfo.getUserId();
        List<ProjectAsset__c> myProjectAssets = ProjectSelector.newInstance().selectAssetsWhere('Person__c = \'' + myUserId + '\'');
        
        Set<Id> myProjectIds = new Set<Id>();
        for(ProjectAsset__c asset : myProjectAssets)
        {
            if(asset.Assigned__c == true && asset.Purpose__c == 'Developer')
                myProjectIds.add(asset.Project__c);
        }
        
        if(myProjectIds.isEmpty())
            return new List<Project__c>();
        
        return ProjectSelector.newInstance().selectProjectsById(myProjectIds);
    }
}