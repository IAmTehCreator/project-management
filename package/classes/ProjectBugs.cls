public class ProjectBugs
{
    public static final String STATUS_PENDING = 'Pending';
    public static final String STATUS_FIXING = 'Fixing';
    public static final String STATUS_TESTING = 'Testing';
    public static final String STATUS_VOID = 'Void';
    public static final String STATUS_FIXED = 'Fixed';
    
	private static ProjectContext.Validator validatorImplementation;
    
    public static ProjectContext.Validator newValidator()
    {
        if(validatorImplementation == null)
        {
            return new Validator();
        }
        else
        {
            return validatorImplementation;
        }
    }
    
    public static void setValidatorImplementation(ProjectContext.Validator impl)
    {
		validatorImplementation = impl;
    }
    
	public class Validator implements ProjectContext.Validator
    {
        private List<ProjectBug__c> newBugs;
        private List<ProjectBug__c> oldBugs;
        
        public Boolean ValidateCantLogBugsAgainstDeprecatedProjects {get; set;}
        public Boolean ValidateCantLogBugsAgainstDeprecatedVersions {get; set;}
        public Boolean ValidateCantReparent {get; set;}
        public Boolean ValidateCantModifyBugFromDeprecatedOrCompleteProject {get; set;}
        public Boolean ValidateFixedBugMustHaveFixedDate {get; set;}
        public Boolean ValidateFixedBugMustHaveFixedVersion {get; set;}
        public Boolean ValidateCantDeleteNonVoid {get; set;}
        
        private Map<Id, Project__c> ownerProjects;
        private Map<Id, ProjectVersion__c> foundVersions;
        
        public Validator()
        {
            ValidateCantLogBugsAgainstDeprecatedProjects = true;
            ValidateCantLogBugsAgainstDeprecatedVersions = true;
            ValidateCantReparent = true;
            ValidateCantModifyBugFromDeprecatedOrCompleteProject = true;
            ValidateFixedBugMustHaveFixedDate = true;
            ValidateFixedBugMustHaveFixedVersion = true;
            ValidateCantDeleteNonVoid = true;
        }
        
        public Validator withOldRecords(List<SObject> oldRecords)
        {
            oldBugs = (List<ProjectBug__c>)oldRecords;
			return this;
        }
        
        public Validator withNewRecords(List<SObject> newRecords)
        {
            newBugs = (List<ProjectBug__c>)newRecords;
            return this;
        }
        
        public Validator exceptCantLogBugsAgainstDeprecatedProjects()
        {
            ValidateCantLogBugsAgainstDeprecatedProjects = false;
            return this;
        }
        
        public Validator exceptCantLogBugsAgainstDeprecatedVersions()
        {
            ValidateCantLogBugsAgainstDeprecatedVersions = false;
            return this;
        }
        
        public Validator exceptCantReparent()
        {
            ValidateCantReparent = false;
            return this;
        }
        
        public Validator exceptCantModifyBugFromDeprecatedOrCompleteProject()
        {
            ValidateCantModifyBugFromDeprecatedOrCompleteProject = false;
            return this;
        }
        
        public Validator exceptFixedBugMustHaveFixedDate()
        {
			ValidateFixedBugMustHaveFixedDate = false;
            return this;
        }
        
        public Validator exceptFixedBugMustHaveFixedVersion()
        {
            ValidateFixedBugMustHaveFixedVersion = false;
            return this;
        }
        
        public Validator exceptCantDeleteNonVoid()
        {
            ValidateCantDeleteNonVoid = false;
            return this;
        }
        
        public void validate()
        {
            for(ProjectBug__c bug : getBugs())
            {
                if(ProjectContext.isRecordPreValidated( (SObject)bug) )
                    continue;
                
                ValidationHandler handler = new ValidationHandler(bug);
                
                if(isInsert())
                {
                    validateInsert(handler);
                    continue;
                }
                
                if(isDelete())
                {
                    validateDelete(handler);
                    continue;
                }
                
                //Update validation, if the handler doesn't have an ID then something went wrong!
            	if(handler.getId() == null)
                	handler.registerError('Update trigger but record has no ID');
                
                if(ValidateCantReparent)
                    doValidateCantReparent(handler);
                
                if(ValidateCantModifyBugFromDeprecatedOrCompleteProject)
                    doValidateCantModifyBugFromDeprecatedOrCompleteProject(handler);
                
                if(ValidateFixedBugMustHaveFixedDate)
                    doValidateFixedBugMustHaveFixedDate(handler);
                
                if(ValidateFixedBugMustHaveFixedVersion)
                    doValidateFixedBugMustHaveFixedVersion(handler);
                
                if(!ProjectContext.isInTrigger())
                    ProjectContext.registerPreValidated(handler);
            }
        }
        
        public void validateInsert(ValidationHandler handler)
        {
            if(ValidateCantLogBugsAgainstDeprecatedProjects)
                doValidateCantLogBugsAgainstDeprecatedProjects(handler);
            
            if(ValidateCantLogBugsAgainstDeprecatedVersions)
            	doValidateCantLogBugsAgainstDeprecatedVersions(handler);
        }
        
        public void validateDelete(ValidationHandler handler)
        {
            if(ValidateCantDeleteNonVoid)
                doValidateCantDeleteNonVoid(handler);
        }
        
        private void doValidateCantReparent(ValidationHandler handler)
        {
            ProjectBug__c oldBug = getOldRecord( handler.getId() );
            if(((ProjectBug__c)handler.getRecord()).Project__c != oldBug.Project__c)
                handler.registerError('Project__c', 'Cannot reparent a project bug.');
        }
        
        private void doValidateCantModifyBugFromDeprecatedOrCompleteProject(ValidationHandler handler)
        {
            Project__c owner = getProject( ((ProjectBug__c)handler.getRecord()).Project__c );
            if(owner.Status__c == Projects.STATUS_DEPRECATED || owner.Status__c == Projects.STATUS_COMPLETE)
                handler.registerError('Cannot change the bugs of a Complete or Deprecated project.');
        }
        
        private void doValidateFixedBugMustHaveFixedDate(ValidationHandler handler)
        {
            if( ((ProjectBug__c)handler.getRecord()).Status__c == ProjectBugs.STATUS_FIXED && ((ProjectBug__c)handler.getRecord()).Fixed_Date__c == null )
                handler.registerError('Fixed_Date__c', 'A Fixed bug must have a fixed date.');
        }
        
        private void doValidateFixedBugMustHaveFixedVersion(ValidationHandler handler)
        {
            if( ((ProjectBug__c)handler.getRecord()).Status__c == ProjectBugs.STATUS_FIXED && ((ProjectBug__c)handler.getRecord()).Fixed_In_Version__c == null )
            	handler.registerError('Fixed_In_Version__c', 'A Fixed bug must have a fixed version.');
        }
        
        private void doValidateCantLogBugsAgainstDeprecatedProjects(ValidationHandler handler)
        {
            Project__c owner = getProject( ((ProjectBug__c)handler.getRecord()).Project__c );
            if(owner.Status__c == Projects.STATUS_DEPRECATED)
                handler.registerError('Cannot log bugs against a Deprecated project.');
        }
        
        private void doValidateCantLogBugsAgainstDeprecatedVersions(ValidationHandler handler)
        {
            ProjectVersion__c foundVersion = getFoundVersion( ((ProjectBug__c)handler.getRecord()).Found_Version__c );
            
            if(foundVersion.Status__c == ProjectVersions.STATUS_DEPRECATED)
                handler.registerError('Cannot log bugs against a Deprecated version.');
        }
        
        private void doValidateCantDeleteNonVoid(ValidationHandler handler)
        {
            Project__c owner = getProject( ((ProjectBug__c)handler.getRecord()).Project__c );
            if(owner.Status__c == Projects.STATUS_DEPRECATED)
            {
                if( ((ProjectBug__c)handler.getRecord()).Status__c != ProjectBugs.STATUS_VOID )
                	handler.registerError('Cannot delete a non-void bug.');
            }
            else
            {
                handler.registerError('Cannot delete a bug from an active project.');
            }
        }
        
        private List<ProjectBug__c> getBugs()
        {
            if(newBugs != null)
                return newBugs;
            
            if(oldBugs != null)
                return oldBugs;
            
            return new List<ProjectBug__c>();
        }
        
        private ProjectBug__c getOldRecord(Id recordId)
        {
            if(oldBugs == null)
            {
                oldBugs = ProjectSelector.newInstance().selectBugsById(new Map<Id, ProjectBug__c>(newBugs).keySet());
            }
            
            return new Map<Id, ProjectBug__c>(oldBugs).get(recordId);
        }
        
        private Project__c getProject(Id projectId)
        {
            if(ownerProjects == null)
                ownerProjects = new Map<Id, Project__c>(ProjectSelector.newInstance().selectProjectsById( getProjectIds() ));
            
            return ownerProjects.get(projectId);
        }
        
        private Set<Id> getProjectIds()
        {
            Set<Id> ids = new Set<Id>();
            for(ProjectBug__c bug : getBugs())
            {
                ids.add(bug.Project__c);
            }
            return ids;
        }
        
        private ProjectVersion__c getFoundVersion(Id versionId)
        {
            if(foundVersions == null)
                foundVersions = new Map<Id, ProjectVersion__c>(ProjectSelector.newInstance().selectVersionsById( getFoundVersionIds() ));
            
            return foundVersions.get(versionId);
        }
        
        private Set<Id> getFoundVersionIds()
        {
            Set<Id> ids = new Set<Id>();
            for(ProjectBug__c bug : getBugs())
            {
                ids.add(bug.Found_Version__c);
            }
            return ids;
        }
        
        private Boolean isInsert()
        {
            return ProjectContext.isInTrigger() && oldBugs == null && newBugs != null;
        }
        
        private Boolean isDelete()
        {
            return ProjectContext.isInTrigger() && oldBugs != null && newBugs == null;
        }
    }
    
    public class ValidationHandler implements ProjectContext.ValidationHandler
    {
        private ProjectBug__c record;
        
        public ValidationHandler(ProjectBug__c sObj)
        {
            record = sObj;
        }
        
        public Id getId()
        {
            return record.Id;
        }
        
        public SObject getRecord()
        {
            return record;
        }
        
        public Boolean isEqual(SObject otherRecord)
        {
            return false;
        }
        
        public void registerError(String errorMessage)
        {
            if(ProjectContext.isInTrigger())
            {
                record.addError(errorMessage);
            }
            else
            {
                throw new ProjectException(errorMessage);
            }
        }
        
        public void registerError(String fieldname, String errorMessage)
        {
            if(!ProjectContext.isInTrigger())
                throw new ProjectException(errorMessage);
            
            if(fieldName == 'Status__c')
            {
                record.Status__c.addError(errorMessage);
            }
            else if(fieldName == 'Fixed_In_Version__c')
            {
                record.Fixed_In_Version__c.addError(errorMessage);
            }
            else if(fieldName == 'Fixed_Date__c')
            {
                record.Fixed_Date__c.addError(errorMessage);
            }
            else if(fieldName == 'Project__c')
            {
                record.Project__c.addError(errorMessage);
            }
        }
    }
}