global class ProjectTriggerHandler extends sim_util.EventBus.Observable
{    
    private ProjectContext.Validator validator;
    private List<SObject> oldRecords;
    private List<SObject> newRecords;
        
    global ProjectTriggerHandler(List<SObject> oldRecords, List<SObject> newRecords, ProjectContext.Validator recordValidator)
    {
        this.oldRecords = oldRecords;
        this.newRecords = newRecords;
        this.validator = recordValidator;
        
        validator.withOldRecords(this.oldRecords)
				 .withNewRecords(this.newRecords);
        
        registerEvents(new List<sim_util.EventBus.Emitter>{
            new BeforeValidateEmitter(),
            new InsertEmitter(),
            new UpdateEmitter(),
            new DeleteEmitter()
        });
    }
    
    public ProjectContext.Validator getValidator()
    {
        return validator;
    }
    
    public void execute()
    {
        if ( ProjectContext.isServiceMode() )
			return;
        
        ProjectContext.registerInTrigger(true);
        
        Boolean isInsert = oldRecords == null && newRecords != null;
        Boolean isUpdate = oldRecords != null && newRecords != null;
        Boolean isDelete = oldRecords != null && newRecords == null;
        
        fireEvent('beforevalidate', new List<Object>{validator, isInsert, isUpdate, isDelete});
        validator.validate();
        
        if(isInsert)
        {
            fireEvent('insert', new List<Object>{newRecords});
        }
        else if(isUpdate)
        {
            fireEvent('update', new List<Object>{oldRecords, newRecords});
        }
        else if(isDelete)
        {
            fireEvent('delete', new List<Object>{oldRecords});
        }
        
        ProjectContext.registerInTrigger(false);
    }
    
    public ProjectTriggerHandler onInsert(sim_util.EventBus.Callback fn)
    {
        addEventListener('insert', fn);
        return this;
    }
    
    public ProjectTriggerHandler onUpdate(sim_util.EventBus.Callback fn)
    {
        addEventListener('update', fn);
        return this;
    }
    
    public ProjectTriggerHandler onDelete(sim_util.EventBus.Callback fn)
    {
        addEventListener('delete', fn);
        return this;
    }
    
    public ProjectTriggerHandler onBeforeValidate(sim_util.EventBus.Callback fn)
    {
        addEventListener('beforevalidate', fn);
        return this;
    }
    
    /**
     * @event beforevalidate
     * Fired before the trigger is about to start validation
	 * @param {ProjectContext.Validator} validator The validator the trigger handler is about to use to validate
	 * 											   the commit with.
	 * @param {Boolean} isInsert True if this is an insert commit
	 * @param {Boolean} isUpdate True if this is an update commit
	 * @param {Boolean} isDelete True if this is a delete commit
     */
    global class BeforeValidateEmitter extends sim_util.EventBus.Emitter
    {
        global override String getEventName() { return 'beforevalidate'; }
        global override List<String> getArgumentNames() { return new List<String>{'validator', 'isInsert', 'isUpdate', 'isDelete'}; }
    }
    
    /**
     * @event insert
     * Fired on insert commits, this is fired in the after trigger.
	 * @param {List<SObject>} newRecords The records being inserted into the database
     */
    global class InsertEmitter extends sim_util.EventBus.Emitter
    {
        global override String getEventName() { return 'insert'; }
        global override List<String> getArgumentNames() { return new List<String>{'newRecords'}; }
    }
    
    /**
     * @event update
     * Fired on update commits, this is fired in the after trigger.
	 * @param {List<SObject>} oldRecords The old copies of the records being updated
	 * @param {List<SObject>} newRecords The new copies of the records being inserted
     */
    global class UpdateEmitter extends sim_util.EventBus.Emitter
    {
        global override String getEventName() { return 'update'; }
        global override List<String> getArgumentNames() { return new List<String>{'oldRecords', 'newRecords'}; }
    }
    
    /**
     * @event delete
     * Fired on delete commits, this is fired in the after trigger.
	 * @param {List<SObject>} oldRecords The records being deleted from the database
     */
    global class DeleteEmitter extends sim_util.EventBus.Emitter
    {
        global override String getEventName() { return 'delete'; }
        global override List<String> getArgumentNames() { return new List<String>{'oldRecords'}; }
    }
}