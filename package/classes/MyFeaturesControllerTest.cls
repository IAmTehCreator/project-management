@isTest(SeeAllData=false)
private class MyFeaturesControllerTest
{
	private static TestMethod void constructor_getsFeaturesAndBugs()
    {
        ProjectTestScenario.Scenario scenario = newScenario();
        MyFeaturesController controller = new MyFeaturesController();
        
        System.assertEquals(1, controller.getFeatures().size(), 'Should load one feature');
        System.assertEquals(1, controller.getBugs().size(), 'Should load one bug');
    }
    
    private static TestMethod void getHasFeatures_userHasFeatures_returnsTrue()
    {
        ProjectTestScenario.Scenario scenario = newScenario();
        MyFeaturesController controller = new MyFeaturesController();
        
        System.assertEquals(true, controller.getHasFeatures(), 'Should return true when user has features');
    }
    
    private static TestMethod void getHasFeatures_userNotHaveFeatures_returnsFalse()
    {
        ProjectTestScenario.Scenario scenario = newScenario();
        scenario.Features.get(0).withUser(null).save();
        MyFeaturesController controller = new MyFeaturesController();
        
        System.assertEquals(false, controller.getHasFeatures(), 'Should return false when user does not have features');
    }
    
    private static TestMethod void getHasBugs_userHasBugs_returnsTrue()
    {
        ProjectTestScenario.Scenario scenario = newScenario();
        MyFeaturesController controller = new MyFeaturesController();
        
        System.assertEquals(true, controller.getHasBugs(), 'Should return true when user has bugs');
    }
    
    private static TestMethod void getHasBugs_userNotHaveBugs_returnsFalse()
    {
        ProjectTestScenario.Scenario scenario = newScenario();
        scenario.Bugs.get(0).withAssignedUser(null).save();
        MyFeaturesController controller = new MyFeaturesController();
        
        System.assertEquals(false, controller.getHasBugs(), 'Should return false when user has not bugs');
    }
    
    private static TestMethod void getFeatureHoursTotal_returnsTotalFeatureHours()
    {
        ProjectTestScenario.Scenario scenario = newScenario();
        MyFeaturesController controller = new MyFeaturesController();
        
        System.assertEquals(3, controller.getFeatureHoursTotal(), 'Should return correct total');
    }
    
    private static TestMethod void getBugHoursTotal_returnsTotalBugHours()
    {
        ProjectTestScenario.Scenario scenario = newScenario();
        MyFeaturesController controller = new MyFeaturesController();
        
        System.assertEquals(3, controller.getBugHoursTotal(), 'Should return correct total');
    }
    
    private static ProjectTestScenario.Scenario newScenario()
    {
        Id userId = UserInfo.getUserId();

        ProjectTestScenario.Scenario scenario = ProjectTestScenario.createBasicScenario();
        scenario.Bugs.get(0).withAssignedUser(userId);
        scenario.Features.get(0).withUser(userId);
        scenario.doUpdate();
        
        return scenario;
    }
}