public class ProjectMocks
{   
    public class NotificationService implements NotificationService.API
    {
        private sim_util.Mocks.Method mockSendNotifications = new sim_util.Mocks.Method('NotificationService', 'sendNotifications');
        private sim_util.Mocks.Method mockSendEmail = new sim_util.Mocks.Method('NotificationService', 'sendEmail');
        private sim_util.Mocks.Method mockSendEmails = new sim_util.Mocks.Method('NotificationService', 'sendEmails');
        private sim_util.Mocks.Method mockSendChatterPost = new sim_util.Mocks.Method('NotificationService', 'sendChatterPost');
        private sim_util.Mocks.Method mockSendChatterPosts = new sim_util.Mocks.Method('NotificationService', 'sendChatterPosts');
        
        public void assertCalls()
        {
            mockSendNotifications.doAssertsNow();
            mockSendEmail.doAssertsNow();
            mockSendEmails.doAssertsNow();
            mockSendChatterPost.doAssertsNow();
            mockSendChatterPosts.doAssertsNow();
        }
        
        public void sendNotifications(List<NotificationService.Notification> messages, Boolean chatter, Boolean email)
        {
            mockSendNotifications.call(new List<Object>{messages, chatter, email});
        }
        
        public sim_util.Mocks.Method whenSendNotifications()
        {
            return mockSendNotifications;
        }
        
        public void sendEmail(NotificationService.Notification message)
        {
            mockSendEmail.call(new List<Object>{message});
        }
        
        public sim_util.Mocks.Method whenSendEmail()
        {
            return mockSendEmail;
        }
        
        public void sendEmails(List<NotificationService.Notification> messages)
        {
            mockSendEmails.call(new List<Object>{messages});
        }
        
        public sim_util.Mocks.Method whenSendEmails()
        {
            return mockSendEmails;
        }
        
        public void sendChatterPost(NotificationService.Notification message)
        {
            mockSendChatterPost.call(new List<Object>{message});
        }
        
        public sim_util.Mocks.Method whenSendChatterPost()
        {
            return mockSendChatterPost;
        }
        
        public void sendChatterPosts(List<NotificationService.Notification> messages)
        {
            mockSendChatterPosts.call(new List<Object>{messages});
        }
        
        public sim_util.Mocks.Method whenSendChatterPosts()
        {
            return mockSendChatterPosts;
        }
    }
    
    public class Validator implements ProjectContext.Validator
    {
        private sim_util.Mocks.Method mockWithOldRecords = new sim_util.Mocks.Method('ProjectContext.Validator', 'withOldRecords');
        private sim_util.Mocks.Method mockWithNewRecords = new sim_util.Mocks.Method('ProjectContext.Validator', 'withNewRecords');
        private sim_util.Mocks.Method mockValidate = new sim_util.Mocks.Method('ProjectContext.Validator', 'validate');
        
        public void assertCalls()
        {
			mockWithOldRecords.doAssertsNow();
            mockWithNewRecords.doAssertsNow();
            mockValidate.doAssertsNow();
        }
        
        public Validator withOldRecords(List<SObject> oldRecords)
        {
            mockWithOldRecords.call(new List<Object>{oldRecords});
            return this;
        }
        
        public sim_util.Mocks.Method whenWithOldRecords()
        {
            return mockWithOldRecords;
        }
        
        public Validator withNewRecords(List<SObject> newRecords)
        {
            mockWithNewRecords.call(new List<Object>{newRecords});
            return this;
        }
        
        public sim_util.Mocks.Method whenWithNewRecords()
        {
            return mockWithNewRecords;
        }
        
        public void validate()
        {
            mockValidate.call();
        }
        
        public sim_util.Mocks.Method whenValidate()
        {
            return mockValidate;
        }
    }
    
    public class ValidationHandler implements ProjectContext.ValidationHandler
    {
        private sim_util.Mocks.Method mockGetId = new sim_util.Mocks.Method('ProjectContext.ValidationHandler', 'getId');
        private sim_util.Mocks.Method mockGetRecord = new sim_util.Mocks.Method('ProjectContext.ValidationHandler', 'getRecord');
        
        private sim_util.Mocks.Method mockIsEqual = new sim_util.Mocks.Method('ProjectContext.ValidationHandler', 'isEqual');
        private sim_util.Mocks.Method mockRegisterError = new sim_util.Mocks.Method('ProjectContext.ValidationHandler', 'registerError');
        private sim_util.Mocks.Method mockRegisterFieldError = new sim_util.Mocks.Method('ProjectContext.ValidationHandler', 'registerError');
        
        public void assertCalls()
        {
            mockGetId.doAssertsNow();
            mockGetRecord.doAssertsNow();
            mockIsEqual.doAssertsNow();
            mockRegisterError.doAssertsNow();
            mockRegisterFieldError.doAssertsNow();
        }
        
        public Id getId()
        {
            return (Id)mockGetId.call();
        }
        
        public sim_util.Mocks.Method whenGetId()
        {
            return mockGetId;
        }
        
        public SObject getRecord()
        {
            return (SObject)mockGetRecord.call();
        }
        
        public sim_util.Mocks.Method whenGetRecord()
        {
            return mockGetRecord;
        }
        
        public Boolean isEqual(SObject otherRecord)
        {
            return (Boolean)mockIsEqual.call(new List<Object>{otherRecord});
        }
        
        public sim_util.Mocks.Method whenIsEqual()
        {
            return mockIsEqual;
        }
        
        public void registerError(String errorMessage)
        {
            mockRegisterError.call(new List<Object>{errorMessage});
        }
        
        public sim_util.Mocks.Method whenRegisterError()
        {
            return mockRegisterError;
        }
        
        public void registerError(String fieldname, String errorMessage)
        {
            mockRegisterFieldError.call(new List<Object>{fieldname, errorMessage});
        }
        
        public sim_util.Mocks.Method whenRegisterFieldError()
        {
            return mockRegisterFieldError;
        }
    }
    
	public class ProjectSelector implements ProjectSelector.API
    {
        private sim_util.Mocks.Method mockSelectProjectById = new sim_util.Mocks.Method('ProjectSelector', 'selectProjectById');
        private sim_util.Mocks.Method mockSelectProjectsById = new sim_util.Mocks.Method('ProjectSelector', 'selectProjectsById');
        private sim_util.Mocks.Method mockSelectProjectsWhere = new sim_util.Mocks.Method('ProjectSelector', 'selectProjectsWhere');
        
        private sim_util.Mocks.Method mockSelectVersionsByProjectId = new sim_util.Mocks.Method('ProjectSelector', 'selectVersionsByProjectId');
        private sim_util.Mocks.Method mockSelectFeaturesByProjectId = new sim_util.Mocks.Method('ProjectSelector', 'selectFeaturesByProjectId');
        private sim_util.Mocks.Method mockSelectBugsByProjectId = new sim_util.Mocks.Method('ProjectSelector', 'selectBugsByProjectId');
        private sim_util.Mocks.Method mockSelectAssetsByProjectId = new sim_util.Mocks.Method('ProjectSelector', 'selectAssetsByProjectId');
        
        private sim_util.Mocks.Method mockSelectVersionById = new sim_util.Mocks.Method('ProjectSelector', 'selectVersionById');
        private sim_util.Mocks.Method mockSelectVersionsById = new sim_util.Mocks.Method('ProjectSelector', 'selectVersionsById');
        private sim_util.Mocks.Method mockSelectVersionsWhere = new sim_util.Mocks.Method('ProjectSelector', 'selectVersionsWhere');
        
        private sim_util.Mocks.Method mockSelectFeatureById = new sim_util.Mocks.Method('ProjectSelector', 'selectFeatureById');
        private sim_util.Mocks.Method mockSelectFeaturesById = new sim_util.Mocks.Method('ProjectSelector', 'selectFeaturesById');
        private sim_util.Mocks.Method mockSelectFeaturesWhere = new sim_util.Mocks.Method('ProjectSelector', 'selectFeaturesWhere');
        
        private sim_util.Mocks.Method mockSelectBugById = new sim_util.Mocks.Method('ProjectSelector', 'selectBugById');
        private sim_util.Mocks.Method mockSelectBugsById = new sim_util.Mocks.Method('ProjectSelector', 'selectBugsById');
        private sim_util.Mocks.Method mockSelectBugsWhere = new sim_util.Mocks.Method('ProjectSelector', 'selectBugsWhere');
        
        private sim_util.Mocks.Method mockSelectAssetById = new sim_util.Mocks.Method('ProjectSelector', 'selectAssetById');
        private sim_util.Mocks.Method mockSelectAssetsById = new sim_util.Mocks.Method('ProjectSelector', 'selectAssetsById');
        private sim_util.Mocks.Method mockSelectAssetsWhere = new sim_util.Mocks.Method('ProjectSelector', 'selectAssetsWhere');
        private sim_util.Mocks.Method mockSelectAssetsMappedByProjectId = new sim_util.Mocks.Method('ProjectSelector', 'selectAssetsMappedByProjectId');
        
        private sim_util.Mocks.Method mockSelectDependencyById = new sim_util.Mocks.Method('ProjectSelector', 'selectDependencyById');
        private sim_util.Mocks.Method mockSelectDependenciesById = new sim_util.Mocks.Method('ProjectSelector', 'selectDependenciesById');
        private sim_util.Mocks.Method mockSelectDependenciesWhere = new sim_util.Mocks.Method('ProjectSelector', 'selectDependenciesWhere');
        
        public void assertCalls()
        {
            mockSelectProjectById.doAssertsNow();
            mockSelectProjectsById.doAssertsNow();
            mockSelectProjectsWhere.doAssertsNow();
            mockSelectVersionsByProjectId.doAssertsNow();
            mockSelectFeaturesByProjectId.doAssertsNow();
            mockSelectVersionsWhere.doAssertsNow();
            mockSelectBugsByProjectId.doAssertsNow();
            mockSelectVersionById.doAssertsNow();
            mockSelectVersionsById.doAssertsNow();
            mockSelectFeatureById.doAssertsNow();
            mockSelectFeaturesById.doAssertsNow();
            mockSelectFeaturesWhere.doAssertsNow();
            mockSelectBugById.doAssertsNow();
            mockSelectBugsById.doAssertsNow();
            mockSelectBugsWhere.doAssertsNow();
            mockSelectDependencyById.doAssertsNow();
            mockSelectDependenciesById.doAssertsNow();
            mockSelectDependenciesWhere.doAssertsNow();
        }
        
        public Project__c selectProjectById(Id projectId)
        {
            return (Project__c)mockSelectProjectById.call(new List<Object>{projectId});
        }
        
        public sim_util.Mocks.Method whenSelectProjectById()
        {
            return mockSelectProjectById;
        }
        
		public List<Project__c> selectProjectsById(Set<Id> projectIds)
        {
            return (List<Project__c>)mockSelectProjectsById.call(new List<Object>{projectIds});
        }
        
        public sim_util.Mocks.Method whenSelectProjectsById()
        {
            return mockSelectProjectsById;
        }
        
        public List<Project__c> selectProjectsWhere(String whereClause)
        {
            return (List<Project__c>)mockSelectProjectsWhere.call(new List<Object>{whereClause});
        }
        
        public sim_util.Mocks.Method whenSelectProjectsWhere()
        {
            return mockSelectProjectsWhere;
        }
        
        public List<ProjectVersion__c> selectVersionsByProjectId(Id projectId)
        {
            return (List<ProjectVersion__c>)mockSelectVersionsByProjectId.call(new List<Object>{projectId});
        }
        
        public sim_util.Mocks.Method whenSelectVersionsByProjectId()
        {
            return mockSelectVersionsByProjectId;
        }
        
        public List<ProjectFeature__c> selectFeaturesByProjectId(Id projectId)
        {
            return (List<ProjectFeature__c>)mockSelectFeaturesByProjectId.call(new List<Object>{projectId});
        }
        
        public sim_util.Mocks.Method whenSelectFeaturesByProjectId()
        {
            return mockSelectFeaturesByProjectId;
        }
        
        public List<ProjectBug__c> selectBugsByProjectId(Id projectId)
        {
            return (List<ProjectBug__c>)mockSelectBugsByProjectId.call(new List<Object>{projectId});
        }
        
        public sim_util.Mocks.Method whenSelectBugsByProjectId()
        {
            return mockSelectBugsByProjectId;
        }
        
        public List<ProjectAsset__c> selectAssetsByProjectId(Id projectId)
        {
            return (List<ProjectAsset__c>)mockSelectAssetsByProjectId.call(new List<Object>{projectId});    
        }
        
        public sim_util.Mocks.Method whenSelectAssetsByProjectId()
        {
			return mockSelectAssetsByProjectId;
        }
        
        public ProjectVersion__c selectVersionById(Id versionId)
        {
            return (ProjectVersion__c)mockSelectVersionById.call(new List<Object>{versionId});
        }
        
        public sim_util.Mocks.Method whenSelectVersionById()
        {
            return mockSelectVersionById;
        }
        
        public List<ProjectVersion__c> selectVersionsById(Set<Id> versionIds)
        {
            return (List<ProjectVersion__c>)mockSelectVersionsById.call(new List<Object>{versionIds});
        }
        
        public sim_util.Mocks.Method whenSelectVersionsById()
        {
            return mockSelectVersionsById;
        }
        
        public List<ProjectVersion__c> selectVersionsWhere(String whereClause)
        {
            return (List<ProjectVersion__c>)mockSelectVersionsWhere.call(new List<Object>{whereClause});
        }
        
        public sim_util.Mocks.Method whenSelectVersionsWhere()
        {
            return mockSelectVersionsWhere;
        }
        
        public ProjectFeature__c selectFeatureById(Id featureId)
        {
            return (ProjectFeature__c)mockSelectFeatureById.call(new List<Object>{featureId});
        }
        
        public sim_util.Mocks.Method whenSelectFeatureById()
        {
            return mockSelectFeatureById;
        }
        
        public List<ProjectFeature__c> selectFeaturesById(Set<Id> featureIds)
        {
            return (List<ProjectFeature__c>)mockSelectFeaturesById.call(new List<Object>{featureIds});
        }
        
        public sim_util.Mocks.Method whenSelectFeaturesById()
        {
            return mockSelectFeaturesById;
        }
        
        public List<ProjectFeature__c> selectFeaturesWhere(String whereClause)
        {
            return (List<ProjectFeature__c>)mockSelectFeaturesWhere.call(new List<Object>{whereClause});
        }
        
        public sim_util.Mocks.Method whenSelectFeaturesWhere()
        {
            return mockSelectFeaturesWhere;
        }
        
        public ProjectBug__c selectBugById(Id bugId)
        {
            return (ProjectBug__c)mockSelectBugById.call(new List<Object>{bugId});
        }
        
        public sim_util.Mocks.Method whenSelectBugById()
        {
            return mockSelectBugById;
        }
        
        public List<ProjectBug__c> selectBugsById(Set<Id> bugIds)
        {
            return (List<ProjectBug__c>)mockSelectBugsById.call(new List<Object>{bugIds});
        }
        
        public sim_util.Mocks.Method whenSelectBugsById()
        {
            return mockSelectBugsById;
        }
        
        public List<ProjectBug__c> selectBugsWhere(String whereClause)
        {
            return (List<ProjectBug__c>)mockSelectBugsWhere.call(new List<Object>{whereClause});
        }
        
        public sim_util.Mocks.Method whenSelectBugsWhere()
        {
            return mockSelectBugsWhere;
        }
        
        public ProjectAsset__c selectAssetById(Id assetId)
        {
            return (ProjectAsset__c)mockSelectAssetById.call(new List<Object>{assetId});
        }
        
        public sim_util.Mocks.Method whenSelectAssetById()
        {
            return mockSelectAssetById;
        }
        
        public List<ProjectAsset__c> selectAssetsById(Set<Id> assetIds)
        {
            return (List<ProjectAsset__c>)mockSelectAssetsById.call(new List<Object>{assetIds});
        }
        
        public sim_util.Mocks.Method whenSelectAssetsById()
        {
            return mockSelectAssetsById;
        }
        
        public List<ProjectAsset__c> selectAssetsWhere(String whereClause)
        {
            return (List<ProjectAsset__c>)mockSelectAssetsWhere.call(new List<Object>{whereClause});
        }
        
        public sim_util.Mocks.Method whenSelectAssetsWhere()
        {
            return mockSelectAssetsWhere;
        }
        
        public Map<Id, List<ProjectAsset__c>> selectAssetsMappedByProjectId(Set<Id> projectIds)
        {
            return (Map<Id, List<ProjectAsset__c>>)mockSelectAssetsMappedByProjectId.call(new List<Object>{projectIds});
        }
        
        public sim_util.Mocks.Method whenSelectAssetsMappedByProjectId()
        {
			return mockSelectAssetsMappedByProjectId;
        }
        
        public ProjectDependency__c selectDependencyById(Id assetId)
        {
            return (ProjectDependency__c)mockSelectDependencyById.call(new List<Object>{assetId});
        }
        
        public sim_util.Mocks.Method whenSelectDependencyById()
        {
            return mockSelectDependencyById;
        }
        
        public List<ProjectDependency__c> selectDependenciesById(Set<Id> assetIds)
        {
            return (List<ProjectDependency__c>)mockSelectDependenciesById.call(new List<Object>{assetIds});
        }
        
        public sim_util.Mocks.Method whenSelectDependenciesById()
        {
            return mockSelectDependenciesById;
        }
        
        public List<ProjectDependency__c> selectDependenciesWhere(String whereClause)
        {
            return (List<ProjectDependency__c>)mockSelectDependenciesWhere.call(new List<Object>{whereClause});
        }
        
        public sim_util.Mocks.Method whenSelectDependenciesWhere()
        {
            return mockSelectDependenciesWhere;
        }
    }
}