global class ProjectContext
{
	private static Boolean platformRequest = true;
    private static Integer triggerCount = 0;
    private static Map<Id, ValidationHandler> serviceObjects = new Map<Id, ValidationHandler>();
    
    public static Boolean isPlatformRequest()
    {
		return platformRequest;
    }
    
    public static Boolean isInTrigger()
    {
		return triggerCount > 0;
    }
    
    public static Boolean isServiceMode()
    {
        return ProjectManagementSettings__c.getInstance().ServiceMode__c;
    }
    
    /**
     * Registers this transaction being invoked by a VisualForce page or Service request
     */
    public static void registerServiceTransaction()
    {
        platformRequest = false;
    }
    
    public static void registerInTrigger(Boolean isInTrigger)
    {
        Integer count = isInTrigger ? 1 : -1;
        
        triggerCount += count;
        triggerCount = triggerCount < 0 ? 0 : triggerCount;
    }
    
    public static void registerPreValidated(ValidationHandler handler)
    {
        serviceObjects.put(handler.getId(), handler);
    }
    
    public static Boolean isRecordPreValidated(SObject record)
    {
        ValidationHandler recordHandler = serviceObjects.get(record.Id);
        return recordHandler != null ? recordHandler.isEqual(record) : false;
    }
    
    global interface Validator
    {
        Validator withOldRecords(List<SObject> oldRecords);
        Validator withNewRecords(List<SObject> newRecords);
        void validate();
    }
    
    public interface ValidationHandler
    {
        Id getId();
        SObject getRecord();
        
        Boolean isEqual(SObject otherRecord);
        void registerError(String errorMessage);
        void registerError(String fieldname, String errorMessage);
    }
}