@isTest(SeeAllData=false)
private class RaiseBugControllerTest
{

    private static TestMethod void shouldCreateANewBug()
    {
        Case testCase = new Case(Origin = 'Web');
        insert testCase;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(testCase);
        RaiseBugController controller = new RaiseBugController(stdController);
        
        System.assertEquals(testCase, controller.RelatedCase, 'Should store the related case on the controller');
        System.assert(controller.Valid, 'Should mark the operation as valid');
        System.assert(controller.Bug != null, 'Should create a new bug');
    }
    
    private static TestMethod void shouldAddPageMessageWhenCaseAlreadyAssociatedWithABug()
    {        
        Project__c project = new Project__c(Name = 'TestProject');
        insert project;
        
        ProjectVersion__c version = new ProjectVersion__c(Name = '1.00', Project__c = project.Id);
        insert version;
        
        project.Current_Version__c = version.Id;
        update project;
        
        ProjectBug__c bug = new ProjectBug__c(Project__c = project.Id, Found_Version__c = version.Id);
        insert bug;
        
        Case testCase = new Case(Origin = 'web', Bug__c = bug.Id);
        insert testCase;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(testCase);
        RaiseBugController controller = new RaiseBugController(stdController);
        
        System.assertEquals(testCase, controller.RelatedCase, 'Should store the related case on the controller');
        System.assert(!controller.Valid, 'Should mark the operation as invalid');
        System.assertEquals(controller.Bug, null, 'Should not create a new bug');
        
        System.assert(ApexPages.hasMessages(), 'Should add a page message');
        System.assert(ApexPages.getMessages()[0].getSummary().contains('This case already has a bug attached.'), 'Should add the appropriate message');
    }
    
    private static TestMethod void raiseShouldInsertBugAndSetRelatedCase()
    {   
        Project__c project = new Project__c(Name = 'TestProject');
        insert project;
        
        ProjectVersion__c version = new ProjectVersion__c(Name = '1.00', Project__c = project.Id);
        insert version;
        
        project.Current_Version__c = version.Id;
        update project;
        
        Case testCase = new Case(Origin = 'web');
        insert testCase;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(testCase);
        RaiseBugController controller = new RaiseBugController(stdController);
        
        controller.Bug.Project__c = project.Id;
        controller.Bug.Found_Version__c = version.Id;
        
        PageReference ref = controller.raise();

		System.assert(controller.Bug.Id != null, 'Should insert the bug');
        System.assert(ref.getUrl().contains(controller.Bug.Id), 'Should redirect to bug detail page');
        
        ProjectBug__c bug = [SELECT Id, Name, Project__c, Found_Version__c FROM ProjectBug__c WHERE Id = :controller.Bug.Id];
        System.assertEquals(project.Id, bug.Project__c, 'Should commit the Project ID');
        System.assertEquals(version.Id, bug.Found_Version__c, 'Should commit the found version ID');
    }
    
    private static TestMethod void raiseShouldAddPageMessageOnException()
    {
        Project__c project = new Project__c(Name = 'TestProject');
        insert project;
        
        ProjectVersion__c version = new ProjectVersion__c(Name = '1.00', Project__c = project.Id);
        insert version;
        
        project.Current_Version__c = version.Id;
        update project;
        
        Case testCase = new Case(Origin = 'web');
        insert testCase;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(testCase);
        RaiseBugController controller = new RaiseBugController(stdController);
        
        controller.Bug.Project__c = project.Id;
        controller.Bug.Found_Version__c = version.Id;
        
        failTrigger();
        PageReference ref = controller.raise();
        
        System.assertEquals(null, ref, 'Should not redirect');
        System.assert(ApexPages.hasMessages(), 'Should add a page message');
        System.assert(ApexPages.getMessages()[0].getSummary().contains('TestError'), 'Should show the exception message');
    }
    
    private static void failTrigger()
    {
        ProjectMocks.Validator validator = new ProjectMocks.Validator();
        validator.whenWithNewRecords().thenDo( new WithNewRecordsCallback() );
        ProjectBugs.setValidatorImplementation(validator);
    }
    
    private class WithNewRecordsCallback implements sim_util.Mocks.MethodListener
    {
        public Object execute(List<Object> args)
        {
            List<ProjectBug__c> bugs = (List<ProjectBug__c>)args.get(0);
            ProjectBug__c bug = bugs.get(0);
            bug.addError('TestError');
            return null;
        }
    }
    
}