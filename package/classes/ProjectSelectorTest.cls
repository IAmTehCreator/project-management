@isTest(SeeAllData=false)
private class ProjectSelectorTest
{
	
    private static TestMethod void selectorShouldReturnNewInstance()
    {
        System.assert(ProjectSelector.newInstance() instanceof ProjectSelector.ServiceImplementation, 'Should return a new service implementation');
    }
    
    private static TestMethod void selectorShouldReturnMock()
    {
		ProjectMocks.ProjectSelector mock = new ProjectMocks.ProjectSelector();
        ProjectSelector.setImplementation(mock);
        
        System.assert(ProjectSelector.newInstance() === mock, 'Should return the mock implementation');
    }
    
    private static TestMethod void selectProjectById_shouldReturnNull_whenNullPassedIn()
    {
        System.assertEquals(null, ProjectSelector.newInstance().selectProjectById(null), 'Should have returned null');
        System.assertEquals(0, Limits.getQueries(), 'Should not have queried the database');
    }
    
    private static TestMethod void selectProjectById_shouldReturnProject()
    {
		Project__c project = ProjectTestScenario.createProject()
            									.withName('Test Project')
            									.save();
        
		Project__c result = ProjectSelector.newInstance().selectProjectById(project.Id);
        
        System.assertEquals(project.Id, result.Id, 'Should have returned the correct record');
        System.assertEquals(project.Name, result.Name, 'Should have returned an SObject with populated fields');
    }
    
    private static TestMethod void selectProjectsById_shouldReturnEmptyList_whenNullPassedIn()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        
        System.assertEquals(new List<Project__c>(), ProjectSelector.newInstance().selectProjectsById(null), 'Should have returned null');
        System.assertEquals(0, Limits.getQueries(), 'Should not have queried the database');
    }
    
    private static TestMethod void selectProjectsById_shouldReturnEmptyList_whenEmptySetPassedIn()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        
        System.assertEquals(new List<Project__c>(), ProjectSelector.newInstance().selectProjectsById(new Set<Id>()), 'Should have returned null');
        System.assertEquals(0, Limits.getQueries(), 'Should not have queried the database');
    }
    
    private static TestMethod void selectProjectsById_ReturnsProjects()
    {
        Project__c project1 = ProjectTestScenario.createProject().save();
        Project__c project2 = ProjectTestScenario.createProject().save();
        Set<Id> ids = new Set<Id>{project1.Id, project2.Id};
            
        List<Project__c> result = ProjectSelector.newInstance().selectProjectsById(ids);
        
        System.assertEquals(2, result.size(), 'Should have returned two projects');
    }
    
    private static TestMethod void selectProjectsWhere_returnsEmptyList_whenNullPassedIn()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        
        Integer queries = Limits.getQueries();
        List<Project__c> result = ProjectSelector.newInstance().selectProjectsWhere(null);
        assertNoRecords(result, queries);
    }
    
    private static TestMethod void selectProjectsWhere_returnsEmptyList_whenEmptyStringPassedIn()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        
        Integer queries = Limits.getQueries();
        List<Project__c> result = ProjectSelector.newInstance().selectProjectsWhere('');
        assertNoRecords(result, queries);
    }
    
    private static TestMethod void selectProjectsWhere_returnsProjects()
    {
        Project__c project1 = ProjectTestScenario.createProject().withName('Test1').save();
        Project__c project2 = ProjectTestScenario.createProject().withName('Test2').save();
        Project__c project3 = ProjectTestScenario.createProject().withName('Test3').save();
        
        List<Project__c> result = ProjectSelector.newInstance().selectProjectsWhere('Name = \'Test2\'');
        assertGotRecord(project2, result);
    }
    
    private static TestMethod void selectVersionsByProjectId_nullId_returnsEmptyList()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        ProjectVersion__c version = ProjectTestScenario.createVersion()
            										   .withProject(project.Id)
            										   .save();
        
        Integer queries = Limits.getQueries();
        List<ProjectVersion__c> result = ProjectSelector.newInstance().selectVersionsByProjectId(null);
        assertNoRecords(result, queries);
    }
    
    private static TestMethod void selectVersionsByProjectId_returnsVersion()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        ProjectVersion__c version = ProjectTestScenario.createVersion()
            										   .withProject(project.Id)
            										   .save();
        
        List<ProjectVersion__c> result = ProjectSelector.newInstance().selectVersionsByProjectId(project.Id);
        assertGotRecord(version, result);
    }
    
    private static TestMethod void selectFeaturesByProjectId_nullID_returnsEmptyList()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        ProjectFeature__c feature = ProjectTestScenario.createFeature()
            										   .withProject(project.Id)
            										   .save();
        
        Integer queries = Limits.getQueries();
        List<ProjectFeature__c> result = ProjectSelector.newInstance().selectFeaturesByProjectId(null);
        assertNoRecords(result, queries);
    }
    
    private static TestMethod void selectFeaturesByProjectId_returnsFeature()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        ProjectFeature__c feature = ProjectTestScenario.createFeature()
            										   .withProject(project.Id)
            										   .save();
        
        List<ProjectFeature__c> result = ProjectSelector.newInstance().selectFeaturesByProjectId(project.Id);
        assertGotRecord(feature, result);
    }
    
    private static TestMethod void selectBugsByProjectId_nullId_returnsEmptyList()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        ProjectBug__c bug = ProjectTestScenario.createBug()
            								   .withProject(project.Id)
            								   .save();
        
        Integer queries = Limits.getQueries();
        List<ProjectBug__c> result = ProjectSelector.newInstance().selectBugsByProjectId(null);
        assertNoRecords(result, queries);
    }
    
    private static TestMethod void selectBugsByProjectId_returnsBug()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        ProjectBug__c bug = ProjectTestScenario.createBug()
            								   .withProject(project.Id)
            								   .save();
        
        List<ProjectBug__c> result = ProjectSelector.newInstance().selectBugsByProjectId(project.Id);
        assertGotRecord(bug, result);
    }
    
    private static TestMethod void selectAssetsByProjectId_nullId_returnsEmptyList()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        ProjectAsset__c asset = ProjectTestScenario.createAsset()
            									   .withProject(project.Id)
            									   .save();
        
        Integer queries = Limits.getQueries();
        List<ProjectAsset__c> result = ProjectSelector.newInstance().selectAssetsByProjectId(null);
        assertNoRecords(result, queries);
    }
    
    private static TestMethod void selectAssetsByProjectId_returnsAsset()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        ProjectAsset__c asset = ProjectTestScenario.createAsset()
            									   .withProject(project.Id)
            									   .save();
        
        List<ProjectAsset__c> result = ProjectSelector.newInstance().selectAssetsByProjectId(project.Id);
        assertGotRecord(asset, result);
    }
    
    private static TestMethod void selectVersionById_nullId_returnsNull()
    {
        ProjectVersion__c version = ProjectTestScenario.createVersion().save();
        ProjectVersion__c version2 = ProjectTestScenario.createVersion().save();
        
        Integer queries = Limits.getQueries();
        ProjectVersion__c result = ProjectSelector.newInstance().selectVersionById(null);
        assertNoRecords(result, queries);
    }
    
    private static TestMethod void selectVersionById_returnsVersion()
    {
        ProjectVersion__c version = ProjectTestScenario.createVersion().withVersionNumber('Test').save();
        ProjectVersion__c version2 = ProjectTestScenario.createVersion().withVersionNumber('Test2').save();
        
        ProjectVersion__c result = ProjectSelector.newInstance().selectVersionById(version.Id);
        assertGotRecord(version, result);
    }
    
    private static TestMethod void selectVersionsById_nullId_returnsNull()
    {
        ProjectVersion__c version = ProjectTestScenario.createVersion().save();
        ProjectVersion__c version2 = ProjectTestScenario.createVersion().save();
        
        Integer queries = Limits.getQueries();
        List<ProjectVersion__c> result = ProjectSelector.newInstance().selectVersionsById(null);
        assertNoRecords(result, queries);
    }
    
    private static TestMethod void selectVersionsById_returnsVersion()
    {
        ProjectVersion__c version = ProjectTestScenario.createVersion().withVersionNumber('Test').save();
        ProjectVersion__c version2 = ProjectTestScenario.createVersion().withVersionNumber('Test2').save();
        
        List<ProjectVersion__c> result = ProjectSelector.newInstance().selectVersionsById(new Set<Id>{version.Id});
        assertGotRecord(version, result);
    }
    
    private static TestMethod void selectVersionsWhere_nullWhereClause_returnsEmptyList()
    {
        ProjectVersion__c version = ProjectTestScenario.createVersion().withVersionNumber('Test').save();
        ProjectVersion__c version2 = ProjectTestScenario.createVersion().withVersionNumber('Test2').save();
        
        Integer queries = Limits.getQueries();
        List<ProjectVersion__c> result = ProjectSelector.newInstance().selectVersionsWhere(null);
        assertNoRecords(result, queries);
    }
    
    private static TestMethod void selectVersionsWhere_emptyStringWhereClause_returnsEmptyList()
    {
        ProjectVersion__c version = ProjectTestScenario.createVersion().withVersionNumber('Test').save();
        ProjectVersion__c version2 = ProjectTestScenario.createVersion().withVersionNumber('Test2').save();
        
        Integer queries = Limits.getQueries();
        List<ProjectVersion__c> result = ProjectSelector.newInstance().selectVersionsWhere('');
        assertNoRecords(result, queries);
    }
    
    private static TestMethod void selectVersionsWhere_returnsVersions()
    {
        ProjectVersion__c version = ProjectTestScenario.createVersion().withVersionNumber('Test').save();
        ProjectVersion__c version2 = ProjectTestScenario.createVersion().withVersionNumber('Test2').save();
        
        List<ProjectVersion__c> result = ProjectSelector.newInstance().selectVersionsWhere('Name = \'Test\'');
        assertGotRecord(version, result);
    }
    
    private static TestMethod void selectFeatureById_nullId_returnsEmptyList()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature().withName('Test').save();
        ProjectFeature__c feature2 = ProjectTestScenario.createFeature().withName('Test2').save();
        
        Integer queries = Limits.getQueries();
        ProjectFeature__c result = ProjectSelector.newInstance().selectFeatureById(null);
        assertNoRecords(result, queries);
    }
    
    private static TestMethod void selectFeatureById_returnsFeature()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature().withName('Test').save();
        ProjectFeature__c feature2 = ProjectTestScenario.createFeature().withName('Test2').save();
        
        ProjectFeature__c result = ProjectSelector.newInstance().selectFeatureById(feature.Id);
        assertGotRecord(feature, result);
    }
    
    private static TestMethod void selectFeaturesById_nullIds_returnsEmptyList()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature().withName('Test').save();
        ProjectFeature__c feature2 = ProjectTestScenario.createFeature().withName('Test2').save();
        
        Integer queries = Limits.getQueries();
        List<ProjectFeature__c> results = ProjectSelector.newInstance().selectFeaturesById(null);
        assertNoRecords(results, queries);
    }
    
    private static TestMethod void selectFeaturesById_emptySet_returnsEmptyList()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature().withName('Test').save();
        ProjectFeature__c feature2 = ProjectTestScenario.createFeature().withName('Test2').save();
        
        Integer queries = Limits.getQueries();
        List<ProjectFeature__c> results = ProjectSelector.newInstance().selectFeaturesById(new Set<Id>());
        assertNoRecords(results, queries);
    }
    
    private static TestMethod void selectFeaturesById_returnsFeature()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature().withName('Test').save();
        ProjectFeature__c feature2 = ProjectTestScenario.createFeature().withName('Test2').save();
        
        List<ProjectFeature__c> results = ProjectSelector.newInstance().selectFeaturesById(new Set<Id>{feature.Id});
        assertGotRecord(feature, results);
    }
    
    private static TestMethod void selectFeaturesWhere_nullWhereClause_returnsEmptyList()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature().withName('Test').save();
        ProjectFeature__c feature2 = ProjectTestScenario.createFeature().withName('Test2').save();
        
        Integer queries = Limits.getQueries();
        List<ProjectFeature__c> results = ProjectSelector.newInstance().selectFeaturesWhere(null);
        assertNoRecords(results, queries);
    }
    
    private static TestMethod void selectFeaturesWhere_emptyStringWhereClause_returnsEmptyList()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature().withName('Test').save();
        ProjectFeature__c feature2 = ProjectTestScenario.createFeature().withName('Test2').save();
        
        Integer queries = Limits.getQueries();
        List<ProjectFeature__c> results = ProjectSelector.newInstance().selectFeaturesWhere('');
        assertNoRecords(results, queries);
    }
    
    private static TestMethod void selectFeaturesWhere_returnsFeature()
    {
        ProjectFeature__c feature = ProjectTestScenario.createFeature().withName('Test').save();
        ProjectFeature__c feature2 = ProjectTestScenario.createFeature().withName('Test2').save();
        
        List<ProjectFeature__c> results = ProjectSelector.newInstance().selectFeaturesWhere('Name__c = \'Test\'');
        assertGotRecord(feature, results);
    }
    
    private static TestMethod void selectBugById_nullId_returnsEmptyList()
    {
        ProjectBug__c bug = ProjectTestScenario.createBug().withEstimatedHours(1).save();
        ProjectBug__c bug2 = ProjectTestScenario.createBug().withEstimatedHours(2).save();
        
        Integer queries = Limits.getQueries();
        ProjectBug__c result = ProjectSelector.newInstance().selectBugById(null);
        assertNoRecords(result, queries);
    }

    private static TestMethod void selectBugById_returnsBug()
    {
        ProjectBug__c bug = ProjectTestScenario.createBug().withEstimatedHours(1).save();
        ProjectBug__c bug2 = ProjectTestScenario.createBug().withEstimatedHours(2).save();
        
        ProjectBug__c result = ProjectSelector.newInstance().selectBugById(bug.Id);
        assertGotRecord(bug, result);
    }
    
    private static TestMethod void selectBugsById_nullIds_returnsEmptyList()
    {
        ProjectBug__c bug = ProjectTestScenario.createBug().withEstimatedHours(1).save();
        ProjectBug__c bug2 = ProjectTestScenario.createBug().withEstimatedHours(2).save();
        
        Integer queries = Limits.getQueries();
        List<ProjectBug__c> results = ProjectSelector.newInstance().selectBugsById(null);
        assertNoRecords(results, queries);
    }

    private static TestMethod void selectBugsById_emptySet_returnsEmptyList()
    {
        ProjectBug__c bug = ProjectTestScenario.createBug().withEstimatedHours(1).save();
        ProjectBug__c bug2 = ProjectTestScenario.createBug().withEstimatedHours(2).save();
        
        Integer queries = Limits.getQueries();
        List<ProjectBug__c> results = ProjectSelector.newInstance().selectBugsById(new Set<Id>());
        assertNoRecords(results, queries);
    }

    private static TestMethod void selectBugsById_returnsBug()
    {
        ProjectBug__c bug = ProjectTestScenario.createBug().withEstimatedHours(1).save();
        ProjectBug__c bug2 = ProjectTestScenario.createBug().withEstimatedHours(2).save();
        
        List<ProjectBug__c> results = ProjectSelector.newInstance().selectBugsById(new Set<Id>{bug.Id});
        assertGotRecord(bug, results);
    }

    private static TestMethod void selectBugsWhere_nullWhereClause_returnsEmptyList()
    {
        ProjectBug__c bug = ProjectTestScenario.createBug().withEstimatedHours(1).save();
        ProjectBug__c bug2 = ProjectTestScenario.createBug().withEstimatedHours(2).save();
        
        Integer queries = Limits.getQueries();
        List<ProjectBug__c> results = ProjectSelector.newInstance().selectBugsWhere(null);
        assertNoRecords(results, queries);
    }

    private static TestMethod void selectBugsWhere_emptyStringWhereClause_returnsEmptyList()
    {
        ProjectBug__c bug = ProjectTestScenario.createBug().withEstimatedHours(1).save();
        ProjectBug__c bug2 = ProjectTestScenario.createBug().withEstimatedHours(2).save();
        
        Integer queries = Limits.getQueries();
        List<ProjectBug__c> results = ProjectSelector.newInstance().selectBugsWhere('');
        assertNoRecords(results, queries);
    }

    private static TestMethod void selectBugsWhere_returnsBug()
    {
        ProjectBug__c bug = ProjectTestScenario.createBug().withEstimatedHours(1).save();
        ProjectBug__c bug2 = ProjectTestScenario.createBug().withEstimatedHours(2).save();
        
        List<ProjectBug__c> results = ProjectSelector.newInstance().selectBugsWhere('Estimated_Hours__c = 1');
        assertGotRecord(bug, results);
    }
    
    private static TestMethod void selectAssetById_nullId_returnsEmptyList()
    {
        ProjectAsset__c asset = ProjectTestScenario.createAsset().withQuantity(1).save();
        ProjectAsset__c asset2 = ProjectTestScenario.createAsset().withQuantity(2).save();
        
        Integer queries = Limits.getQueries();
        ProjectAsset__c result = ProjectSelector.newInstance().selectAssetById(null);
        assertNoRecords(result, queries);
    }

    private static TestMethod void selectAssetById_returnsAsset()
    {
        ProjectAsset__c asset = ProjectTestScenario.createAsset().withQuantity(1).save();
        ProjectAsset__c asset2 = ProjectTestScenario.createAsset().withQuantity(2).save();
        
        ProjectAsset__c result = ProjectSelector.newInstance().selectAssetById(asset.Id);
        assertGotRecord(asset, result);
    }
    
    private static TestMethod void selectAssetsById_nullIds_returnsEmptyList()
    {
        ProjectAsset__c asset = ProjectTestScenario.createAsset().withQuantity(1).save();
        ProjectAsset__c asset2 = ProjectTestScenario.createAsset().withQuantity(2).save();
        
        Integer queries = Limits.getQueries();
        List<ProjectAsset__c> result = ProjectSelector.newInstance().selectAssetsById(null);
        assertNoRecords(result, queries);
    }

    private static TestMethod void selectAssetsById_emptySet_returnsEmptyList()
    {
        ProjectAsset__c asset = ProjectTestScenario.createAsset().withQuantity(1).save();
        ProjectAsset__c asset2 = ProjectTestScenario.createAsset().withQuantity(2).save();
        
        Integer queries = Limits.getQueries();
        List<ProjectAsset__c> result = ProjectSelector.newInstance().selectAssetsById(new Set<Id>());
        assertNoRecords(result, queries);
    }

    private static TestMethod void selectAssetsById_returnsAsset()
    {
        ProjectAsset__c asset = ProjectTestScenario.createAsset().withQuantity(1).save();
        ProjectAsset__c asset2 = ProjectTestScenario.createAsset().withQuantity(2).save();
        
        Integer queries = Limits.getQueries();
        List<ProjectAsset__c> result = ProjectSelector.newInstance().selectAssetsById(new Set<Id>{asset.Id});
        assertGotRecord(asset, result);
    }

    private static TestMethod void selectAssetsWhere_nullWhereClause_returnsEmptyList()
    {
        ProjectAsset__c asset = ProjectTestScenario.createAsset().withQuantity(1).save();
        ProjectAsset__c asset2 = ProjectTestScenario.createAsset().withQuantity(2).save();
        
        Integer queries = Limits.getQueries();
        List<ProjectAsset__c> result = ProjectSelector.newInstance().selectAssetsWhere(null);
        assertNoRecords(result, queries);
    }

    private static TestMethod void selectAssetsWhere_emptyStringWhereClause_returnsEmptyList()
    {
        ProjectAsset__c asset = ProjectTestScenario.createAsset().withQuantity(1).save();
        ProjectAsset__c asset2 = ProjectTestScenario.createAsset().withQuantity(2).save();
        
        Integer queries = Limits.getQueries();
        List<ProjectAsset__c> result = ProjectSelector.newInstance().selectAssetsWhere('');
        assertNoRecords(result, queries);
    }

    private static TestMethod void selectAssetsWhere_returnsAsset()
    {
        ProjectAsset__c asset = ProjectTestScenario.createAsset().withQuantity(1).save();
        ProjectAsset__c asset2 = ProjectTestScenario.createAsset().withQuantity(2).save();
        
        List<ProjectAsset__c> result = ProjectSelector.newInstance().selectAssetsWhere('Quantity__c = 1');
        assertGotRecord(asset, result);
    }

    private static TestMethod void selectAssetsMappedByProjectId_nullIds_returnsEmptyMap()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        Project__c project2 = ProjectTestScenario.createProject().save();
        ProjectAsset__c asset = ProjectTestScenario.createAsset().withProject(project.Id).save();
        ProjectAsset__c asset2 = ProjectTestScenario.createAsset().withProject(project.Id).save();
        ProjectAsset__c asset3 = ProjectTestScenario.createAsset().withProject(project2.Id).save();
        
        Integer queries = Limits.getQueries();
        Map<Id, List<ProjectAsset__c>> results = ProjectSelector.newInstance().selectAssetsMappedByProjectId(null);
        
        System.assertEquals(0, results.keySet().size(), 'Expected an empty map');
        System.assertEquals(queries, Limits.getQueries(), 'Expected no database queries');
    }

    private static TestMethod void selectAssetsMappedByProjectId_emptySet_returnsEmptyMap()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        Project__c project2 = ProjectTestScenario.createProject().save();
        ProjectAsset__c asset = ProjectTestScenario.createAsset().withProject(project.Id).save();
        ProjectAsset__c asset2 = ProjectTestScenario.createAsset().withProject(project.Id).save();
        ProjectAsset__c asset3 = ProjectTestScenario.createAsset().withProject(project2.Id).save();
        
        Integer queries = Limits.getQueries();
        Map<Id, List<ProjectAsset__c>> results = ProjectSelector.newInstance().selectAssetsMappedByProjectId(new Set<Id>());
        
        System.assertEquals(0, results.keySet().size(), 'Expected an empty map');
        System.assertEquals(queries, Limits.getQueries(), 'Expected no database queries');
    }

    private static TestMethod void selectAssetsMappedByProjectId_returnsMap()
    {
        Project__c project = ProjectTestScenario.createProject().save();
        Project__c project2 = ProjectTestScenario.createProject().save();
        ProjectAsset__c asset = ProjectTestScenario.createAsset().withProject(project.Id).save();
        ProjectAsset__c asset2 = ProjectTestScenario.createAsset().withProject(project.Id).save();
        ProjectAsset__c asset3 = ProjectTestScenario.createAsset().withProject(project2.Id).save();
        
        Map<Id, List<ProjectAsset__c>> results = ProjectSelector.newInstance().selectAssetsMappedByProjectId(new Set<Id>{project.Id});
        
        System.assertEquals(1, results.keySet().size(), 'Expected map to contain 2 project entries');
        
        List<ProjectAsset__c> assets = results.get(project.Id);
        System.assertEquals(2, assets.size(), 'Expected 2 assets mapped to the project');
        
        Boolean found1 = false;
        Boolean found2 = false;
        for(ProjectAsset__c record : assets)
        {
            if (record.Id == asset.Id)
            {
                found1 = true;
            }
            else if (record.Id == asset2.Id)
            {
                found2 = true;
            }
            else{
                System.assert(false, 'Unexpected record returned');
            }
        }
        
        System.assert(found1, 'Expected record was not returned');
        System.assert(found2, 'Expected record was not returned');
    }
    
    private static TestMethod void selectDependencyById_nullId_returnsEmptyList()
    {
        ProjectDependency__c dependency1 = ProjectTestScenario.createDependency().withName('a').save();
        ProjectDependency__c dependency2 = ProjectTestScenario.createDependency().withName('b').save();
        
        Integer queries = Limits.getQueries();
        ProjectDependency__c result = ProjectSelector.newInstance().selectDependencyById(null);
        assertNoRecords(result, queries);
    }

    private static TestMethod void selectDependencyById_returnsDependency()
    {
        ProjectDependency__c dependency1 = ProjectTestScenario.createDependency().withName('a').save();
        ProjectDependency__c dependency2 = ProjectTestScenario.createDependency().withName('b').save();
        
        ProjectDependency__c result = ProjectSelector.newInstance().selectDependencyById(dependency1.Id);
        System.assertEquals(dependency1.SoftwareName__c, result.SoftwareName__c, 'Should return correct dependency');
    }
    
    private static TestMethod void selectDependenciesById_nullIds_returnsEmptyList()
    {
        ProjectDependency__c dependency1 = ProjectTestScenario.createDependency().withName('a').save();
        ProjectDependency__c dependency2 = ProjectTestScenario.createDependency().withName('b').save();
        
        Integer queries = Limits.getQueries();
        List<ProjectDependency__c> result = ProjectSelector.newInstance().selectDependenciesById(null);
        assertNoRecords(result, queries);
    }

    private static TestMethod void selectDependenciesById_emptySet_returnsEmptyList()
    {
        ProjectDependency__c dependency1 = ProjectTestScenario.createDependency().withName('a').save();
        ProjectDependency__c dependency2 = ProjectTestScenario.createDependency().withName('b').save();
        
        Integer queries = Limits.getQueries();
        List<ProjectDependency__c> result = ProjectSelector.newInstance().selectDependenciesById(new Set<Id>());
        assertNoRecords(result, queries);
    }

    private static TestMethod void selectDependenciesById_returnsDependencies()
    {
        ProjectDependency__c dependency1 = ProjectTestScenario.createDependency().withName('a').save();
        ProjectDependency__c dependency2 = ProjectTestScenario.createDependency().withName('b').save();

        List<ProjectDependency__c> result = ProjectSelector.newInstance().selectDependenciesById(new Set<Id>{dependency1.Id});
        assertGotRecord(dependency1, result);
    }
    
    private static TestMethod void selectDependenciesWhere_nullWhereClause_returnsEmptyList()
	{
        ProjectDependency__c dependency1 = ProjectTestScenario.createDependency().withName('a').save();
        ProjectDependency__c dependency2 = ProjectTestScenario.createDependency().withName('b').save();

        Integer queries = Limits.getQueries();
        List<ProjectDependency__c> result = ProjectSelector.newInstance().selectDependenciesWhere(null);
        assertNoRecords(result, queries);
    }

    private static TestMethod void selectDependenciesWhere_emptyStringWhereClause_returnsEmptyList()
    {
        ProjectDependency__c dependency1 = ProjectTestScenario.createDependency().withName('a').save();
        ProjectDependency__c dependency2 = ProjectTestScenario.createDependency().withName('b').save();

        Integer queries = Limits.getQueries();
        List<ProjectDependency__c> result = ProjectSelector.newInstance().selectDependenciesWhere('');
        assertNoRecords(result, queries);
    }

    private static TestMethod void selectDependenciesWhere_returnsDependencies()
    {
        ProjectDependency__c dependency1 = ProjectTestScenario.createDependency().withName('a').save();
        ProjectDependency__c dependency2 = ProjectTestScenario.createDependency().withName('b').save();

        List<ProjectDependency__c> result = ProjectSelector.newInstance().selectDependenciesWhere('SoftwareName__c = \'a\'');
        assertGotRecord(dependency1, result);
    }
    
    private static void assertNoRecords(SObject result, Integer queries)
    {
        System.assertEquals(null, result, 'Expected null record');
        System.assertEquals(queries, Limits.getQueries(), 'Expected no database queries');
    }
    
    private static void assertNoRecords(List<SObject> results, Integer queries)
    {
        System.assertEquals(0, results.size(), 'Expected empty list');
        System.assertEquals(queries, Limits.getQueries(), 'Expected no database queries');
    }
    
    private static void assertGotRecord(SObject record, SObject result)
    {
        assertGotRecord(record, new List<SObject>{result});
    }
    
    private static void assertGotRecord(SObject record, List<SObject> results)
    {
        System.assertEquals(1, results.size(), 'Expected 1 record');
        System.assertEquals(record.Id, results[0].Id, 'Unexpected record returned');
    }

}