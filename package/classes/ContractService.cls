global class ContractService 
{
    global interface API
    {
        Id createFromOpportunity(ContractService.ContractSettings settings);
        Id createOrder(Contract orderContract, Date startDate);
    }
    
    private static ContractService.API impl;
    
    global static API newInstance()
    {
		if(impl != null)
        {
            return impl;
        }
        else
        {
            return new ServiceImplementation();
        }
    }
    
    global static void setImplementation(ContractService.API implementation)
    {
        ContractService.impl = implementation;
	}
    
    global class ServiceImplementation implements ContractService.API
    {
        global Id createFromOpportunity(ContractService.ContractSettings settings)
        {   
            Contract newContract = new Contract(
            	Opportunity__c = settings.Opp.Id,
                Status = 'Draft',
                StartDate = settings.StartDate,
                AccountId = settings.Opp.AccountId,
                ContractTerm = settings.Periods,
                Pricebook2Id = settings.Opp.Pricebook2Id
            );
            
            insert newContract;
            
            return newContract.Id;
        }
        
        global Id createOrder(Contract orderContract, Date startDate)
        {
            Order newOrder = new Order(
            	ContractId = orderContract.Id,
                Status = 'Draft',
                EffectiveDate = startDate,
                Pricebook2Id = orderContract.Pricebook2Id,
                AccountId = orderContract.AccountId,
                BillingCity = orderContract.BillingCity,
                BillingCountry = orderContract.BillingCountry,
                BillingPostalCode = orderContract.BillingPostalCode
            );
            
            insert newOrder;
            createOrderLines(newOrder, orderContract);
            
            return newOrder.Id;
        }
        
        private void createOrderLines(Order header, Contract orderContract)
        {
			List<OpportunityLineItem> lines = [SELECT Id, Name, Description, UnitPrice, PriceBookEntryId, Quantity FROM OpportunityLineItem WHERE OpportunityId =: orderContract.Opportunity__c];
            
            List<OrderItem> orderLines = new List<OrderItem>();
            for(OpportunityLineItem line : lines)
            {
                OrderItem orderLine = new OrderItem(
                    OrderId = header.Id,
                    PricebookEntryId = line.PriceBookEntryId,
                    Description = line.Description,
                    Quantity = line.Quantity,
                    UnitPrice = line.UnitPrice
                );
                
                orderLines.add(orderLine);
			}
            
            if(!orderLines.isEmpty())
                insert orderLines;
        }
    }
    
    global class ContractSettings
    {
        global Opportunity Opp {get; set;}
        global Date StartDate {get; set;}
        global Integer Periods {get; set;}
    }
    
    global class Mock implements ContractService.API
    {
        sim_util.Mocks.Method mockCreateFromOpportunity = new sim_util.Mocks.Method('ContractService', 'createFromOpportunity');
        sim_util.Mocks.Method mockCreateOrder = new sim_util.Mocks.Method('ContractService', 'createOrder');
        
        global void assertCalls()
        {
            mockCreateFromOpportunity.doAssertsNow();
            mockCreateOrder.doAssertsNow();
        }
        
        global Id createFromOpportunity(ContractService.ContractSettings settings)
        {
            return (Id)mockCreateFromOpportunity.call(new List<Object>{settings});
        }
        
        public sim_util.Mocks.Method whenCreateFromOpportunity()
        {
            return mockCreateFromOpportunity;
        }
        
        global Id createOrder(Contract orderContract, Date startDate)
        {
            return (Id)mockCreateOrder.call(new List<Object>{orderContract, startDate});
        }
        
        public sim_util.Mocks.Method whenCreateOrder()
        {
            return mockCreateOrder;
        }
    }

}