public class Projects
{
	public static String STATUS_PENDING = 'Pending';
    public static String STATUS_PLANNING = 'Planning';
    public static String STATUS_IN_PROGRESS = 'In Progress';
    public static String STATUS_ABANDONED = 'Abandoned';
    public static String STATUS_COMPLETE = 'Complete';
    public static String STATUS_DEPRECATED = 'Deprecated';
    
    public static String PLATFORM_SALESFORCE = 'Salesforce';
    public static String PLATFORM_WEB = 'Web';
    public static String PLATFORM_WINDOWS = 'Windows';
    public static String PLATFORM_MAC_OS = 'Mac OS';
    public static String PLATFORM_ANDROID = 'Android';
    public static String PLATFORM_IOS = 'iOS';
    public static String PLATFORM_OTHER = 'Other';
    
    public static String SOURCE_CONTROL_NONE = 'None';
    public static String SOURCE_CONTROL_MANUAL = 'Manual';
    public static String SOURCE_CONTROL_GIT = 'GIT';
    public static String SOURCE_CONTROL_SVN = 'SVN';
    
    public static String TYPE_INTERNAL = 'Internal';
    public static String TYPE_EXTERNAL = 'External';
    
    public class Validator implements ProjectContext.Validator
    {
        public Boolean ValidateStatus {get; set;}
        public Boolean ValidateCantModifyDeprecated {get; set;}
        public Boolean ValidateCantDeleteActiveProject {get; set;}
        
        private List<Project__c> newProjects;
        private List<Project__c> oldProjects;
        
        public Validator()
        {
            ValidateStatus = true;
            ValidateCantModifyDeprecated = true;
            ValidateCantDeleteActiveProject = true;
        }
        
        public Validator withOldRecords(List<SObject> oldRecords)
        {
            oldProjects = (List<Project__c>)oldRecords;
            return this;
        }
        
        public Validator withNewRecords(List<SObject> newRecords)
        {
            newProjects = (List<Project__c>)newRecords;
            return this;
        }
        
        public Validator exceptStatus()
        {
            ValidateStatus = false;
            return this;
        }
        
        public Validator exceptCantModifyDeprecated()
        {
			ValidateCantModifyDeprecated = false;
            return this;
        }
        
        public Validator exceptCantDeleteActiveProject()
        {
            ValidateCantDeleteActiveProject = false;
            return this;
        }
        
        public void validate()
        {
            for(Project__c project : getProjects())
            {
                if(ProjectContext.isRecordPreValidated( (SObject)project) )
                    continue;
                
                ValidationHandler handler = new ValidationHandler(project);
                
                if(isInsert())
                {
                    validateInsert(handler);
                    continue;
                }
                
                if(isDelete())
                {
                    validateDelete(handler);
                    continue;
                }
                
                //Update validation, if the handler doesn't have an ID then something went wrong!
            	if(handler.getId() == null)
                	handler.registerError('Update trigger but record has no ID');
                
                //Insert and update validation
                if(ValidateStatus)
                    doValidateStatus(handler);
                
                if(ValidateCantModifyDeprecated)
                    doValidateCantModifyDeprecated(handler);
                
                if(!ProjectContext.isInTrigger())
                    ProjectContext.registerPreValidated(handler);
            }
        }
        
        private void validateInsert(ValidationHandler handler)
        {
            
        }
        
        private void validateDelete(ValidationHandler handler)
        {
            if(ValidateCantDeleteActiveProject)
	        	doValidateCantDeleteActiveProject(handler);
        }
        
        private void doValidateStatus(ValidationHandler handler)
        {
            String oldStatus = getOldRecord(handler.getId()).Status__c;
            String newStatus = ((Project__c)handler.getRecord()).Status__c;
            
            if(oldStatus != newStatus)
            {
                if(oldStatus == Projects.STATUS_PENDING && !(newStatus == Projects.STATUS_PLANNING || newStatus == Projects.STATUS_ABANDONED))
                    handler.registerError('Status__c', 'Invalid status change');
                
                if(oldStatus == Projects.STATUS_PLANNING && !(newStatus == Projects.STATUS_IN_PROGRESS || newStatus == Projects.STATUS_ABANDONED))
                    handler.registerError('Status__c', 'Invalid status change');
                
                if(oldStatus == Projects.STATUS_IN_PROGRESS && !(newStatus == Projects.STATUS_COMPLETE || newStatus == Projects.STATUS_ABANDONED))
                    handler.registerError('Status__c', 'Invalid status change');
                
                if(oldStatus == Projects.STATUS_COMPLETE && !( newStatus == Projects.STATUS_DEPRECATED || newStatus == Projects.STATUS_ABANDONED || newStatus == Projects.STATUS_PLANNING))
                    handler.registerError('Status__c', 'Invalid status change');
                
                if(oldStatus == Projects.STATUS_ABANDONED && newStatus != Projects.STATUS_ABANDONED)
                    handler.registerError('Status__c', 'Invalid status change');
            }
        }
        
        private void doValidateCantModifyDeprecated(ValidationHandler handler)
        {
			if( ((Project__c)handler.getRecord()).Status__c == Projects.STATUS_DEPRECATED )
                handler.registerError('Cannot modify a deprecated project');
        }
        
        private void doValidateCantDeleteActiveProject(ValidationHandler handler)
        {
            if( ((Project__c)handler.getRecord()).Status__c != Projects.STATUS_DEPRECATED )
                handler.registerError('Cannot delete an active project');
        }
        
        private List<Project__c> getProjects()
        {
            if(newProjects != null)
                return newProjects;
            
            if(oldProjects != null)
                return oldProjects;
            
            return new List<Project__c>();
        }
        
        private Project__c getOldRecord(Id recordId)
        {
            if(oldProjects == null)
            {
                oldProjects = ProjectSelector.newInstance().selectProjectsById(new Map<Id, Project__c>(newProjects).keySet());
            }
            
            return new Map<Id, Project__c>(oldProjects).get(recordId);
        }
        
        private Boolean isInsert()
        {
            return ProjectContext.isInTrigger() && oldProjects == null && newProjects != null;
        }
        
        private Boolean isDelete()
        {
            return ProjectContext.isInTrigger() && oldProjects != null && newProjects == null;
        }
    }
    
    public class ValidationHandler implements ProjectContext.ValidationHandler
    {
        private Project__c record;
        
        public ValidationHandler(Project__c sObj)
        {
            record = sObj;
        }
        
        public Id getId()
        {
            return record.Id;
        }
        
        public SObject getRecord()
        {
            return record;
        }
        
        public Boolean isEqual(SObject otherRecord)
        {
            return false;
        }
        
        public void registerError(String errorMessage)
        {
            if(ProjectContext.isInTrigger())
            {
                record.addError(errorMessage);
            }
            else
            {
                throw new ProjectException(errorMessage);
            }
        }
        
        public void registerError(String fieldname, String errorMessage)
        {
            if(!ProjectContext.isInTrigger())
                throw new ProjectException(errorMessage);
            
            if(fieldName == 'Status__c')
            {
                record.Status__c.addError(errorMessage);
            }
        }
    }
}