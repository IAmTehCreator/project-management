@isTest(SeeAllData=false)
private class ProjectMocksTest
{
	private static TestMethod void notificationServiceMock_assertsCalls()
    {
        ProjectMocks.NotificationService mock = new ProjectMocks.NotificationService();
        NotificationService.setImplementation(mock);
        
        mock.whenSendNotifications().thenAssertCalled(1);
        mock.whenSendEmail().thenAssertCalled(1);
        mock.whenSendEmails().thenAssertCalled(1);
        mock.whenSendChatterPost().thenAssertCalled(1);
        mock.whenSendChatterPosts().thenAssertCalled(1);
        
        NotificationService.newInstance().sendNotifications(null, false, false);
        NotificationService.newInstance().sendEmail(null);
        NotificationService.newInstance().sendEmails(null);
        NotificationService.newInstance().sendChatterPost(null);
        NotificationService.newInstance().sendChatterPosts(null);
        
        mock.assertCalls();
    }
    
    private static TestMethod void validator_assertsCalls()
    {
        ProjectMocks.Validator mock = new ProjectMocks.Validator();
        
        mock.whenWithOldRecords().thenAssertCalled(1);
        mock.whenWithNewRecords().thenAssertCalled(1);
        mock.whenValidate().thenAssertCalled(1);
        
        mock.withOldRecords(null);
        mock.withNewRecords(null);
        mock.validate();
        
        mock.assertCalls();
    }
    
    private static TestMethod void validationHandler_assertsCalls()
    {
        ProjectMocks.ValidationHandler mock = new ProjectMocks.ValidationHandler();
        
        mock.whenGetId().thenAssertCalled(1);
        mock.whenGetRecord().thenAssertCalled(1);
        mock.whenIsEqual().thenAssertCalled(1);
        mock.whenRegisterError().thenAssertCalled(1);
        mock.whenRegisterFieldError().thenAssertCalled(1);
        
        mock.getId();
        mock.getRecord();
        mock.isEqual(null);
        mock.registerError('TestError');
        mock.registerError('Field__c', 'TestError');
        
        mock.assertCalls();
    }
    
    private static TestMethod void projectSelector_assertsCalls()
    {
        ProjectMocks.ProjectSelector mock = new ProjectMocks.ProjectSelector();
        ProjectSelector.setImplementation(mock);
        
        mock.whenSelectProjectById().thenAssertCalled(1);
        mock.whenSelectProjectsById().thenAssertCalled(1);
        mock.whenSelectProjectsWhere().thenAssertCalled(1);
        mock.whenSelectVersionsByProjectId().thenAssertCalled(1);
        mock.whenSelectFeaturesByProjectId().thenAssertCalled(1);
        mock.whenSelectBugsByProjectId().thenAssertCalled(1);
        mock.whenSelectAssetsByProjectId().thenAssertCalled(1);
        mock.whenSelectVersionById().thenAssertCalled(1);
        mock.whenSelectVersionsById().thenAssertCalled(1);
        mock.whenSelectVersionsWhere().thenAssertCalled(1);
        mock.whenSelectFeatureById().thenAssertCalled(1);
        mock.whenSelectFeaturesById().thenAssertCalled(1);
        mock.whenSelectFeaturesWhere().thenAssertCalled(1);
        mock.whenSelectBugById().thenAssertCalled(1);
        mock.whenSelectBugsById().thenAssertCalled(1);
        mock.whenSelectBugsWhere().thenAssertCalled(1);
        mock.whenSelectAssetById().thenAssertCalled(1);
        mock.whenSelectAssetsById().thenAssertCalled(1);
        mock.whenSelectAssetsWhere().thenAssertCalled(1);
        mock.whenSelectAssetsMappedByProjectId().thenAssertCalled(1);
        mock.whenSelectDependencyById().thenAssertCalled(1);
        mock.whenSelectDependenciesById().thenAssertCalled(1);
        mock.whenSelectDependenciesWhere().thenAssertCalled(1);
        
        ProjectSelector.newInstance().selectProjectById(null);
        ProjectSelector.newInstance().selectProjectsById(null);
        ProjectSelector.newInstance().selectProjectsWhere(null);
        ProjectSelector.newInstance().selectVersionsByProjectId(null);
        ProjectSelector.newInstance().selectFeaturesByProjectId(null);
        ProjectSelector.newInstance().selectBugsByProjectId(null);
        ProjectSelector.newInstance().selectAssetsByProjectId(null);
        ProjectSelector.newInstance().selectVersionById(null);
        ProjectSelector.newInstance().selectVersionsById(null);
        ProjectSelector.newInstance().selectVersionsWhere(null);
        ProjectSelector.newInstance().selectFeatureById(null);
        ProjectSelector.newInstance().selectFeaturesById(null);
        ProjectSelector.newInstance().selectFeaturesWhere(null);
        ProjectSelector.newInstance().selectBugById(null);
        ProjectSelector.newInstance().selectBugsById(null);
        ProjectSelector.newInstance().selectBugsWhere(null);
        ProjectSelector.newInstance().selectAssetById(null);
        ProjectSelector.newInstance().selectAssetsById(null);
        ProjectSelector.newInstance().selectAssetsWhere(null);
        ProjectSelector.newInstance().selectAssetsMappedByProjectID(null);
        ProjectSelector.newInstance().selectDependencyById(null);
        ProjectSelector.newInstance().selectDependenciesById(null);
        ProjectSelector.newInstance().selectDependenciesWhere(null);
        
        mock.assertCalls();
    }
}