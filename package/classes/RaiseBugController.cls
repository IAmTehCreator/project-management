public class RaiseBugController
{

    public ProjectBug__c Bug {get; set;}
    public Case RelatedCase {get; set;}
    public Boolean Valid {get; private set;}
    
    public RaiseBugController(ApexPages.StandardController stdController)
    {
        Valid = true;
        RelatedCase = (Case)stdController.getRecord();
        
        if(RelatedCase.Bug__c != null)
        {
            Valid = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This case already has a bug attached.'));
        }
        else
        {
            Bug = new ProjectBug__c();
        }
    }
    
    public PageReference raise()
    {
        try
        {
        	insert Bug;
            
            RelatedCase.Bug__c = Bug.Id;
            update RelatedCase;
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            
            return null;
        }
        
        return new PageReference('/' + Bug.Id);
    }
}