public class MyFeatureEditController
{
    public static final String ACTION_LOG = 'log';
    public static final String ACTION_DROP = 'drop';
    public static final String ACTION_PLAN = 'plan';
    public static final String ACTION_START = 'start';
    public static final String ACTION_COMPLETE = 'complete';
    public static final String ACTION_TEST = 'test';
    
    public static final String TYPE_BUG = 'bug';
    public static final String TYPE_FEATURE = 'feature';

    public String Mode {get; set;}
    public String ItemType {get; set;}
    public String PageTitle {get; set;}
    public String Message {get; set;}
    public Id RecordId {get; set;}
    
    public Decimal Hours {get; set;}
    
    public Project__c Project {get; set;}
    public ProjectFeature__c Feature {get; set;}
    public ProjectBug__c Bug {get; set;}
    
	public MyFeatureEditController()
    {
        Map<String, String> urlParams = ApexPages.currentPage().getParameters();
        Mode = urlParams.get('action');
        ItemType = urlParams.get('type');
        RecordId = urlParams.get('recordid');
        
        getRecord();
        setPageText();
        
        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO, Message) );
    }
    
    public PageReference cancel()
    {
        return new PageReference('/apex/myfeatures');
    }
    
    public PageReference next()
    {
        try{
            if(Mode == ACTION_LOG)
            {
                if(ItemType == TYPE_FEATURE)
                {
                    Feature.Remaining_Dev_Hours__c = Feature.Remaining_Dev_Hours__c - Hours;
                    Feature.Total_Dev_Hours__c = Feature.Total_Dev_Hours__c + Hours;
                    
                    if(Feature.Remaining_Dev_Hours__c < 0)
                        Feature.Remaining_Dev_Hours__c = 0;
                    
                    update Feature;
                }
                else if(ItemType == TYPE_BUG)
                {
                    Bug.Remaining_Dev_Hours__c = Bug.Remaining_Dev_Hours__c - Hours;
                    Bug.Total_Dev_Hours__c = Bug.Total_Dev_Hours__c + Hours;
                    
                    if(Bug.Remaining_Dev_Hours__c < 0)
                        Bug.Remaining_Dev_Hours__c = 0;
                    
                    update Bug;
                }
            }
            else if(Mode == ACTION_DROP)
            {
                if(ItemType == TYPE_FEATURE)
                {
                    Feature.Assigned_User__c = null;
                    update Feature;
                }
                else if(ItemType == TYPE_BUG)
                {
                    Bug.Assigned_User__c = null;
                    update Bug;
                }
            }
            else if(Mode == ACTION_PLAN)
            {
                Feature.Status__c = ProjectFeatures.STATUS_PLANNING;
                update Feature;
            }
            else if(Mode == ACTION_START)
            {
                if(ItemType == TYPE_FEATURE)
                {
                    Feature.Status__c = ProjectFeatures.STATUS_IN_PROGRESS;
                    Feature.Start_Date__c = Date.today();
                    update Feature;
                }
                else if(ItemType == TYPE_BUG)
                {
                    Bug.Status__c = ProjectBugs.STATUS_FIXING;
                    update Bug;
                }
            }
            else if(Mode == ACTION_COMPLETE)
            {
                if(ItemType == TYPE_FEATURE)
                {
                    Feature.Remaining_Dev_Hours__c = 0;
                    Feature.Status__c = ProjectFeatures.STATUS_COMPLETE;
                    Feature.Complete_Date__c = Date.today();
                    update Feature;
                }
                else if(ItemType == TYPE_BUG)
                {
                    Bug.Remaining_Dev_Hours__c = 0;
                    Bug.Status__c = ProjectBugs.STATUS_FIXED;
                    Bug.Fixed_Date__c = Date.today();
                    Bug.Fixed_In_Version__c = Project.Current_Version__c;
                    update Bug;
                }
            }
            else if(Mode == ACTION_TEST)
            {
                Bug.Status__c = ProjectBugs.STATUS_TESTING;
                update Bug;
            }
        }
        catch(Exception e)
        {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()) );
            return null;
        }
        
        return new PageReference('/apex/myfeatures');
    }
    
    private void getRecord()
    {
		if(ItemType == TYPE_FEATURE)
        {
            Feature = ProjectSelector.newInstance().selectFeatureById(RecordId);
            Project = ProjectSelector.newInstance().selectProjectById(Feature.Project__c);
        }
        else if(ItemType == TYPE_BUG)
        {
            Bug = ProjectSelector.newInstance().selectBugById(RecordId);
            Project = ProjectSelector.newInstance().selectProjectById(Bug.Project__c);
        }
    }
    
    private void setPageText()
    {
		if(Mode == ACTION_COMPLETE)
        {
            PageTitle = 'Complete ' + ItemType;
            Message = 'This will set remaining hours to zero and set this ' + ItemType + ' to complete, click \'Next\' to continue.';
        }
        else if(Mode == ACTION_LOG)
        {
            PageTitle = 'Log work on ' + ItemType;
            Message = 'This will add the set number of hours onto the total hours and deduct the set number of hours from the remaining hours. Click \'Next\' to continue.';
        }
        else if(Mode == ACTION_DROP)
        {
            PageTitle = 'Drop ' + ItemType;
            Message = 'This will un-assign you from this ' + ItemType + '. Click \'Next\' to continue.';
        }
        else if(Mode == ACTION_START)
        {
            PageTitle = 'Start ' + ItemType;
            Message = 'This will mark the ' + ItemType + ' as started on todays date. Click \'Next\' to continue.';
        }
        else if(Mode == ACTION_TEST)
        {
            PageTitle = 'Start testing ' + Bug.Name;
            Message = 'This will mark this bug as in the testing phase. Click \'Next\' to continue.';
        }
        else if(Mode == ACTION_PLAN)
        {
            PageTitle = 'Start planning ' + ItemType;
            Message = 'This will mark this feature as in the planning phase. Click \'Next\' to continue.';
        }
    }
}