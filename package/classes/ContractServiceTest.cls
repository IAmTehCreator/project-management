@isTest(SeeAllData=true)
private class ContractServiceTest
{
	private static TestMethod void newInstance_returnsNewInstance()
    {
        System.assert(
            ContractService.newInstance() instanceof ContractService.ServiceImplementation,
            'Should return a new service implementation by default'
        );
    }
    
    private static TestMethod void setImplementation_setsServiceImplementation()
    {
        ContractService.Mock mockService = new ContractService.Mock();
        ContractService.setImplementation(mockService);
        
        System.assertEquals(
        	mockService,
            ContractService.newInstance(),
            'Should return the specified service implementation'
        );
    }
    
    private static TestMethod void createFromOpportunity_createsContract()
    {
        Account acc = new Account(Name = 'Test Account');
        insert acc;

        Pricebook2 pricebook = new Pricebook2(Name = 'Test Pricebook');
        insert pricebook;

        Opportunity opp = new Opportunity(
            Name = 'test',
            Pricebook2Id = pricebook.Id,
            AccountId = acc.Id,
            StageName = 'Prospecting',
            CloseDate = Date.today()
        );
        insert opp;

        Date startDate = Date.today();
        ContractService.ContractSettings settings = new ContractService.ContractSettings();
        settings.Opp = opp;
        settings.Periods = 6;
        settings.StartDate = startDate;

        Id contractId = ContractService.newInstance().createFromOpportunity(settings);
        Contract ct = [SELECT Id, AccountId, Status, StartDate, ContractTerm, Pricebook2Id, Opportunity__c FROM Contract WHERE Id =: contractId];
        
        System.assertEquals(acc.Id, ct.AccountId, 'Should set the opportunity account on the contract');
        System.assertEquals(startDate, ct.StartDate, 'Should set the specified start date on the contract');
        System.assertEquals('Draft', ct.Status, 'Should create a draft contract');
        System.assertEquals(6, ct.ContractTerm, 'Should set the specified number of terms');
        System.assertEquals(pricebook.Id, ct.Pricebook2Id, 'Should set the opportunity pricebook on the contract');
        System.assertEquals(opp.Id, ct.Opportunity__c, 'Should set the opportunity ID on the contract');
    }

    private static TestMethod void createOrder_createsOrderAndLines()
    {
        Date orderDate = Date.today().addDays(3);

        Account acc = new Account(Name = 'Test Account');
        insert acc;
        
        Pricebook2 pricebook = [SELECT Id FROM Pricebook2 WHERE IsStandard = true LIMIT 1];

        //Pricebook2 pricebook = new Pricebook2(Name = 'Test Pricebook');
        //insert pricebook;
        
        Product2 product = new Product2(Name = 'Test Product');
        insert product;
        
        PricebookEntry entry = new PricebookEntry(
        	Pricebook2Id = pricebook.Id,
            Product2Id = product.Id,
            UnitPrice = 123.99,
            IsActive = true
        );
        insert entry;
        
        Opportunity opp = new Opportunity(
            Name = 'test',
            Pricebook2Id = pricebook.Id,
            AccountId = acc.Id,
            StageName = 'Prospecting',
            CloseDate = Date.today()
        );
        insert opp;
        
        OpportunityLineItem line = new OpportunityLineItem(
        	OpportunityId = opp.Id,
            ServiceDate = Date.today(),
            PricebookEntryId = entry.Id,
            Quantity = 2,
            UnitPrice = 123.99
        );
        insert line;
        
        Contract ct = new Contract(
        	Opportunity__c = opp.Id,
            Status = 'Draft',
            StartDate = Date.today(),
            AccountId = acc.Id,
            ContractTerm = 12,
            Pricebook2Id = pricebook.Id
        );
        insert ct;
        
        Id orderId = ContractService.newInstance().createOrder(ct, orderDate);
        Order od = [
            SELECT Id, ContractId, Status, EffectiveDate, Pricebook2Id, AccountId
            FROM Order
            WHERE Id = :orderId
        ];
        
        System.assertEquals(ct.Id, od.ContractId, 'Should set the contract ID');
        System.assertEquals('Draft', od.Status, 'Should create a draft order');
        System.assertEquals(orderDate, od.EffectiveDate, 'Should set the correct effective date');
        System.assertEquals(pricebook.Id, od.Pricebook2Id, 'Should set the contract pricebook');
        System.assertEquals(acc.Id, od.AccountId, 'Should set the contract account');
        
        List<OrderItem> odLines = [
            SELECT Id, OrderId, PricebookEntryId, Quantity, UnitPrice
            FROM OrderItem
            WHERE OrderId = :od.Id
        ];
        
        System.assertEquals(1, odLines.size(), 'Should have created 1 order line');
        
        OrderItem odLine = odLines.get(0);
        System.assertEquals(entry.Id, odLine.PricebookEntryId, 'Should set the pricebook entry Id');
        System.assertEquals(2, odLine.Quantity, 'Should set the correct quantity');
        System.assertEquals(123.99, odLine.UnitPrice, 'Should set the correct unit price');
    }
    
    private static TestMethod void mock_assertsCalls()
    {
        ContractService.Mock mock = new ContractService.Mock();
        ContractService.setImplementation(mock);
        
        mock.whenCreateFromOpportunity().thenAssertCalled(1);
        mock.whenCreateOrder().thenAssertCalled(1);
        
        ContractService.newInstance().createFromOpportunity(null);
        ContractService.newInstance().createOrder(null, null);
        
        mock.assertCalls();
    }
}