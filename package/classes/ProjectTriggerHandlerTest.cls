@isTest(SeeAllData=true)
private class ProjectTriggerHandlerTest
{
	private static TestMethod void handlerRegistersEvents()
    {
        ProjectMocks.Validator validator = new ProjectMocks.Validator();
        ProjectTriggerHandler handler = new ProjectTriggerHandler(null, null, validator);
        
        System.assertEquals(4, handler.getEvents().size(), 'Should have registered four events');
        System.assert(handler.getEvents().contains('beforevalidate'), 'Should have registered the event "beforevalidate"');
        System.assert(handler.getEvents().contains('insert'), 'Should have registered the event "insert"');
        System.assert(handler.getEvents().contains('update'), 'Should have registered the event "update"');
        System.assert(handler.getEvents().contains('delete'), 'Should have registered the event "delete"');
    }
    
    private static TestMethod void handlerSetsRecordsOnValidator()
    {
        Account acc1 = new Account();
        Account acc2 = new Account();
        Account acc3 = new Account();
        
        List<SObject> oldList = new List<SObject>{acc1, acc2};
        List<SObject> newList = new List<SObject>{acc3};
        
        ProjectMocks.Validator validator = new ProjectMocks.Validator();
        
        validator.whenWithOldRecords().thenAssertCalled(1)
            						  .thenAssertCalledWith(new List<Object>{ newList });
        
        validator.whenWithNewRecords().thenAssertCalled(1)
            						  .thenAssertCalledWith(new List<Object>{ oldList });
        
        ProjectTriggerHandler handler = new ProjectTriggerHandler(newList, oldList, validator);
        
        validator.assertCalls();
    }
    
    private static TestMethod void handlerCanReturnValidator()
    {
        ProjectMocks.Validator validator = new ProjectMocks.Validator();
        ProjectTriggerHandler handler = new ProjectTriggerHandler(null, null, validator);
        
        System.assertEquals(validator, handler.getValidator(), 'Should have returned the validator');
    }
    
    private static TestMethod void handlerCallsValidator()
    {
        ProjectMocks.Validator validator = new ProjectMocks.Validator();
        validator.whenValidate().thenAssertCalled(0);
        
        ProjectTriggerHandler handler = new ProjectTriggerHandler(null, null, validator);
        
        validator.assertCalls();
        validator.whenValidate().thenAssertCalled(1);
        
        handler.execute();
        
		validator.assertCalls();
    }
    
    private static TestMethod void handlerFiresEventBeforeValidate_Insert()
    {
        Account acc = new Account();
        sim_util.EventBus.MockCallback fn = new sim_util.EventBus.MockCallback();
        ProjectMocks.Validator validator = new ProjectMocks.Validator();
        
        ProjectTriggerHandler handler = new ProjectTriggerHandler(null, new List<SObject>{acc}, validator);
        handler.onBeforeValidate(fn)
               .execute();
        
        System.assertEquals(1, fn.Calls.size(), 'Should fire the beforevalidate event');
    }
    
    private static TestMethod void handlerFiresEventBeforeValidate_Update()
    {
        Account acc = new Account();
        sim_util.EventBus.MockCallback fn = new sim_util.EventBus.MockCallback();
        ProjectMocks.Validator validator = new ProjectMocks.Validator();
        
        ProjectTriggerHandler handler = new ProjectTriggerHandler(new List<SObject>{acc}, new List<SObject>{acc}, validator);
        handler.onBeforeValidate(fn)
               .execute();
        
        System.assertEquals(1, fn.Calls.size(), 'Should fire the beforevalidate event');
    }
    
    private static TestMethod void handlerFiresEventBeforeValidate_Delete()
    {
        Account acc = new Account();
        sim_util.EventBus.MockCallback fn = new sim_util.EventBus.MockCallback();
        ProjectMocks.Validator validator = new ProjectMocks.Validator();
        
        ProjectTriggerHandler handler = new ProjectTriggerHandler(new List<SObject>{acc}, null, validator);
        handler.onBeforeValidate(fn)
               .execute();
        
        System.assertEquals(1, fn.Calls.size(), 'Should fire the beforevalidate event');
    }
    
    private static TestMethod void handlerFiresEvents_Insert()
    {
        Account acc = new Account();
        sim_util.EventBus.MockCallback fn = new sim_util.EventBus.MockCallback();
        ProjectMocks.Validator validator = new ProjectMocks.Validator();
        
        ProjectTriggerHandler handler = new ProjectTriggerHandler(null, new List<SObject>{acc}, validator);
        handler.onInsert(fn)
               .execute();
        
        System.assertEquals(1, fn.Calls.size(), 'Should fire the insert event');
    }
    
    private static TestMethod void handlerFiresEvents_Update()
    {
        Account acc = new Account();
        sim_util.EventBus.MockCallback fn = new sim_util.EventBus.MockCallback();
        ProjectMocks.Validator validator = new ProjectMocks.Validator();
        
        ProjectTriggerHandler handler = new ProjectTriggerHandler(new List<SObject>{acc}, new List<SObject>{acc}, validator);
        handler.onUpdate(fn)
               .execute();
        
        System.assertEquals(1, fn.Calls.size(), 'Should fire the update event');
    }
    
    private static TestMethod void handlerFiresEvents_Delete()
    {
        Account acc = new Account();
        sim_util.EventBus.MockCallback fn = new sim_util.EventBus.MockCallback();
        ProjectMocks.Validator validator = new ProjectMocks.Validator();
        
        ProjectTriggerHandler handler = new ProjectTriggerHandler(new List<SObject>{acc}, null, validator);
        handler.onDelete(fn)
               .execute();
        
        System.assertEquals(1, fn.Calls.size(), 'Should fire the delete event');
    }
    
    private static TestMethod void handlerRegistersInTrigger()
    {
        System.assertEquals(false, ProjectContext.isInTrigger(), 'SANITY -- Should not already be in a trigger');
        
        ProjectMocks.Validator validator = new ProjectMocks.Validator();
        validator.whenValidate().thenAssertCalled(1)
            					.thenDo( new HandlerRegistersInTriggerCallback() );
        
        ProjectTriggerHandler handler = new ProjectTriggerHandler(null, new List<SObject>(), validator);
        handler.execute();
        
        System.assertEquals(false, ProjectContext.isInTrigger(), 'Should register not in trigger when trigger has finished');
        validator.assertCalls();
    }
    
    public class HandlerRegistersInTriggerCallback implements sim_util.Mocks.MethodListener
    {
        public Object execute(List<Object> args)
        {
            System.assertEquals(true, ProjectContext.isInTrigger(), 'Should register in trigger while in trigger');
            return null;
        }
    }
}