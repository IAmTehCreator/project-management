@isTest(SeeAllData=false)
private class ProjectVersionsTest
{
	private static TestMethod void validationHandlerReturnsId()
    {
        ProjectVersion__c version = createProjectVersion();
        ProjectVersions.ValidationHandler handler = createValidationHandler(version);
        
        System.assertEquals(version.Id, handler.getId(), 'Should return the SObject ID');
    }
    
    private static TestMethod void validationHandlerReturnsRecord()
    {
		ProjectVersion__c version = createProjectVersion();
        ProjectVersions.ValidationHandler handler = createValidationHandler(version);
        
        System.assertEquals(version, handler.getRecord(), 'Should return the SObject');
    }
    
    private static TestMethod void validationHandlerThrowsExceptionWhenErrorRegistered()
    {
        ProjectVersions.ValidationHandler handler = createValidationHandler();
        
        try
        {
            handler.registerError('TestError');
            System.assert(false, 'Should have thrown an exception');
        }
        catch(Exception e)
        {
            System.assertEquals('TestError', e.getMessage(), 'Unexpected exception message');
        }
    }
    
    private static TestMethod void validationHandlerDoesNotThrowExceptionWhenErrorRegisteredInTrigger()
    {
        ProjectVersions.ValidationHandler handler = createValidationHandler();
        
        ProjectContext.registerInTrigger(true);
        handler.registerError('TestError');
        ProjectContext.registerInTrigger(false);
    }
    
    private static TestMethod void validationHandlerThrowsExceptionWhenFieldErrorRegistered()
    {
        ProjectVersions.ValidationHandler handler = createValidationHandler();
        
        try
        {
            handler.registerError('Status__c', 'TestError');
            System.assert(false, 'Should have thrown an exception');
        }
        catch(Exception e)
        {
            System.assertEquals('TestError', e.getMessage(), 'Unexpected exception message');
        }
    }
    
    private static ProjectVersion__c createProjectVersion()
    {
		Project__c project = new Project__c();
        project.Status__c = Projects.STATUS_PENDING;
        project.Name = 'Test Project';
        
        insert project;
        
        ProjectVersion__c version = new ProjectVersion__c();
        version.Name = '1.00';
        version.Project__c = project.Id;
        
        insert version;
        
        return version;
    }
    
    private static ProjectVersions.ValidationHandler createValidationHandler()
    {
		return createValidationHandler( createProjectVersion() );
    }
    
    private static ProjectVersions.ValidationHandler createValidationHandler( ProjectVersion__c version )
    {
        return new ProjectVersions.ValidationHandler( version );
    }
}