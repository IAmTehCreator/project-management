public class IncrementVersionController {

    public Project__c Project {get; set;}
    public ProjectVersion__c Version {get; set;}
    public ProjectVersion__c PreviousVersion {get; set;}
    public Boolean CanIncrement {get; set;}

    public IncrementVersionController(ApexPages.StandardController stdController)
    {
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, Label.IncrementVersionIntroduction);

        Project = (Project__c)stdController.getRecord();
        Version = new ProjectVersion__c();
        PreviousVersion = ProjectSelector.newInstance().selectVersionById(Project.Current_Version__c);
        CanIncrement = true;

        try
        {
        	validate();
        }
        catch(Exception e)
        {
            CanIncrement = false;
            msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
        }

        ApexPages.addMessage(msg);
    }

    private void validate()
    {
        if(Project.Status__c != 'Complete')
			throw new ProjectException(Label.ErrorIncrementProjectNotComplete);

        if(Project.Remaining_Feature_Hours__c > 0)
            throw new ProjectException(Label.ErrorIncrementProjectStillHasTime);

        if(PreviousVersion.Release_Date__c == null)
            throw new ProjectException(Label.ErrorIncrementProjectNotReleased);
    }

    public PageReference increment()
    {
        ProjectContext.registerServiceTransaction();

        try
        {
            updatePreviousVersion();
            createNewVersion();
            updateProject();
        }
        catch(Exception e)
        {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            CanIncrement = false;
            return null;
        }

		return new PageReference('/' + Project.Id);
    }

    private void updatePreviousVersion()
    {
		previousVersion.Current__c = false;
        update previousVersion;
    }

    private void createNewVersion()
    {
		Version.Current__c = true;
        Version.Project__c = Project.Id;
        insert Version;
    }

    private void updateProject()
    {
		Project.Current_Version__c = Version.Id;
        Project.Status__c = 'Planning';
        update Project;
    }

}
