@isTest(SeeAllData=false)
private class GetBugControllerTest
{
	private static TestMethod void constructor_loadsProjects()
    {
        ProjectTestScenario.Scenario scenario = ProjectTestScenario.createBasicScenario();
        scenario.Assets.get(0).withPerson( UserInfo.getUserId() ).save();
        GetBugController controller = new GetBugController();
        
        System.assertEquals(2, controller.getProjects().size(), 'Should load one project and the default option');
    }
}