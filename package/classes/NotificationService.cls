public class NotificationService
{
    private static NotificationService.API implementation;
    
	public interface API
    {
        void sendNotifications(List<NotificationService.Notification> messages, Boolean chatter, Boolean email);
        void sendEmail(NotificationService.Notification message);
        void sendEmails(List<NotificationService.Notification> messages);
        void sendChatterPost(NotificationService.Notification message);
        void sendChatterPosts(List<NotificationService.Notification> message);
    }
    
    public static NotificationService.API newInstance()
    {
        if(implementation == null)
        {
            return new NotificationService.ServiceImplementation();
        }
        else
        {
            return implementation;
        }
    }
    
    public static void setImplementation(NotificationService.API impl)
    {
        implementation = impl;
    }
    
    public class ServiceImplementation implements NotificationService.API
    {
        private Map<Id, User> usersById;

        public void sendNotifications(List<NotificationService.Notification> messages, Boolean chatter, Boolean email)
        {
            if(chatter)
                sendChatterPosts(messages);
            
            if(email)
                sendEmails(messages);
            
            usersById = null;
        }
        
        public void sendEmail(NotificationService.Notification message)
        {
            sendEmails(new List<NotificationService.Notification>{message});
        }
        
        public void sendEmails(List<NotificationService.Notification> messages)
        {
            Map<Id, User> users = selectUsersById( getUserIds(messages) );

            List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
            for(NotificationService.Notification message : messages)
            {
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
				email.setSubject(message.Subject);
                email.setPlainTextBody(message.Message);
                email.setToAddresses(new List<String>{ users.get(message.User).Email });
            }
            
            Messaging.reserveSingleEmailCapacity(emails.size());
            Messaging.sendEmail(emails);
        }
        
        public void sendChatterPost(NotificationService.Notification message)
        {
            sendChatterPosts(new List<NotificationService.Notification>{message});
        }
        
        public void sendChatterPosts(List<NotificationService.Notification> messages)
        {
            Map<Id, User> users = selectUsersById( getUserIds(messages) );
            for(NotificationService.Notification message : messages)
            {
             	ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), users.get(message.User).Id, ConnectApi.FeedElementType.FeedItem, message.Message);   
            }
        }
        
        private Map<Id, User> selectUsersById(Set<Id> ids)
        {
            if (ids == null || ids.isEmpty())
                return new Map<Id, User>();
            
            if ( usersById == null )
                usersById = new Map<Id, User>([SELECT Name, Email FROM User WHERE Id IN :ids]);
            
            return usersById;
        }
        
        private Set<Id> getUserIds(List<NotificationService.Notification> messages)
        {
            Set<Id> ids = new Set<Id>();
            for(NotificationService.Notification message : messages)
            {
                ids.add(message.User);
            }
            return ids;
        }
    }
    
    public class Notification
    {
        public Id User {get; set;}
        public String Subject {get; set;}
        public String Message {get; set;}
    }
}