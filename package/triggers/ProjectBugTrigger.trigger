trigger ProjectBugTrigger on ProjectBug__c (before delete, after insert, after update)
{
	ProjectTriggerHandler handler = new ProjectTriggerHandler(Trigger.old, Trigger.new, ProjectBugs.newValidator());
    handler.onInsert( new ProjectTriggerActions.SendNotificationToProjectDevelopers() )
		   .execute();
}