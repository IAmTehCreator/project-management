trigger ProjectFeatureTrigger on ProjectFeature__c (before delete, after insert, after update)
{
    ProjectTriggerHandler handler = new ProjectTriggerHandler(Trigger.old, Trigger.new, ProjectFeatures.newValidator());
	handler.execute();
}