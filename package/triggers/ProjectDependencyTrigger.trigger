trigger ProjectDependencyTrigger on ProjectDependency__c (before delete, after insert, after update)
{
    ProjectTriggerHandler handler = new ProjectTriggerHandler(Trigger.old, Trigger.new, new ProjectDependencies.Validator());
    handler.execute();
}