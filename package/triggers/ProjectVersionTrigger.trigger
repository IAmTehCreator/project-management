trigger ProjectVersionTrigger on ProjectVersion__c (before delete, after insert, after update)
{
    ProjectTriggerHandler handler = new ProjectTriggerHandler(Trigger.old, Trigger.new, new ProjectVersions.Validator());
    handler.execute();
}