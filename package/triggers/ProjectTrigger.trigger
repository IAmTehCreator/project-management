trigger ProjectTrigger on Project__c (before delete, after insert, after update)
{   
    ProjectTriggerHandler handler = new ProjectTriggerHandler(Trigger.old, Trigger.new, new Projects.Validator());
    handler.execute();
}