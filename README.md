# Project Management Package
This repository contains the Project Management Package for Salesforce. This project uses the [Apache Ant](http://ant.apache.org/) build system. Below is a summary of the build targets which can be invoked through the terminal with the command `ant target[,target][,target...] [-Dproperty=value]`.

* init - Requires dependencies
* clean - Deletes old build files
* deploy - Deploys the package to a Salesforce Organization
* undeploy - Removes the package from a Salesforce Organization

Before the package can be deployed to an Org you must specify the login credentials to be used in the `local.build.properties` file by inserting your username, password and security token to the lines shown below;

```
sf.username = <Insert your Salesforce username here>
sf.password = <Insert your Salesforce password here>
sf.token = <Insert your Salesforce security token here>
```
